import React from 'react';
import Header from '../headerBar';
import Menu from '../sideBar';
import Footer from '../footer';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';


const Contact = () => {

    return (
        <div className='main'>
            <Header />
            <Menu />
            <div className='content'>
                <div className="container3">
                    <div className='headersGrid' style={{ paddingLeft: '20vw' }}>Contacto</div>
                    <div className='schoolPic' />
                    <div className='contactInfo'>
                        <div className='contactInfo-child' style={{ paddingLeft: '4vw', fontSize: '18px' }}>
                            <div>
                                Director: Manuel Antonio Argüelles Morales
                            </div>
                            <div>
                                Ubicación: Calle Otancahui s/n entre Guerrero e Hidalgo, col. Infonavit Yukujimari
                            </div>
                            <div>
                                Municipio: Cd. Obregón
                            </div>
                            <div>
                                Teléfono: (644) 415 3087 y 415 3086
                            </div>
                            <div>
                                Correo: enso.direccion@creson.edu.mx
                            </div>
                            <div style={{ display: 'flex' }}>
                                <Link to='/external'>
                                    <div style={{ marginRight: '12vw' }} className='external'>
                                        Sitio: http://enso.creson.edu.mx/
                                </div>
                                </Link>
                                <Link to='/dashboard'>
                                    <Button style={{ marginBottom: '2vh', opacity: '0.9', width: '10vw !important', paddingLeft: "0px !important", paddingRight: "0px !important", height: '8vh' }}
                                    // onClick={tablePDF()}
                                    >
                                        <span class="fas fa-home"></span> Regresar
                            </Button>
                                </Link>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div >
    );

}

export default Contact;