import React from 'react';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import { useApolloClient } from "@apollo/react-hooks";
import { useHistory } from "react-router-dom";
import client from '../../config/apollo';
import { ApolloProvider } from '@apollo/react-hooks';
// import { ApolloConsumer, ApolloClient } from '@apollo/client';
// import { InMemoryCache } from 'apollo-cache-inmemory';
// import {
//     AsyncStorage,
//     View
// } from 'react-native';
// const cache = new InMemoryCache();



const Form = () => {
    //const client = useApolloClient();
    let history = useHistory();

    //const authToken = localStorage.getItem('token');
    // const cerrarSesion = () => {
    //     localStorage.removeItem('token');
    //     history.push('/');
    // };
    // const logout = () => {
    //     return (
    //         <ApolloConsumer />
    //     );
    // };
    const cerrarSesionUsuario = async (cliente, history) => {
        //console.log(localStorage.getItem('token'));
        await localStorage.removeItem('token');

        //desloguear
        //console.log(localStorage.getItem('token'));
        //cliente.resetStore();
        //redireccionar
        history.push('/');
    }
    // React.useEffect(logout, []);

    return (
        <div className='header'>
            <div className='logoSmall' />
            <div className='navHeader'>
                {'ESCUELA NORMAL SUPERIOR'}
                <div className='navHeaderSub'>Plantel Cd. Obregón</div>
            </div>
            <div className='sysTitle'>
                <div className='sysTitle'>Sistema de Talleres y Clubes</div>
                <span class="fas fa-school" style={{ marginLeft: '8.6vw' }}></span>
            </div>
            <Link to='/dashboard/contact'>
                <Button className='buttonA' style={{ paddingLeft: '0.5vw', paddingRight: '0.5vw' }}>
                    <span class="fas fa-headset" style={{ marginRight: '0.3vw' }}></span>
                    Contacto
                </Button>

            </Link>
            <Link to='/dashboard/support'>
                <Button className='buttonA' style={{ paddingLeft: '0.5vw', paddingRight: '0.5vw' }}>
                    <span class="fas fa-external-link-alt" style={{ marginRight: '0.3vw' }}></span>
                    Soporte
                </Button>
            </Link>
            <ApolloProvider>
                {(cliente => {
                    return (
                        <Button
                            onClick={() => cerrarSesionUsuario(cliente, history)}
                            className='buttonA' style={{ paddingLeft: '0.5vw', paddingRight: '0.5vw' }}>
                            <span class="fas fa-door-open" style={{ marginRight: '0.3vw' }}></span>
                            Salir
                        </Button>
                    );
                })()
                }
            </ApolloProvider>
            {/* <Button
                // onClick={logout(),
                //     localStorage.setItem("token", ""),
                //     history.push('/')
                // }
                onClick={logout()}
                className='buttonA'
            //onClick={() => cerrarSesion()}
            >
                Salir
                </Button> */}


        </div >
    );
}

export default Form;