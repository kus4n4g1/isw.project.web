import React from 'react';
import Select from 'react-select';
import propTypes from 'prop-types';

const Group = (props) => {

    const {
        setGroup
    } = props;

    const options = [{ value: 'A', label: 'A' }, { value: 'B', label: 'B' }]

    return (
        <Select
            className='selectStyle'
            options={options}
            placeholder='Grupo'
            style={{ fontSize: '16px' }}
            menuPlacement="bottom"
            menuPosition="fixed"
            onChange={e => setGroup(e.value)}
        />
    );
};

Group.propTypes = {
    setGroup: propTypes.func
};

export default Group;