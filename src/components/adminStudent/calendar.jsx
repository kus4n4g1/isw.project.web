import React, { useState } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import propTypes from 'prop-types';

const Calendar = (propsS) => {

    const {
        value,
        handleChange,
        onChange,
        onBlur,
        errors,
        touched,
        props

    } = propsS;

    // const [selectedDate, setSelectedDate] = useState(null);

    // const handleChange = (selectedOption) => {
    //     console.log('It will never happen');
    // };
    const [selectedTime, setSelectedTime] = useState(null);

    const handleTimeChange = (selectedTime, value) => {
        // console.log('Esto es selectedTime' + selectedTime);
        props.setFieldValue('birthday', selectedTime);
        setSelectedTime(selectedTime);
    };

    return (

        <DatePicker
            className='timeStyleX'
            //style={{ fontSize: '16px', marginRight: '3vw' }}
            selected={props.values.birthday}
            errors={errors}
            touched={touched}
            onBlur={onBlur}
            onChange={selectedOption => {
                handleTimeChange(selectedOption);
                console.log("values", value);
            }}
            dateFormat='dd/MM/yyyy'
            maxDate={new Date()}
            dropdownMode={"select"}
            scrollableMonthYearDropdown={true}
            placeholderText='Nacimiento'
            showYearDropdown
            scrollableYearDropdown
        />

    );

}
Calendar.propTypes = {
    value: propTypes.object,
    handleChange: propTypes.func,
    onChange: propTypes.func,
    onBlur: propTypes.func,
    errors: propTypes.object,
    touched: propTypes.object,
    useField: propTypes.func,
    useFormikContext: propTypes.func,
    props: propTypes.object,
};

export default Calendar;

