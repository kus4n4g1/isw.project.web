import React, { useState, useContext, useEffect } from 'react';
import { Button } from 'reactstrap';
import Header from '../headerBar';
import Footer from '../footer';
import Semester from './semester';
import Group from './group';
import Calendar from './calendar';
import Gender from './gender';
import Club from './club';
import { Link } from 'react-router-dom';
import BloodType from './bloodType';
import { AllContext } from '../context/AllContext';
import { gql, useQuery, useLazyQuery, useMutation } from '@apollo/client';
import moment from 'moment';
import { Route, Redirect, useHistory } from "react-router-dom";
import Swal from 'sweetalert2';
import { Formik, useFormik, useField, useFormikContext } from 'formik';

import * as Yup from 'yup';

const NUEVO_ESTUDIANTE = gql`
mutation nuevoEstudiante($input:EstudianteInput){
    nuevoEstudiante(input:$input){
      id
      nombre
      paterno
      materno
      email
      nacimiento
      semestre
      genero
      curp
      sangre
      alergias
      padecimientos
      matricula
      creado
      grupo
    }
  }
`;
const ACTUALIZAR_GPO_EST = gql`
mutation actualizarGpoEst($id:ID!, $input:ActualizarGpoEstInput){
    actualizarGpoEst(id:$id,input:$input){
      id
      nombre
      paterno
      grupo
    }
  }
`;
// const AUTENTICAR_USUARIO = gql`
//     mutation autenticarUsuario($input:AutenticarInput){
//         autenticarUsuario(input:$input){
//         token
//         }
//     }
// `
const OBTENER_USUARIO_EMAIL = gql`
query obtenerUsuarioEmail($email:String!){
    obtenerUsuarioEmail(email:$email){
      id
      nombre
      email
    }
  }
`
const OBTENER_ESTUDIANTE_EMAIL = gql`
query obtenerEstudianteEmail($email:String!){
    obtenerEstudianteEmail(email:$email){
      id
      nombre
      email
      grupo
    }
  }
`

const Form = () => {
    let history = useHistory();
    const value = useContext(AllContext);
    const [idUsuario, setIdUsuario] = useState(null);
    const [idEstudiante, setIdEstudiante] = useState(null);
    const [newStudent, setNewStudent] = useState(false);

    //console.log(value.student);
    //console.log(value);
    const [nuevoEstudiante] = useMutation(NUEVO_ESTUDIANTE);
    const [actualizarGpoEst] = useMutation(ACTUALIZAR_GPO_EST);



    const getUser = useQuery(OBTENER_USUARIO_EMAIL, {
        variables: { email: value.email }
    });
    const [getStudent] = useLazyQuery(OBTENER_ESTUDIANTE_EMAIL, {
        variables: { email: value.email },
        onCompleted: data => {
            console.log('data ', data);
            console.log('data obtener estudiante email', data.obtenerEstudianteEmail.id);
            setIdEstudiante(data.obtenerEstudianteEmail.id);
            var ide = data.obtenerEstudianteEmail.id;
            console.log(ide);
            console.log(idUsuario);
            const { data2 } = actualizarGpoEst({
                variables: {
                    id: ide,
                    input: {
                        grupo: idUsuario,
                    }
                }
            });
            Swal.fire({
                title: 'Correcto',
                text: "El Estudiante se registró con éxito!",
                icon: 'success',
                showCancelButton: true,
                confirmButtonColor: '#9400D3',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Volver a Inicio',
                cancelButtonText: 'Seguir registrando'
            }).then((result) => {
                if (result.value) {
                    history.push('/dashboard');
                    Swal.fire(
                        'Correcto',
                        'El Estudiante se registró correctamente',
                        'success'
                    )
                } else {
                    history.push('/dashboard/admin/user')
                }
            })
        }
    });
    // const getStudent = useQuery(OBTENER_ESTUDIANTE_EMAIL, {
    //     variables: { email: value.email },
    //     skip: newStudent == false,
    // });

    // useEffect(() => {
    //     if (!getUser.loading && getUser.data) {
    //         getStudent();
    //     }
    // }, [getUser.data, getUser.loading]);
    //console.log("Esto es loadingY: " + loadingY);
    if (getUser.loading) return 'Cargando...';

    // if (dataY && dataY.obtenerUsuarioEmail) {
    //     console.log(dataY.obtenerUsuarioEmail.id);
    //     setIdUsuario(dataY.obtenerUsuarioEmail.id);
    //     console.log('Esto es id usuario' + idUsuario);
    // }
    // if (getStudent.loading) return 'Cargando...';

    // if (getStudent.loading && getStudent.data.obtenerEstudianteEmail) {
    //     setIdUsuario(getUser.data.obtenerUsuarioEmail.id);
    //     setIdEstudiante(getStudent.data.obtenerEstudianteEmail.id);
    //     console.log('Esto es id estudiante' + idEstudiante);
    //     console.log('Esto es id usuario' + idUsuario);
    // }






    //if (errorY) return 'Algo malo sucedió...:(';




    //console.log("Esto es obtener usuario por email: " + getUser.data.obtenerUsuarioEmail.id);
    //setIdUsuario(dataY.obtenerUsuarioEmail.id);

















    // const [name, setName] = useState('');
    // const [lastName, setLastName] = useState('');
    // const [lastName2, setLastName2] = useState('');
    // const [curp, setCurp] = useState('');
    // const [allergies, setAllergies] = useState('');
    // const [email, setEmail] = useState('');
    // const [semester, setSemester] = useState('');
    // const [group, setGroup] = useState('');
    // const [birthday, setBirthday] = useState(null);
    // const [gender, setGender] = useState('');
    // const [club, setClub] = useState([]);
    // const [bloodType, setBloodType] = useState('');
    // const [diseases, setDiseases] = useState('');
    // const [mensaje, setMensaje] = useState(null);
    // const [cargando, setCargando] = useState(true);
    // const [taller, setTaller] = useState(null);
    // const [loadOpt, loadOptions] = useState(null);

    //validación de formulario

    // var cantidad = 1;
    // var total = 1;

    // const updateName = (e) => {
    //     setName(e.target.value);
    // }

    // const updateCurp = (e) => {
    //     setCurp(e.target.value);
    // }

    // const updateLastName = (e) => {
    //     setLastName(e.target.value);
    // }

    // const handleSubmit = (e) => {
    //     e.preventDefault();
    //     value.setStudent(prevStudent => [...prevStudent, {
    //         name: name,
    //         lastName: lastName,
    //         lastName2: lastName2,
    //         email: email,
    //         birthday: birthday,
    //         semester: semester,
    //         gender: gender,
    //         club: club,
    //         curp: curp,
    //         bloodType: bloodType,
    //         allergies: allergies,
    //         diseases: diseases
    //     }]);
    //console.log(value.student)
    // }



    //const [nuevoPedido] = useMutation(NUEVO_PEDIDO);

    // console.log(data);
    // console.log(loading);
    // console.log(error);
    // console.log(dataR);
    // console.log(loadingR);
    // console.log(errorR);

    // var marvelHeroes;

    // if (!marvelHeroes == 3) {
    //     //Redireccionar
    //     history.push('/');
    //     //Mensaje de éxito                                
    //     Swal.fire({
    //         icon: 'error',
    //         title: 'Oops...',
    //         text: 'Ya tienes un taller registrado!',
    //         //footer: '<a href>Why do I have this issue?</a>'
    //     })
    // }

    // moment.locale();

    // const dateB = Date.now;
    // const formattedDate = moment(dateB).format("LL");




    // const mostrarMensaje = () => {
    //     return (
    //         <div className='headersGrid' >
    //             <p>{mensaje}</p>
    //         </div>
    //     );

    // }
    //Schema de validación
    const schemaValidacion = Yup.object({
        name: Yup.string()
            .required('Nombre es obligatorio*'),
        lastName: Yup.string()
            .required('Apellido paterno es obligatorio*'),
        lastName2: Yup.string()
            .required('Apellido materno es obligatorio*'),
        email: Yup.string()
            .email('El email no es válido')
            .required('Email es obligatorio*'),
        gender: Yup.string()
            .required('Género es obligatorio*'),
        birthday: Yup.date()
            .required('Fecha de nacimiento obligatoria*'),
        semester: Yup.string()
            .required('Semestre es obligatorio*'),
        enrollment: Yup.string()
            .required('Matrícula es obligatoria*')
            .matches(/^(\d{8})$/, {
                message: 'Matrícula en 8 dígitos',
                excludeEmptyString: true
            }),
        curp: Yup.string()
            .required('Curp es obligatorio*')
            .matches(/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/,
                {
                    message: 'Verifique el CURP',
                    excludeEmptyString: true
                }),
        bloodType: Yup.string()
            .required('Tipo de sangre es obligatorio*'),
        allergies: Yup.string()
            .required('Alergias es obligatorio*'),
        diseases: Yup.string()
            .required('Padecimientos es obligatorio*'),

    });

    const initialValues = {
        name: value.name,
        lastName: value.lastName,
        lastName2: '',
        email: value.email,
        gender: '',
        enrollment: '',
        birthday: '',
        bloodType: '',
        allergies: '',
        diseases: '',
        semester: '',
        curp: '',
    };

    return (
        <Formik
            validationSchema={schemaValidacion}
            enableReinitialize
            //initialvalues={ obtenerMaestros}
            initialValues={initialValues}
            onSubmit={async (valores, funciones) => {
                console.log('enviando');
                console.log('Entró la validación de formik!!!!!!!!');
                console.log('Esto es id de usuario' + getUser.data.obtenerUsuarioEmail.id);
                setIdUsuario(getUser.data.obtenerUsuarioEmail.id);
                //console.log(valores);
                const { name, lastName, lastName2, email, gender, enrollment, birthday, bloodType, allergies, diseases, semester, curp } = valores
                try {
                    //Crear nuevo estudiante
                    const { data } = await nuevoEstudiante({
                        variables: {
                            input: {
                                nombre: name,
                                paterno: lastName,
                                materno: lastName2,
                                email,
                                nacimiento: birthday,
                                semestre: semester,
                                genero: gender,
                                curp,
                                sangre: bloodType,
                                alergias: allergies,
                                padecimientos: diseases,
                                matricula: enrollment
                            }
                        }
                    })
                    if (data) {
                        getStudent();
                    }

                } catch (error) {
                    console.log(error);
                }

                // try {
                //     //Buscar por email el usuario
                //     //getUser();
                //     console.log("Este es idUsuario: " + idUsuario);

                // } catch (dataY) {
                //     console.log(dataY);
                // }
                // try {
                //     console.log("Entró penúltimo try catch");
                //     //Buscar por email el estudiante
                //     await idEstudiante;
                //     //getStudent();
                //     console.log("Este es idEstudiante: " + idEstudiante);


                // } catch (dataZ) {
                //     console.log(dataZ);
                // }
                // try {
                //     //Corregir el id de usuario por el correcto
                //     console.log("Last Try Catch");
                //     console.log("En lo último idEstudiante" + idEstudiante);
                //     console.log("En lo último id usuario" + idUsuario);
                //     if (idEstudiante != null) {
                //         const { data } = await actualizarGpoEst({
                //             variables: {
                //                 id: idEstudiante,
                //                 input: {
                //                     grupo: idUsuario,
                //                 }
                //             }
                //         });
                //         //Redireccionar a la pantalla de login
                //         Swal.fire({
                //             title: 'Correcto',
                //             text: "El Estudiante se registró con éxito! ",
                //             icon: 'success',
                //             confirmButtonColor: '#9400D3',
                //             cancelButtonColor: '#3085d6',
                //             confirmButtonText: 'Volver a Inicio',
                //             cancelButtonText: 'Seguir registrando',
                //         }).then((result) => {
                //             history.push('/');
                //             Swal.fire(
                //                 'Correcto',
                //                 'El Estudiante se registró correctamente',
                //                 'success'
                //             )

                //         })

                //     }



                // } catch (error) {

                // }

                // try {

                //     //Modificar el estudiante
                //     const { data } = await obtenerEstudianteEmail({
                //         variables: {
                //             input: {
                //                 email: value.email,
                //             }
                //         }
                //     });
                //     console.log(data);
                //     setIdEstudiante(data.obtenerEstudianteEmail.id);



                // } catch (error) {
                //     console.log(error);
                // }


            }}




        >
            {props => {

                //  console.log(props);
                return (
                    <form className="main2" onSubmit={props.handleSubmit} onKeyPress={e => { if (e.key === 'Enter') { e.preventDefault(); document.getElementById("submitBtn").click() } }}>
                        <Header />
                        <div className='content' >
                            <div className='headersGrid' >
                                <span class="fas fa-user-graduate" style={{ marginLeft: '-0.5vw', marginRight: '0.3vw' }}></span>
                                Registro de Estudiante
                            </div>
                            {/* {mensaje && mostrarMensaje()} */}
                            <div className='container1X'>
                                {props.touched.name && props.errors.name ? (
                                    <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '3.1vw', marginBottom: '-3vh' }}>
                                        <p>{props.errors.name}</p>
                                    </div>
                                ) : null}
                                <div className="input-group" style={{ marginLeft: '0.5vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>
                                    <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.5vw' }}><i className="fas fa-user-graduate"></i></span>
                                    <input
                                        id="name"
                                        type="text"
                                        className="form-control"
                                        name="name"
                                        value={value.name}
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        placeholder='Nombre' />
                                </div>
                                {props.touched.lastName && props.errors.lastName ? (
                                    <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '3.1vw', marginTop: '9.1vh' }}>
                                        <p>{props.errors.lastName}</p>
                                    </div>
                                ) : null}
                                <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', width: '16.9vw', marginTop: '3.35vh' }}>
                                    <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.3vw' }}> <i className="fas fa-parking"></i></span>
                                    <input
                                        id="lastName"
                                        type="text"
                                        className="form-control"
                                        name="lastName"
                                        value={value.lastName}
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        placeholder={'Apellido Paterno'} />
                                </div >
                                {props.touched.lastName2 && props.errors.lastName2 ? (
                                    <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '3.1vw', marginTop: '18.3vh' }}>
                                        <p>{props.errors.lastName2}</p>
                                    </div>
                                ) : null}
                                <div className="input-group" style={{ marginLeft: '0.5vw', paddingLeft: '0vw', paddingRight: '1vw', marginTop: '3.35vh', width: '17.3vw', marginRight: '1vw' }}>
                                    <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.5vw' }}><i className="fas fa-file-medical-alt"></i></span>
                                    <input
                                        id="lastName2"
                                        type="text"
                                        className="form-control"
                                        name="lastName2"
                                        value={props.values.lastName2}
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        placeholder={'Apellido Materno'} />
                                </div>
                                {props.touched.gender && props.errors.gender ? (
                                    <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '3.1vw', marginTop: '27.9vh' }}>
                                        <p>{props.errors.gender}</p>
                                    </div>
                                ) : null}
                                <div className="input-group " style={{ marginLeft: '0.7vw', paddingLeft: '0', paddingRight: '1vw', marginRight: '1vw', width: '22.2vw', marginTop: '3.35vh' }}>
                                    <span className="input-group-addon" style={{ color: 'purple', marginRight: '-0.2vw', marginLeft: '0.2vw' }}><i className="fas fa-venus-mars"></i></span>
                                    <Gender
                                        className="form-control"
                                        value={props.values.gender}
                                        handleChange={props.handleChange}
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        errors={props.errors}
                                        touched={props.touched}
                                        useField={useField}
                                        useFormikContext={useFormikContext}
                                        props={props}
                                    />
                                </div>
                                {props.touched.birthday && props.errors.birthday ? (
                                    <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '3.1vw', marginTop: '37.1vh' }}>
                                        <p>{props.errors.birthday}</p>
                                    </div>
                                ) : null}
                                <div className="input-group" style={{ marginLeft: '0.7vw', paddingLeft: '0', paddingRight: '1vw', marginRight: '0vw', width: '17.7vw', marginTop: '3.35vh' }}>
                                    <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.5vw' }}><i className="fas fa-birthday-cake"></i></span>
                                    <Calendar
                                        className="form-control"
                                        value={props.values.birthday}
                                        handleChange={props.handleChange}
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        errors={props.errors}
                                        touched={props.touched}
                                        useField={useField}
                                        useFormikContext={useFormikContext}
                                        props={props}
                                    />
                                </div>
                                {props.touched.allergies && props.errors.allergies ? (
                                    <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '3.1vw', marginTop: '46.3vh' }}>
                                        <p>{props.errors.allergies}</p>
                                    </div>
                                ) : null}
                                <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>
                                    <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.5vw' }}><i className="fas fa-allergies"></i></span>
                                    <input
                                        id="allergies"
                                        type="text"
                                        className="form-control"
                                        name="allergies"
                                        value={props.values.allergies}
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        placeholder={'Alergias'} />
                                </div>
                                <Button
                                    style={{ width: '10vw', height: '3.5vw', marginLeft: '7.9vw', marginTop: '3.35vh' }}
                                    type='submit'
                                    id="submitBtn"
                                // disabled={club.length == 0}
                                // onClick={async () => {
                                //     try {
                                //         //console.log(error);
                                //         console.log(mensaje);

                                //         //Redireccionar
                                //         history.push('/');
                                //         //Mensaje de éxito                                
                                //         Swal.fire(
                                //             'Correcto',
                                //             'El alumno se registró correctamente',
                                //             'success'
                                //         )

                                //     } catch (error) {
                                //         //setMensaje(error.message.replace('GraphQL error: ', ''));
                                //         console.log(mensaje);

                                //         setTimeout(() => {
                                //             setMensaje(null);
                                //         }, 3000);
                                //     }
                                // }
                                // }
                                //className={`${validarPedido()}`}
                                >
                                    Guardar
                        </Button>

                            </div>
                            <div className='container2X'>

                                {/* <div className="input-group" style={{ paddingLeft: '0.3vw', paddingRight: '1vw' }}>
                            <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                            <input id="semester" type="text" className="form-control" name="semester" placeholder="Semestre" />
                        </div> */}
                                {props.touched.email && props.errors.email ? (
                                    <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginBottom: '-3vh' }}>
                                        <p>{props.errors.email}</p>
                                    </div>
                                ) : null}
                                <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>
                                    <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-envelope"></i></span>
                                    <input
                                        id="email"
                                        type="text"
                                        className="form-control"
                                        name="email"
                                        value={value.email}
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        placeholder={'email'} />
                                </div>
                                {props.touched.curp && props.errors.curp ? (
                                    <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '9.1vh' }}>
                                        <p>{props.errors.curp}</p>
                                    </div>
                                ) : null}
                                <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>
                                    <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-atlas"></i></span>
                                    <input
                                        id="curp"
                                        type="text"
                                        className="form-control"
                                        name="curp"
                                        value={props.values.curp}
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        placeholder={'CURP'} />
                                </div>
                                {props.touched.semester && props.errors.semester ? (
                                    <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '18.7vh' }}>
                                        <p>{props.errors.semester}</p>
                                    </div>
                                ) : null}
                                <div className="input-group " style={{ marginLeft: '0.7vw', paddingLeft: '0', paddingRight: '1vw', marginRight: '1vw', width: '22.2vw', marginTop: '3.35vh' }}>
                                    <span className="input-group-addon" style={{ color: 'purple', marginRight: '-0.2vw', marginLeft: '0vw' }}><i className="fas fa-university"></i></span>
                                    <Semester
                                        className="form-control"
                                        value={props.values.semester}
                                        handleChange={props.handleChange}
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        errors={props.errors}
                                        touched={props.touched}
                                        useField={useField}
                                        useFormikContext={useFormikContext}
                                        props={props}
                                    />
                                </div>
                                {/* <div className="input-group" style={{ marginLeft: '0.7vw', paddingLeft: '0', paddingRight: '1vw', marginRight: '1vw', width: '18.3vw', marginBottom: '0vh', marginTop: '1vh' }}>
                            <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                            <Group style={{ position: 'absolute !important' }}
                                setGroup={setGroup} />
                        </div> */}

                                {/* <div className="input-group" style={{ marginTop: '0vh', marginLeft: '0.5vw', paddingLeft: '0.3vw', paddingRight: '1vw', marginRight: '1vw', width: '18.5vw' }}>
                            <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                            <input id="bloodType" type="text" className="form-control" name="bloodType" placeholder="Tipo de Sangre" />
                        </div> */}
                                {props.touched.enrollment && props.errors.enrollment ? (
                                    <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '27.9vh' }}>
                                        <p>{props.errors.enrollment}</p>
                                    </div>
                                ) : null}
                                <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '16.8vw' }}>
                                    <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-key"></i></span>
                                    <input
                                        id="enrollment"
                                        type="text"
                                        className="form-control"
                                        name="enrollment"
                                        value={props.values.enrollment}
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        placeholder={'Matrícula'}
                                    />
                                </div>
                                {props.touched.bloodType && props.errors.bloodType ? (
                                    <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '37.5vh' }}>
                                        <p>{props.errors.bloodType}</p>
                                    </div>
                                ) : null}
                                <div className="input-group " style={{ marginLeft: '0.7vw', paddingLeft: '0', paddingRight: '1vw', marginRight: '1vw', width: '22.2vw', marginTop: '3.25vh' }}>
                                    <span className="input-group-addon" style={{ color: 'purple', marginRight: '-0.2vw', marginLeft: '0.2vw' }}><i className="fas fa-syringe"></i></span>
                                    {/* <input
                                id="bloodType"
                                type="text"
                                className="form-control"
                                name="bloodType"
                                value={bloodType}
                                onChange={data => setBloodType(data.target.value)}
                                placeholder={'Tipo de Sangre'} 
                            /> */}
                                    <BloodType
                                        className="form-control"
                                        value={props.values.bloodType}
                                        handleChange={props.handleChange}
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        errors={props.errors}
                                        touched={props.touched}
                                        useField={useField}
                                        useFormikContext={useFormikContext}
                                        props={props}

                                    />
                                </div>
                                {props.touched.diseases && props.errors.diseases ? (
                                    <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '46.7vh' }}>
                                        <p>{props.errors.diseases}</p>
                                    </div>
                                ) : null}
                                <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.23vh', width: '16.8vw' }}>
                                    <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.3vw', marginLeft: '0.2vw' }}><i className="fas fa-disease"></i></span>
                                    <input
                                        id="diseases"
                                        type="text"
                                        className="form-control"
                                        name="diseases"
                                        value={props.values.diseases}
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        placeholder={'Padecimientos'} />
                                </div>

                                <Link to='/dashboard'>
                                    <Button style={{ marginLeft: '0.7vw', width: '10vw', height: '3.5vw', marginRight: '5vw', marginTop: '3.1vh' }}>Regresar</Button>
                                </Link>
                            </div>
                        </div>
                        <Footer />
                    </form >

                );
            }}
        </Formik>
    );
};

export default Form;
