import React from 'react';
import Select from 'react-select';
import propTypes from 'prop-types';
import { gql, useQuery } from '@apollo/client';
import AsyncSelect from 'react-select';
//import { colors } from 'react-select/src/theme';


const OBTENER_TALLERES = gql`
    query obtenerTalleres {
        obtenerTalleres {
        id
        nombre
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino
        maestro
        aula
        creado    
        }
    }

`;

const Club = (props) => {
    const {
        setClub,
        dataR,
        setCargando,
        setTaller,
        loadOptions,
    } = props;

    //console.log(dataR);
    // console.log(setClub);

    const handleChange = (e) => {
        setClub(e.value);
        console.log("This is it man" + e.value);
        setTaller(e.value);
        loadOptions({ variables: { id: e.value } });
        setCargando(false);
    }

    const options = [
        { value: 'chess', label: 'Ajedrez' },
        { value: 'painting', label: 'Pintura' },
        { value: 'reading', label: 'Lectura' },
        { value: 'write', label: 'Redacción' }
    ];

    const { data, loading, error } = useQuery(OBTENER_TALLERES);

    if (loading) return 'Cargando...';

    // const content = dataR.obtenerTalleres.map((taller) =>
    //     <div key={taller.id}>
    //         <h3 value={taller.nombre}>{taller.nombre}</h3>
    //     </div>
    // );

    // {
    //     DataR.keys(obtenerTalleres).map((nombre, i) => (
    //         <li className="travelcompany-input" key={i}>
    //             <span className="input-label">key: {i} Name: {obtenerTalleres[nombre]}</span>
    //         </li>
    //     ))
    // }
    // console.log(content);
    // console.log(Object.keys(dataR.obtenerTalleres));

    // Object.keys(dataR.obtenerTalleres).map(function (nombre, id) {
    //     // use keyName to get current key's name
    //     // and a[keyName] to get its value
    // })
    // let optionsF = Object.keys(dataR).forEach(item => {
    //     console.log(item);



    // })
    // var rows = [];
    // for (let value of Object.values(dataR.obtenerTalleres)) {
    //     console.log(value); // John, then 30
    //     // rows.push(value);
    //     rows.push(value);

    // }
    var rows2 = [];
    obj = {};
    for (let value of Object.values(dataR.obtenerTalleres)) {
        //console.log(value); // John, then 30
        // rows.push(value);
        var obj = {};

        obj['value'] = value.id;
        obj['label'] = value.nombre + ' - \nDisponible: ' + value.existencia;
        rows2.push(obj);

    }


    console.log(rows2);

    // options={dataR.obtenerTalleres.map(taller => (
    //     <li className='selectStyle3' key={taller.id} value={taller.nombre}>
    //         {taller.nombre}
    //         {console.log(taller.nombre)}
    //     </li>
    // ))}

    return (
        <Select
            options={rows2}
            placeholder='Elija Taller o Club...'
            className='selectStyle3'
            menuPlacement="bottom"
            menuPosition="fixed"
            onChange={handleChange}
            // getOptionValue={opciones => opciones.id}
            // getOptionLabel={opciones => opciones.nombre}
            // noOptionsMessage={() => "No hay resultados"}
            setClub={setClub}

        >

        </Select>
    )
}
Club.propTypes = {
    setClub: propTypes.func,
    dataR: propTypes.object,
    setCargando: propTypes.func,
    setTaller: propTypes.any,
    loadOptions: propTypes.func,
};

export default Club;