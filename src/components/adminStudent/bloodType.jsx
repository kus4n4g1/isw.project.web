import React, { useState } from 'react';
import Select from 'react-select';
import propTypes from 'prop-types';

const BloodType = (props) => {
    const {
        value,
        handleChange,
        onChange,
        onBlur,
        errors,
        touched,

    } = props;
    const [selectedBloodType, setSelectedBloodType] = useState("");

    const handleBloodTypeChange = (selectedBloodType, value) => {
        value = selectedBloodType.value;
        console.log(selectedBloodType);
        setSelectedBloodType(selectedBloodType);
    };
    const options = [
        { value: 'O+', label: 'O+' },
        { value: 'O-', label: 'O-' },
        { value: 'A+', label: 'A+' },
        { value: 'A-', label: 'A-' },
        { value: 'B+', label: 'B+' },
        { value: 'B-', label: 'B-' },
        { value: 'AB+', label: 'AB+' },
        { value: 'AB-', label: 'AB-' }
    ];
    return (
        <Select
            options={options}
            className='selectStyle3X'
            placeholder='Tipo de Sangre'
            errors={errors}
            touched={touched}
            onBlur={onBlur}
            onChange={selectedOption => {
                handleBloodTypeChange(selectedOption);
                console.log("values", value);
                handleChange("bloodType")(selectedOption.value);;
            }}
            menuPlacement="bottom"
            menuPosition="fixed"
        />
    )
}

BloodType.propTypes = {
    value: propTypes.object,
    handleChange: propTypes.func,
    onChange: propTypes.func,
    onBlur: propTypes.func,
    errors: propTypes.object,
    touched: propTypes.object,
}

export default BloodType;