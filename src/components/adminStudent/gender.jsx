import React, { useState } from 'react';
import Select from 'react-select';
import propTypes from 'prop-types';

const Gender = (props) => {

    const {
        value,
        handleChange,
        onChange,
        onBlur,
        errors,
        touched,
    } = props;
    const [selectedGender, setSelectedGender] = useState("");

    const handleGenderChange = (selectedGender, value) => {
        value = selectedGender.value;
        console.log(selectedGender);
        setSelectedGender(selectedGender);
    };
    const options = [
        { value: 'Masculino', label: 'Masculino' },
        { value: 'Femenino', label: 'Femenino' },
    ];

    return (
        <Select
            placeholder={'Seleccione el género '}
            className='selectStyle3X'
            options={options}
            style={{ fontSize: '16px' }}
            menuPlacement="auto"
            menuPosition="fixed"
            errors={errors}
            touched={touched}
            onBlur={onBlur}
            onChange={selectedOption => {
                handleGenderChange(selectedOption);
                console.log("values", value);
                handleChange("gender")(selectedOption.value);;
            }}

        // style={{ width: '18.2vw' }}
        />
    );
}

Gender.propTypes = {
    value: propTypes.object,
    handleChange: propTypes.func,
    onChange: propTypes.func,
    onBlur: propTypes.func,
    errors: propTypes.object,
    touched: propTypes.object,
};

export default Gender;