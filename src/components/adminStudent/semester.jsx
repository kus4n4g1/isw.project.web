import React, { useState } from 'react';
import Select from 'react-select';
import propTypes from 'prop-types';

const Semester = (props) => {

    const {
        value,
        handleChange,
        onChange,
        onBlur,
        errors,
        touched,
    } = props;
    const [selectedSemester, setSelectedSemester] = useState("");

    const handleSemesterChange = (selectedSemester, value) => {
        value = selectedSemester.value;
        console.log(selectedSemester);
        setSelectedSemester(selectedSemester);
    };
    const options = [{ value: 'I', label: 'I' }, { value: 'II', label: 'II' }, { value: 'III', label: 'III' }, { value: 'IV', label: 'IV' }, { value: 'V', label: 'V' }, { value: 'VI', label: 'VI' }]

    return (
        <Select
            className='selectStyle3X'
            options={options}
            placeholder='Semestre'
            style={{ fontSize: '16px' }}
            menuPlacement="bottom"
            menuPosition="fixed"
            errors={errors}
            touched={touched}
            onBlur={onBlur}
            onChange={selectedOption => {
                handleSemesterChange(selectedOption);
                console.log("values", value);
                handleChange("semester")(selectedOption.value);;
            }}
        />
    );
};

Semester.propTypes = {
    value: propTypes.object,
    handleChange: propTypes.func,
    onChange: propTypes.func,
    onBlur: propTypes.func,
    errors: propTypes.object,
    touched: propTypes.object,
};

export default Semester;