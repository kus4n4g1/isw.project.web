import React, { useState } from 'react';
import SideBar from '../sideBar';
import Header from '../headerBar';
import { gql, useQuery, useLazyQuery, useMutation } from '@apollo/client';
import { Button } from 'reactstrap';
import ReactTable from 'react-table';
import * as ReactBootstrap from 'react-bootstrap';
import Taller from '../registerStudent/taller';
// import { Glyphicon } from 'react-bootstrap';
// import "react-table/react-table.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import ModalStudent from '../modal/modalStudent';
import ModalStudentMod from '../modal/modalStudentMod';
//import AdminTeacher from '../adminTeachers';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import { useHistory } from "react-router-dom";
import jsPDF from 'jspdf';
import 'jspdf-autotable';

const OBTENER_CLUBES = gql`
query obtenerClubes {
    obtenerClubes {
    id
    nombre
    clave
    existencia
    dia
    fechaInicio
    fechaTermino
    horaInicio
    horaTermino
    maestro{
      id
      nombre
      paterno
      materno
      email
      telefono
      celular
      curp
      sangre
      alergias
      padecimientos
      creado
      grupo
    }
    aula
    creado    
    }
}
`;
const OBTENER_CLUBESPY = gql`
query obtenerClubes {
    obtenerClubes {    
    nombre
    clave
    existencia
    dia
    fechaInicio
    fechaTermino
    horaInicio
    horaTermino    
    aula
    }
}
`;
const OBTENER_CLUBES_FEMENINOS = gql`
query obtenerClubesFemeninos {
    obtenerClubesFemeninos {
    id
    nombre
    clave
    existencia
    dia
    fechaInicio
    fechaTermino
    horaInicio
    horaTermino
    maestro{
      id
      nombre
      paterno
      materno
      email
      telefono
      celular
      curp
      sangre
      alergias
      padecimientos
      creado
      grupo
    }
    aula
    creado    
    }
}
`;
const OBTENER_CLUBES_FEMENINOSPY = gql`
query obtenerClubesFemeninos {
    obtenerClubesFemeninos {
        nombre
        clave
        existencia
        dia
        fechaInicio
        horaInicio
        horaTermino    
        aula
    }
}
`;
const OBTENER_CLUBES_MASCULINOS = gql`
    query obtenerClubesMasculinos {
        obtenerClubesMasculinos {
        id
        nombre
        clave
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino
        maestro{
        id
        nombre
        paterno
        materno
        email
        telefono
        celular
        curp
        sangre
        alergias
        padecimientos
        creado
        grupo
        }
        aula
        creado    
        }
    }
`;
const OBTENER_CLUBES_MASCULINOSPY = gql`
    query obtenerClubesMasculinos {
        obtenerClubesMasculinos {
            nombre
            clave
            existencia
            dia
            fechaInicio
            fechaTermino
            horaInicio
            horaTermino    
            aula
        }
    }
`;
const OBTENER_CLUB = gql`
    query obtenerClub($id:ID!){
        obtenerClub(id:$id){
        id
        nombre
        clave
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino
        maestro{
            id
            nombre
            paterno
            materno
            email
            telefono
            celular
            curp
            sangre
            alergias
            padecimientos      
        }
        aula        
        }
    }
  `;

const OBTENER_CLUB_FEMENINO = gql`
    query obtenerClubFemenino($id:ID!){
        obtenerClubFemenino(id:$id){
        id
        nombre
        clave
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino
        maestro{
            id
            nombre
            paterno
            materno
            email
            telefono
            celular
            curp
            sangre
            alergias
            padecimientos      
        }
        aula        
        }
    }
  `;
const OBTENER_CLUB_MASCULINO = gql`
  query obtenerClubMasculino($id:ID!){
      obtenerClubMasculino(id:$id){
      id
      nombre
      clave
      existencia
      dia
      fechaInicio
      fechaTermino
      horaInicio
      horaTermino
      maestro{
          id
          nombre
          paterno
          materno
          email
          telefono
          celular
          curp
          sangre
          alergias
          padecimientos      
      }
      aula        
      }
  }
`;

const OBTENER_PEDIDOS = gql`
    query obtenerPedidos {
        obtenerPedidos{
        id
        estudiante{
            id
            nombre
            email
            semestre
        }
        estado
        }
    }  
`;
const OBTENER_PEDIDOS_TALLER = gql`
    query obtenerPedidosTaller($id:ID!) {
        obtenerPedidosTaller(id:$id){
        id
        pedido{
            id
            cantidad
        }
        estudiante{
            id
            nombre
            paterno
            materno
            email
            nacimiento
            semestre
            genero
            curp
            sangre
            alergias
            padecimientos     
        }
        total    
        }
    }
  `;
const OBTENER_ESTUDIANTE = gql`

  query obtenerEstudiante($id:ID!){
      obtenerEstudiante(id:$id){
        id
        nombre
        paterno
        materno
        email
        nacimiento
        semestre
        genero
        curp
        sangre
        alergias
        padecimientos
        matricula
      }
    }
  `;
const ELIMINAR_PEDIDO = gql`
  mutation eliminarPedido($id:ID!){
      eliminarPedido(id:$id)
  }  
`;
const ELIMINAR_CLUB = gql`
mutation eliminarClub($id:ID!){
    eliminarClub(id:$id)
}  
`;
const ELIMINAR_CLUB_FEMENINO = gql`
mutation eliminarClubFemenino($id:ID!){
    eliminarClubFemenino(id:$id)
}  
`;
const ELIMINAR_CLUB_MASCULINO = gql`
mutation eliminarClubMasculino($id:ID!){
    eliminarClubMasculino(id:$id)
}  
`;

const Dashboard = () => {
    let history = useHistory();
    const [club, setClub] = useState([]);
    const [cargando, setCargando] = useState(true);
    const [taller, setTaller] = useState(null);
    const [modal, setModal] = useState(null);
    const [studentX, setStudentX] = useState(null);
    const [modal2, setModal2] = useState(null);

    var p3d1d0 = null;

    //const [expanded, setExpanded] = useState();
    //const [defaultExpandedRows, setDefaultExpandedRow] = useState();


    // const columns = [
    //     {
    //         Header: "Nombre",
    //         accessor: "nombre"
    //     },
    //     {
    //         Header: "Cupo",
    //         accessor: "existencia"
    //     },
    //     {
    //         Header: "Maestro",
    //         accessor: "maestro"
    //     }
    // ];

    const [getStudentX] = useLazyQuery(OBTENER_ESTUDIANTE, {
        onCompleted: thisData => {

            p3d1d0 = thisData.obtenerEstudiante;
            setStudentX(p3d1d0);
            console.log('studentx', studentX)
            console.log('pedido', p3d1d0);
            setModal(true);
            setStudentX(p3d1d0);
        }
    });
    const [getStudentY] = useLazyQuery(OBTENER_ESTUDIANTE, {
        onCompleted: thisData => {

            p3d1d0 = thisData.obtenerEstudiante;
            setStudentX(p3d1d0);
            console.log('studentx', studentX)
            console.log('pedido', p3d1d0);
            setModal2(true);
            setStudentX(p3d1d0);
        }
    });
    const { data: dataC, loading: loadingC, error: errorC } = useQuery(OBTENER_CLUBES);

    const { data: dataPY, loading: loadingPY, error: errorPY } = useQuery(OBTENER_CLUBESPY);

    const { data: dataF, loading: loadingF, error: errorF } = useQuery(OBTENER_CLUBES_FEMENINOS);

    const { data: dataFY, loading: loadingFY, error: errorFY } = useQuery(OBTENER_CLUBES_FEMENINOSPY);


    const { data: dataM, loading: loadingM, error: errorM } = useQuery(OBTENER_CLUBES_MASCULINOS);

    const { data: dataMY, loading: loadingMY, error: errorMY } = useQuery(OBTENER_CLUBES_MASCULINOSPY);


    const { data, error, loading } = useQuery(OBTENER_PEDIDOS);

    const [eliminarClub] = useMutation(ELIMINAR_PEDIDO);

    const [eliminarClubFemenino] = useMutation(ELIMINAR_CLUB_FEMENINO);
    const [eliminarClubMasculino] = useMutation(ELIMINAR_CLUB_MASCULINO);
    const [eliminarClubO] = useMutation(ELIMINAR_CLUB);

    const [loadHeader, { data: dataC2, error: errorC2, loading: loadingC2 }] = useLazyQuery(OBTENER_CLUB, {
        variables: { id: taller },
    });

    const [loadHeader2, { data: dataF2, error: errorF2, loading: loadingF2 }] = useLazyQuery(OBTENER_CLUB_FEMENINO, {
        variables: { id: taller },
    });

    const [loadHeader3, { data: dataM2, error: errorM2, loading: loadingM2 }] = useLazyQuery(OBTENER_CLUB_MASCULINO, {
        variables: { id: taller },
    });

    const [loadOptions, { data: players, error: errorZ, loading: loadingZ }] = useLazyQuery(OBTENER_PEDIDOS_TALLER, {
        variables: { id: taller },
        // skip: { id: null }
    });
    if (loadingC || loadingF || loadingM || loadingZ || loadingPY || loadingFY || loadingMY) return 'Cargando...';

    console.log(players);
    // console.log(players.obtenerPedidosTaller);
    console.log('Este es Club: ' + club);
    console.log('Este es cargando' + cargando);



    //if (loading) return 'Cargando...';

    console.log(data);
    console.log(loading);
    console.log(error);

    var esTaller = false;
    var esClub = true;
    var genero = 'Femenino';
    var genero2 = 'Masculino';
    var placeToHold = "Elija Club...";

    const renderPlayer = (player, index) => {
        return (
            <tr key={index} >
                <td> {player.estudiante.nombre} </td>
                <td> {player.estudiante.paterno} </td>
                <td> {player.estudiante.materno} </td>
                <td> {player.estudiante.curp} </td>
                <td> {player.estudiante.email} </td>
                <td> {player.estudiante.semestre} </td>
                <td  >
                    <a style={{ color: '#6A5ACD', marginRight: '0.5vw' }} onClick={() => getStudentX({ variables: { id: player.estudiante.id } })}>
                        <span className="fas fa-eye"></span>
                        <ModalStudent
                            modal={modal}
                            setModal={setModal}
                            studentX={studentX}

                        />
                    </a>
                    {/* href="#" */}
                    <a style={{ color: '#9400D3', marginRight: '0.5vw' }} onClick={() => getStudentY({ variables: { id: player.estudiante.id } })}>
                        <span className="fas fa-edit"></span>
                        <ModalStudentMod
                            modal2={modal2}
                            setModal2={setModal2}
                            studentX={studentX}

                        />
                    </a>
                    <a style={{ color: 'red', marginRight: '0vw' }} onClick={() =>
                        Swal.fire({
                            title: '¿Deseas eliminar del Club este estudiante?',
                            text: "Esta acción no podrá deshacerse.",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: 'red',
                            cancelButtonColor: '#9400D3',
                            confirmButtonText: 'Sí, Elimínalo',
                            cancelButtonText: 'No, Cancelar'
                        }).then(async (result) => {
                            if (result) {
                                try {
                                    console.log("justo antes de eliminar" + player.estudiante.id);
                                    p3d1d0 = player.id;
                                    console.log("3st3 3s p3d1d0", p3d1d0);
                                    const { data } = await eliminarClub({
                                        variables: {
                                            id: player.id
                                        },
                                        refetchQueries: [
                                            {
                                                query: OBTENER_PEDIDOS_TALLER,
                                                variables: { id: p3d1d0 }
                                            }
                                        ]

                                    })
                                    renderPlayer();
                                } catch (error) {
                                    p3d1d0 = player.id;
                                    console.log("3st3 3s p3d1d0", p3d1d0);
                                    console.log("justo antes de eliminar" + player.id);
                                    console.log(error);
                                }
                                if (result.value) {
                                    Swal.fire(
                                        'Eliminado',
                                        "El estudiante se ha eliminado exitosamente",
                                        'success'
                                    )
                                }
                                // if (dataC2) {
                                //     try {
                                //         console.log("justo antes de eliminar" + player.estudiante.id);
                                //         p3d1d0 = player.id;
                                //         console.log("3st3 3s p3d1d0", p3d1d0);
                                //         const { data } = await eliminarClub({
                                //             variables: {
                                //                 id: player.id
                                //             },
                                //             refetchQueries: [
                                //                 {
                                //                     query: OBTENER_PEDIDOS_TALLER,
                                //                     variables: { id: player.id }
                                //                 }
                                //             ]

                                //         })
                                //         renderPlayer();
                                //     } catch (error) {
                                //         p3d1d0 = player.id;
                                //         console.log("3st3 3s p3d1d0", p3d1d0);
                                //         console.log("justo antes de eliminar" + player.id);
                                //         console.log(error);
                                //     }
                                //     if (result.value) {
                                //         history.push('/dashboard/taller');
                                //         Swal.fire(
                                //             'Eliminado',
                                //             "El estudiante se ha eliminado exitosamente",
                                //             'success'
                                //         )
                                //     }
                                // }
                                // if (dataM2) {
                                //     try {
                                //         console.log("justo antes de eliminar" + player.estudiante.id);
                                //         p3d1d0 = player.id;
                                //         console.log("3st3 3s p3d1d0", p3d1d0);
                                //         const { data } = await eliminarClubMasculino({
                                //             variables: {
                                //                 id: player.id
                                //             },
                                //             refetchQueries: [
                                //                 {
                                //                     query: OBTENER_PEDIDOS_TALLER,
                                //                     variables: { id: player.id }
                                //                 }
                                //             ]

                                //         })
                                //         renderPlayer();
                                //     } catch (error) {
                                //         p3d1d0 = player.id;
                                //         console.log("3st3 3s p3d1d0", p3d1d0);
                                //         console.log("justo antes de eliminar" + player.id);
                                //         console.log(error);
                                //     }
                                //     if (result.value) {
                                //         history.push('/dashboard/taller');
                                //         Swal.fire(
                                //             'Eliminado',
                                //             "El estudiante se ha eliminado exitosamente",
                                //             'success'
                                //         )
                                //     }
                                // }
                                // if (dataF2) {
                                //     try {
                                //         console.log("justo antes de eliminar" + player.estudiante.id);
                                //         p3d1d0 = player.id;
                                //         console.log("3st3 3s p3d1d0", p3d1d0);
                                //         const { data } = await eliminarClubFemenino({
                                //             variables: {
                                //                 id: player.id
                                //             },
                                //             refetchQueries: [
                                //                 {
                                //                     query: OBTENER_PEDIDOS_TALLER,
                                //                     variables: { id: player.id }
                                //                 }
                                //             ]

                                //         })
                                //         renderPlayer();
                                //     } catch (error) {
                                //         p3d1d0 = player.id;
                                //         console.log("3st3 3s p3d1d0", p3d1d0);
                                //         console.log("justo antes de eliminar" + player.id);
                                //         console.log(error);
                                //     }
                                //     if (result.value) {
                                //         history.push('/dashboard/taller');
                                //         Swal.fire(
                                //             'Eliminado',
                                //             "El estudiante se ha eliminado exitosamente",
                                //             'success'
                                //         )
                                //     }
                                // }

                            }

                        })

                    }>
                        <span className="fas fa-trash-alt"></span>
                    </a>
                </td>
            </tr>
        )
    }

    const headerReady = () => {
        if (dataC2) {
            return (
                <tr style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>
                    <th colSpan='5' style={{ fontSize: '22px', paddingBottom: '3vh' }}>{dataC2.obtenerClub.nombre}</th>
                    <th colSpan='1' style={{ paddingTop: '0', paddingBottom: '0' }}>
                        <tr>
                            Maestro:
                        </tr>
                        <tr style={{ fontSize: '12px' }}>
                            {dataC2.obtenerClub.maestro.nombre} {dataC2.obtenerClub.maestro.paterno} {dataC2.obtenerClub.maestro.materno}
                        </tr>
                        <tr style={{ fontSize: '12px' }}>
                            {dataC2.obtenerClub.aula}
                        </tr>
                    </th>
                    <th colSpan='1' style={{ paddingTop: '0', paddingBottom: '0' }}>
                        {/* <tr style={{marginBottom: '0.5vw'}}>
                        Opciones
                    </tr> */}
                        <tr>
                            <a style={{ color: '#9400D3', marginRight: '0.5vw' }} href="#">
                                <span className="fas fa-eye"></span>
                            </a>
                            <a style={{ color: '#9400D3', marginRight: '0.5vw' }} href="#">
                                <span className="fas fa-edit"></span>
                            </a>
                            <a style={{ color: 'red', marginRight: '0vw' }} href="#"


                            >
                                <span className="fas fa-trash-alt"></span>
                            </a>

                        </tr>
                    </th>
                </tr>
            );
        }
        if (dataF2) {
            return (
                <tr>
                    <th colSpan='5' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)', fontSize: '22px', paddingBottom: '3vh' }}>{dataF2.obtenerClubFemenino.nombre}</th>
                    <th colSpan='1' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)', paddingTop: '0', paddingBottom: '0', paddingRight: '0' }}>
                        <tr>
                            Maestro:
                        </tr>
                        <tr style={{ fontSize: '12px' }}>
                            {dataF2.obtenerClubFemenino.maestro.nombre} {dataF2.obtenerClubFemenino.maestro.paterno} {dataF2.obtenerClubFemenino.maestro.materno}
                        </tr>
                        <tr style={{ fontSize: '12px' }}>
                            {dataF2.obtenerClubFemenino.aula}
                        </tr>
                    </th>
                    <th colSpan='1' style={{ paddingTop: '0', paddingBottom: '0' }}>
                        {/* <tr style={{marginBottom: '0.5vw'}}>
                        Opciones
                    </tr> */}
                        <tr>
                            <a style={{ color: '#9400D3', marginRight: '0.5vw' }} href="#">
                                <span className="fas fa-eye"></span>
                            </a>
                            <a style={{ color: '#9400D3', marginRight: '0.5vw' }} href="#">
                                <span className="fas fa-edit"></span>
                            </a>
                            <a style={{ color: 'red', marginRight: '0vw' }} href="#"


                            >
                                <span className="fas fa-trash-alt"></span>
                            </a>

                        </tr>
                    </th>
                </tr>
            );
        }
        if (dataM2) {
            return (
                <tr>
                    <th colSpan='5' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)', fontSize: '22px', paddingBottom: '3vh' }}>{dataM2.obtenerClubMasculino.nombre}</th>
                    <th colSpan='1' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)', paddingTop: '0', paddingBottom: '0' }}>
                        <tr>
                            Maestro:
                        </tr>
                        <tr style={{ fontSize: '12px' }}>
                            {dataM2.obtenerClubMasculino.maestro.nombre} {dataM2.obtenerClubMasculino.maestro.paterno} {dataM2.obtenerClubMasculino.maestro.materno}
                        </tr>
                        <tr style={{ fontSize: '12px' }}>
                            {dataM2.obtenerClubMasculino.aula}
                        </tr>
                    </th>
                    <th colSpan='1' style={{ paddingTop: '0', paddingBottom: '0' }}>
                        {/* <tr style={{marginBottom: '0.5vw'}}>
                        Opciones
                    </tr> */}
                        <tr>
                            <a style={{ color: '#9400D3', marginRight: '0.5vw' }} href="#">
                                <span className="fas fa-eye"></span>
                            </a>
                            <a style={{ color: '#9400D3', marginRight: '0.5vw' }} href="#">
                                <span className="fas fa-edit"></span>
                            </a>
                            <a style={{ color: 'red', marginRight: '0vw' }} href="#"


                            >
                                <span className="fas fa-trash-alt"></span>
                            </a>

                        </tr>
                    </th>
                </tr>
            );
        }

    }
    const myBodyIsReady = () => {
        if (players[0] == []) {
            return (
                <tr>
                    Aún no hay alumnos registrados en este Club.
                </tr>
            );

        } else {
            return (
                players.obtenerPedidosTaller.map(renderPlayer)
            );

        }

    }
    var resultXY = Object.values(dataPY.obtenerClubes);
    var resultFY = Object.values(dataFY.obtenerClubesFemeninos);
    var resultMY = Object.values(dataMY.obtenerClubesMasculinos);

    var arrClubsXY = [];
    for (var i = 0; i < resultXY.length; i++) {
        arrClubsXY.push(Object.values(resultXY[i]));
    }
    for (var i = 0; i < resultFY.length; i++) {
        arrClubsXY.push(Object.values(resultFY[i]));
    }
    for (var i = 0; i < resultMY.length; i++) {
        arrClubsXY.push(Object.values(resultMY[i]));
    }

    var columns = [
        { title: "Tipo", dataKey: "Tipo" },
        { title: "Nombre", dataKey: "Nombre" },
        { title: "Clave", dataKey: "Clave" },
        { title: "Cupo", dataKey: "Cupo" },
        { title: "Día", dataKey: "Día" },
        { title: "Inicio", dataKey: "Inicio" },
        { title: "Término", dataKey: "Término" },
        // { title: "T.Sangre", dataKey: "Sangre" },
        // { title: "Alergias", dataKey: "Alergias" },
        // { title: "Padecimientos", dataKey: "Padecimientos" },
    ];

    // const tablePDF = () => {
    //     var doc = new jsPDF('p', 'pt');
    //     doc.autoTable(columns, arrClubsXY);
    //     doc.save('tableReady.pdf');
    //     // const doc = new jsPDF();

    //     // doc.autoTable({ tableReady });

    //     // doc.save('tableReady.pdf');
    // }
    const tableReady = () => {
        if (cargando === true || taller === null) {
            return (
                <div className='dashboardHeader' style={{ marginLeft: '0.5vw', marginTop: '0.7vh', borderRadius: '3px', width: '18.5vw', backgroundImage: 'linear-gradient(to right, #8A2BE2, #DA70D6)', opacity: '0.9', textAlign: 'center' }}>
                    <p>No hay aún datos para mostrar</p>
                </div>
            );
        }
        return (
            <ReactBootstrap.Table striped bordered hover style={{ backgroundColor: 'white', marginBottom: '0vh' }}>
                <thead style={{ fontWeight: '500px', font: 'Roboto', color: 'white', backgroundImage: 'linear-gradient(to right, #BA55D3, #DDA0DD)' }}>
                    {headerReady()}
                    <tr>
                        <th>Nombre</th>
                        <th>Paterno</th>
                        <th>Materno</th>
                        <th>CURP</th>
                        <th>Email</th>
                        <th>Semestre</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    {myBodyIsReady()}
                </tbody>
            </ReactBootstrap.Table>
        );
    }
    return (
        <div className='main'>
            <Header />
            <SideBar />
            <div className='content' style={{ marginLeft: '0vw' }}>
                <Taller
                    club={club}
                    setClub={setClub}
                    dataC={dataC}
                    dataF={dataF}
                    dataM={dataM}

                    setTaller={setTaller}
                    setCargando={setCargando}
                    loadHeader={loadHeader}
                    loadHeader2={loadHeader2}
                    loadHeader3={loadHeader3}
                    loadOptions={loadOptions}
                    esTaller={esTaller}
                    esClub={esClub}
                    genero={genero}
                    genero2={genero2}
                    placeToHold={placeToHold}
                />
                <div style={{ display: 'flex', flexDirection: 'column', marginLeft: '1vw' }}>
                    {Taller && tableReady()}
                    <div style={{ display: 'flex', marginLeft: '-0.3vw', marginTop: '1vh' }}>
                        <Button style={{ borderRadius: '3px', opacity: '0.9', width: '10vw !important', paddingLeft: "0px !important", paddingRight: "0px !important", height: '8vh' }}
                            onClick={() => {
                                var doc = new jsPDF('p', 'pt');
                                doc.autoTable(columns, arrClubsXY);
                                doc.save('tableReady.pdf');
                            }}
                        >
                            <span class="fas fa-print"></span> Reporte
                        </Button>
                        <Link to='/dashboard'>
                            <Button style={{ borderRadius: '3px', opacity: '0.9', width: '10vw !important', paddingLeft: "0px !important", paddingRight: "0px !important", height: '8vh' }}
                            // onClick={tablePDF()}
                            >
                                <span class="fas fa-home"></span> Regresar
                            </Button>
                        </Link>
                    </div>

                </div>
            </div>
            <div className='footer'>
                <div>
                    <span class="fas fa-university" style={{ marginRight: '0.3vw' }}></span>
                    Copyright © 2020, B3st0 Team
                </div>
            </div>
        </div>
    );

}

export default Dashboard;