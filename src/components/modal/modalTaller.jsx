import React, { useState } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Button } from 'reactstrap';
import propTypes from 'prop-types';


const ModalTaller = (props) => {
    const {
        modal,
        setModal
    } = props;


    console.log(modal);
    return (
        <>
            <Modal className='modal-dialog-centered' style={{ left: '9vw', top: '-5vh', opacity: '0.9' }} isOpen={modal} toggle={modal} size='lg' contentEditable='false' >
                <ModalHeader>Hi</ModalHeader>
                <ModalBody>
                    <form id='myForm' className="container-fluid" >
                        <div>
                            <div className='modalFlex'>
                                <div>
                                    Hola
                                </div>
                                <div>
                                    que
                                </div>
                                <div>
                                    hace
                                </div>
                            </div>
                            <div className='modalFlex'>
                                <div className="input-group" style={{ marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '2vh', }}>
                                    <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                                    <input
                                        id="studentName"
                                        type="text"
                                        className="form-control"
                                    />
                                </div>
                                <div className="input-group" style={{ marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '2vh', }}>
                                    <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                                    <input
                                        id="studentName"
                                        type="text"
                                        className="form-control"
                                    />
                                </div>
                                <div className="input-group" style={{ marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '2vh', }}>
                                    <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                                    <input
                                        id="studentName"
                                        type="text"
                                        className="form-control"
                                    />
                                </div>
                            </div>
                            <div className='modalFlex'>
                                <div className="input-group" style={{ marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '2vh', width: '17.7vw' }}>
                                    <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                                    <input
                                        id="studentName"
                                        type="text"
                                        className="form-control"
                                    />
                                </div>
                                <div className="input-group" style={{ marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '2vh', width: '17.7vw' }}>
                                    <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                                    <input
                                        id="studentName"
                                        type="text"
                                        className="form-control"
                                    />
                                </div>
                                <div className="input-group" style={{ marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '2vh', width: '17.7vw' }}>
                                    <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                                    <input
                                        id="studentName"
                                        type="text"
                                        className="form-control"
                                    />
                                </div>
                            </div>

                            <div className='modalFlex'>
                                <div className="input-group" style={{ marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '2vh', width: '17.7vw' }}>
                                    <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                                    <input
                                        id="studentName"
                                        type="text"
                                        className="form-control"
                                    />
                                </div>
                                <div className="input-group" style={{ marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '2vh', width: '17.7vw' }}>
                                    <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                                    <input
                                        id="studentName"
                                        type="text"
                                        className="form-control"
                                    />
                                </div>
                                <div className="input-group" style={{ marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '2vh', width: '17.7vw' }}>
                                    <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                                    <input
                                        id="studentName"
                                        type="text"
                                        className="form-control"
                                    />
                                </div>
                            </div>

                        </div>


                    </form>
                </ModalBody>
                <ModalFooter>
                    <Button onClick={() => setModal(false)} type='submit' color='primary'>Guardar</Button>
                    <Button onClick={() => setModal(false)} color='secondary'>Cancelar</Button>
                </ModalFooter>
            </Modal>
        </>

    );
};

ModalTaller.propTypes = {
    modal: propTypes.bool,
    setModal: propTypes.func
};

export default ModalTaller;