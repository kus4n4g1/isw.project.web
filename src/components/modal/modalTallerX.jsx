import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import propTypes from 'prop-types';

import React, { useState, useContext } from 'react';
import { Button } from 'reactstrap';
import Header from '../headerBar';
import Footer from '../footer';
import Semester from '../adminStudent/semester';
import Group from '../adminStudent/group';
import Calendar from '../adminStudent/calendar';
import Gender from '../adminStudent/gender';
import Club from '../adminStudent/club';
import { Link } from 'react-router-dom';
import BloodType from '../adminStudent/bloodType';
import { AllContext } from '../context/AllContext';
import { gql, useQuery, useMutation } from '@apollo/client';
import moment from 'moment';
import { Route, Redirect, useHistory } from "react-router-dom";
import Swal from 'sweetalert2';
import { Formik, useFormik, useField, useFormikContext } from 'formik';

import * as Yup from 'yup';

const OBTENER_ALUMNO = gql`
query obtenerEstudiante($id:ID!){
    obtenerEstudiante(id:$id){
      id
      nombre
      paterno
      materno
      email
      nacimiento
      semestre
      genero
      curp
      sangre
      alergias
      padecimientos
      matricula    
    }
  }
`;

const ModalTallerX = (props) => {
    const {
        modal,
        setModal,
        studentX,
        setStudentX,
        player,
    } = props;

    //console.log("on open modal", studentX);

    let history = useHistory();
    const value = useContext(AllContext);
    //console.log(value.student);
    const [name, setName] = useState('');
    const [lastName, setLastName] = useState('');
    const [lastName2, setLastName2] = useState('');
    const [curp, setCurp] = useState('');
    const [allergies, setAllergies] = useState('');
    const [email, setEmail] = useState('');
    const [semester, setSemester] = useState('');
    const [group, setGroup] = useState('');
    const [birthday, setBirthday] = useState(null);
    const [gender, setGender] = useState('');
    const [club, setClub] = useState([]);
    const [bloodType, setBloodType] = useState('');
    const [diseases, setDiseases] = useState('');
    const [mensaje, setMensaje] = useState(null);
    const [cargando, setCargando] = useState(true);
    const [taller, setTaller] = useState(null);
    const [loadOpt, loadOptions] = useState(null);
    const [loadHead, loadHeader] = useState(null);
    const [loadHead2, loadHeader2] = useState(null);
    const [loadHead3, loadHeader3] = useState(null);


    const { data, error, loading } = useQuery(OBTENER_ALUMNO);

    var genero = "";
    var cantidad = 1;
    var total = 1;

    const updateName = (e) => {
        setName(e.target.value);
    }

    const updateCurp = (e) => {
        setCurp(e.target.value);
    }

    const updateLastName = (e) => {
        setLastName(e.target.value);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        value.setStudent(prevStudent => [...prevStudent, {
            name: name,
            lastName: lastName,
            lastName2: lastName2,
            email: email,
            birthday: birthday,
            semester: semester,
            gender: gender,
            club: club,
            curp: curp,
            bloodType: bloodType,
            allergies: allergies,
            diseases: diseases
        }]);
        //console.log(value.student)
    }



    //const [nuevoPedido] = useMutation(NUEVO_PEDIDO);

    // console.log(data);
    // console.log(loading);
    // console.log(error);
    // console.log(dataR);
    // console.log(loadingR);
    // console.log(errorR);

    var marvelHeroes;

    if (!marvelHeroes == 3) {
        //Redireccionar
        history.push('/');
        //Mensaje de éxito                                
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Ya tienes un taller registrado!',
            //footer: '<a href>Why do I have this issue?</a>'
        })
    }

    moment.locale();

    const dateB = Date.now;
    const formattedDate = moment(dateB).format("LL");


    const mostrarMensaje = () => {
        return (
            <div className='headersGrid' >
                <p>{mensaje}</p>
            </div>
        );

    }
    var realName = '';
    var realLastName = '';
    var realLastName2 = '';
    var realEmail = '';
    var realBirthDay = '';
    var realSemester = '';
    var realGender = '';
    var realCurp = '';
    var realBloodType = '';
    var realAllergies = '';
    var realDiseases = '';
    var realEnrollment = '';




    if (studentX != null) {

        realName = studentX.nombre;
        realLastName = studentX.paterno;
        realLastName2 = studentX.materno
        realEmail = studentX.email
        realBirthDay = studentX.nacimiento
        realSemester = studentX.semestre
        realGender = studentX.genero
        realCurp = studentX.curp
        realBloodType = studentX.sangre
        realAllergies = studentX.alergias
        realDiseases = studentX.padecimientos
        realEnrollment = studentX.matricula
    }
    //console.log(modal);
    return (
        <>
            <Modal style={{ left: '0vw', top: '8vh', fontFamily: 'Roboto' }} isOpen={modal} toggle={modal} size='lg' contentEditable='false' >
                <ModalHeader className='headersGridModal' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>
                    <span className="fas fa-eye" > Ver Detalles</span>
                </ModalHeader>
                <ModalBody>
                    <form id='myForm' className="container-fluid" style={{ display: 'flex', flexDirection: 'row', paddingLeft: '0vw', paddingRight: '0vw', marginBottom: '0vh' }}>
                        <div className='container1X' style={{ marginRight: '0vw', paddingBottom: '3.35vh' }}>
                            <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Nombre</div>
                            <div className="input-group" style={{ marginLeft: '0.5vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0vh', width: '17vw' }}>

                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.5vw' }}><i className="fas fa-user-graduate"></i></span>
                                <input
                                    id="studentName"
                                    type="text"
                                    className="form-control"
                                    name="name"
                                    //onChange={updateName}
                                    //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.nombre)}
                                    value={realName}
                                />
                            </div>
                            <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Apellido Paterno</div>
                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', width: '16.9vw', marginTop: '0' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.3vw' }}> <i className="fas fa-parking"></i></span>
                                <input
                                    id="lastName"
                                    type="text"
                                    className="form-control"
                                    name="lastName"
                                    value={realLastName}
                                //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.paterno)} 
                                />
                            </div>
                            <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Apellido Materno</div>

                            <div className="input-group" style={{ marginLeft: '0.5vw', paddingLeft: '0vw', paddingRight: '1vw', marginTop: '0vh', width: '17.3vw', marginRight: '1vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.7vw', marginLeft: '0.3vw' }}><i className="fas fa-file-medical-alt"></i></span>
                                <input
                                    id="lastName2"
                                    type="text"
                                    className="form-control"
                                    name="lastName2"
                                    onChange={e => setLastName2(e.target.value)}
                                    // value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.materno)}
                                    value={realLastName2}
                                />
                            </div>
                            <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Género</div>

                            <div className="input-group" style={{ marginLeft: '0.5vw', paddingLeft: '0vw', paddingRight: '1vw', marginTop: '0vh', width: '17.3vw', marginRight: '1vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.2vw', marginBottom: '3.35vh' }}><i className="fas fa-venus-mars"></i></span>
                                <input
                                    id="gender"
                                    type="text"
                                    className="form-control"
                                    name="gender"
                                    value={realGender}
                                //onChange={data => setGender(data.target.value)}

                                />
                            </div>



                        </div>
                        <div className='container2X'>

                            {/* <div className="input-group" style={{ paddingLeft: '0.3vw', paddingRight: '1vw' }}>
                        <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                        <input id="semester" type="text" className="form-control" name="semester" placeholder="Semestre" />
                    </div> */}
                            <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Email</div>

                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0vh', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-envelope"></i></span>
                                <input
                                    id="email"
                                    type="text"
                                    className="form-control"
                                    name="email"
                                    //onChange={data => setEmail(data.target.value)}
                                    //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.email)} 
                                    value={realEmail}
                                />
                            </div>
                            <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Curp</div>

                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0vh', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-atlas"></i></span>
                                <input
                                    id="curp"
                                    type="text"
                                    className="form-control"
                                    name="curp"
                                    //onChange={updateCurp}
                                    //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.curp)} 
                                    value={realCurp}

                                />
                            </div>
                            <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Semestre</div>

                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0vh', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-university"></i></span>
                                <input
                                    id="semester"
                                    type="text"
                                    className="form-control"
                                    name="semester"
                                    // onChange={data => setSemester(data.target.value)}
                                    //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.semestre)} 
                                    value={realSemester}
                                />
                            </div>

                            <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Tipo de Sangre</div>

                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '-0.1vw', marginBottom: '3.35vh' }}><i className="fas fa-syringe"></i></span>
                                <input
                                    id="bloodType"
                                    type="text"
                                    className="form-control"
                                    name="bloodType"
                                    // onChange={data => setBloodType(data.target.value)}
                                    //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.sangre)} 
                                    value={realBloodType}
                                />
                            </div>
                            {/* <div className="input-group" style={{ marginTop: '0vh', marginLeft: '0.5vw', paddingLeft: '0.3vw', paddingRight: '1vw', marginRight: '1vw', width: '18.5vw' }}>
                        <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                        <input id="bloodType" type="text" className="form-control" name="bloodType" placeholder="Tipo de Sangre" />
                    </div> */}


                        </div>
                        <div className='container2X'>

                            {/* <div className="input-group" style={{ paddingLeft: '0.3vw', paddingRight: '1vw' }}>
<span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
<input id="semester" type="text" className="form-control" name="semester" placeholder="Semestre" />
</div> */}
                            <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Fecha de Nacimiento</div>

                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.3vw' }}><i className="fas fa-birthday-cake"></i></span>
                                <input
                                    id="birthday"
                                    type="text"
                                    className="form-control"
                                    name="birthday"
                                    value={realBirthDay}
                                    // onChange={data => setBirthday(data.target.value)}
                                    //placeholder={formattedDate} 
                                    value={realBirthDay}
                                />
                            </div>
                            <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Alergias</div>

                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.3vw' }}><i className="fas fa-allergies"></i></span>
                                <input
                                    id="allergies"
                                    type="text"
                                    className="form-control"
                                    name="allergies"
                                    // onChange={e => setAllergies(e.target.value)}
                                    //  value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.alergias)} 
                                    value={realAllergies}
                                />
                            </div>
                            <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Padecimientos</div>

                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-disease"></i></span>
                                <input
                                    id="diseases"
                                    type="text"
                                    className="form-control"
                                    name="diseases"
                                    // onChange={e => setDiseases(e.target.value)}
                                    // value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.padecimientos)} 
                                    value={realDiseases}
                                />
                            </div>
                            {/* <div className="input-group" style={{ marginTop: '0vh', marginLeft: '0.5vw', paddingLeft: '0.3vw', paddingRight: '1vw', marginRight: '1vw', width: '18.5vw' }}>
<span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
<input id="bloodType" type="text" className="form-control" name="bloodType" placeholder="Tipo de Sangre" />
</div> */}
                            <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Matrícula</div>

                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '1vw', paddingRight: '0.6vw', marginTop: '0', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '-0.1vw', marginBottom: '3.35vh' }}><i className="fas fa-key"></i></span>
                                <input
                                    id="enrollment"
                                    type="text"
                                    className="form-control"
                                    name="enrollment"
                                    // onChange={data => setBloodType(data.target.value)}
                                    //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.sangre)} 
                                    value={realEnrollment}
                                />
                            </div>
                        </div>
                    </form>
                </ModalBody>
                <ModalFooter style={{ padding: '0', height: '10vh' }}>
                    <Button onClick={() => setModal(false)} type='toggle' style={{ marginLeft: '6.9vw', width: '10vw', height: '3.5vw', marginRight: '1vw', marginTop: '1vh', marginBottom: '1vh' }}>Regresar</Button>

                    {/* <Button onClick={() => setModal(false)} type='submit' color='primary'>Guardar</Button>
                    <Button onClick={() => setModal(false)} color='secondary'>Cancelar</Button> */}
                </ModalFooter>
            </Modal>
        </>

    );
};

ModalStudent.propTypes = {
    modal: propTypes.bool,
    setModal: propTypes.func,
    setStudentX: propTypes.func,
    studentX: propTypes.object
};

export default ModalTallerX;