import React, { useState } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Button } from 'reactstrap';
import propTypes from 'prop-types';


const ModalSD = (props) => {
    const {
        modal,
        setModal
    } = props;


    console.log(modal);
    return (
        <>
            <Modal className='modal-dialog-centered' style={{ left: '9vw', top: '-5vh', opacity: '0.9' }} isOpen={modal} toggle={modal} size='lg' contentEditable='false' >
                <ModalHeader>Hi</ModalHeader>
                <ModalBody>
                    <form id='myForm' className="container-fluid" style={{ display: 'flex', flexDirection: 'row', paddingLeft: '0vw', paddingRight: '0vw' }}>
                        <div className='container1X' style={{ marginRight: '0vw' }}>
                            <div className="input-group" style={{ marginLeft: '0.5vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.5vw' }}><i className="fas fa-user-graduate"></i></span>
                                <input
                                    id="studentName"
                                    type="text"
                                    className="form-control"
                                    name="name"
                                //onChange={updateName}
                                //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.nombre)}
                                />
                            </div>
                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', width: '16.9vw', marginTop: '3.35vh' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.3vw' }}> <i className="fas fa-parking"></i></span>
                                <input
                                    id="lastName"
                                    type="text"
                                    className="form-control"
                                    name="lastName"
                                //onChange={updateLastName}
                                //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.paterno)} 
                                />
                            </div>
                            <div className="input-group" style={{ marginLeft: '0.5vw', paddingLeft: '0vw', paddingRight: '1vw', marginTop: '3.35vh', width: '17.3vw', marginRight: '1vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.7vw', marginLeft: '0.3vw' }}><i className="fas fa-file-medical-alt"></i></span>
                                <input
                                    id="lastName2"
                                    type="text"
                                    className="form-control"
                                    name="lastName2"
                                //onChange={e => setLastName2(e.target.value)}
                                // value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.materno)}
                                />
                            </div>
                            <div className="input-group" style={{ marginLeft: '0.5vw', paddingLeft: '0vw', paddingRight: '1vw', marginTop: '3.35vh', width: '17.3vw', marginRight: '1vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.2vw' }}><i className="fas fa-venus-mars"></i></span>
                                <input
                                    id="gender"
                                    type="text"
                                    className="form-control"
                                    name="gender"
                                // value={genero}
                                //onChange={data => setGender(data.target.value)}

                                />
                            </div>
                            <div className="input-group" style={{ marginLeft: '0.7vw', paddingLeft: '0', paddingRight: '1vw', marginRight: '0vw', width: '17.3vw', marginTop: '3.35vh' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.3vw' }}><i className="fas fa-birthday-cake"></i></span>
                                <input
                                    id="birthday"
                                    type="text"
                                    className="form-control"
                                    name="birthday"
                                //  value={formattedDate}
                                //onChange={data => setBirthday(data.target.value)}
                                //placeholder={formattedDate} 
                                />
                            </div>
                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0vw', paddingRight: '1vw', marginTop: '3.35vh', width: '17.3vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.3vw' }}><i className="fas fa-allergies"></i></span>
                                <input
                                    id="allergies"
                                    type="text"
                                    className="form-control"
                                    name="allergies"
                                //  onChange={e => setAllergies(e.target.value)}
                                //  value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.alergias)} 
                                />
                            </div>



                        </div>
                        <div className='container2X'>

                            {/* <div className="input-group" style={{ paddingLeft: '0.3vw', paddingRight: '1vw' }}>
                        <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                        <input id="semester" type="text" className="form-control" name="semester" placeholder="Semestre" />
                    </div> */}
                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-envelope"></i></span>
                                <input
                                    id="email"
                                    type="text"
                                    className="form-control"
                                    name="email"
                                //  onChange={data => setEmail(data.target.value)}
                                //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.email)} 
                                />
                            </div>
                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-atlas"></i></span>
                                <input
                                    id="curp"
                                    type="text"
                                    className="form-control"
                                    name="curp"
                                //  onChange={updateCurp}
                                //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.curp)} 
                                />
                            </div>
                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-university"></i></span>
                                <input
                                    id="semester"
                                    type="text"
                                    className="form-control"
                                    name="semester"
                                //onChange={data => setSemester(data.target.value)}
                                //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.semestre)} 
                                />
                            </div>


                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-university"></i></span>
                                <input
                                    id="semester"
                                    type="text"
                                    className="form-control"
                                    name="semester"
                                //  onChange={data => setSemester(data.target.value)}
                                // value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.semestre)}
                                />
                            </div>
                            {/* <div className="input-group" style={{ marginTop: '0vh', marginLeft: '0.5vw', paddingLeft: '0.3vw', paddingRight: '1vw', marginRight: '1vw', width: '18.5vw' }}>
                        <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                        <input id="bloodType" type="text" className="form-control" name="bloodType" placeholder="Tipo de Sangre" />
                    </div> */}
                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '1vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '-0.1vw' }}><i className="fas fa-syringe"></i></span>
                                <input
                                    id="bloodType"
                                    type="text"
                                    className="form-control"
                                    name="bloodType"
                                //  onChange={data => setBloodType(data.target.value)}
                                //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.sangre)} 
                                />
                            </div>
                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.23vh', width: '16.8vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-disease"></i></span>
                                <input
                                    id="diseases"
                                    type="text"
                                    className="form-control"
                                    name="diseases"
                                //  onChange={e => setDiseases(e.target.value)}
                                // value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.padecimientos)} 
                                />
                            </div>


                        </div>
                        <div className='container2X'>

                            {/* <div className="input-group" style={{ paddingLeft: '0.3vw', paddingRight: '1vw' }}>
<span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
<input id="semester" type="text" className="form-control" name="semester" placeholder="Semestre" />
</div> */}
                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-envelope"></i></span>
                                <input
                                    id="email"
                                    type="text"
                                    className="form-control"
                                    name="email"
                                //   onChange={data => setEmail(data.target.value)}
                                //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.email)} 
                                />
                            </div>
                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-atlas"></i></span>
                                <input
                                    id="curp"
                                    type="text"
                                    className="form-control"
                                    name="curp"
                                // onChange={updateCurp}
                                //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.curp)} 
                                />
                            </div>
                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-university"></i></span>
                                <input
                                    id="semester"
                                    type="text"
                                    className="form-control"
                                    name="semester"
                                //   onChange={data => setSemester(data.target.value)}
                                //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.semestre)} 
                                />
                            </div>


                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-university"></i></span>
                                <input
                                    id="semester"
                                    type="text"
                                    className="form-control"
                                    name="semester"
                                // onChange={data => setSemester(data.target.value)}
                                // value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.semestre)}
                                />
                            </div>
                            {/* <div className="input-group" style={{ marginTop: '0vh', marginLeft: '0.5vw', paddingLeft: '0.3vw', paddingRight: '1vw', marginRight: '1vw', width: '18.5vw' }}>
<span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
<input id="bloodType" type="text" className="form-control" name="bloodType" placeholder="Tipo de Sangre" />
</div> */}
                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '1vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '-0.1vw' }}><i className="fas fa-syringe"></i></span>
                                <input
                                    id="bloodType"
                                    type="text"
                                    className="form-control"
                                    name="bloodType"
                                // onChange={data => setBloodType(data.target.value)}
                                //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.sangre)} 
                                />
                            </div>
                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.23vh', width: '16.8vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-disease"></i></span>
                                <input
                                    id="diseases"
                                    type="text"
                                    className="form-control"
                                    name="diseases"
                                //  onChange={e => setDiseases(e.target.value)}
                                // value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.padecimientos)} 
                                />
                            </div>
                        </div>
                    </form>
                </ModalBody>
                <ModalFooter>
                    <Button onClick={() => setModal(false)} type='submit' style={{ marginLeft: '6.9vw', width: '10vw', height: '3.5vw', marginRight: '0vw', marginTop: '3.5vh', marginBottom: '3.5vh' }}>Regresar</Button>
                    <Button onClick={() => setModal(false)} color='secondary'>Cancelar</Button>
                </ModalFooter>
            </Modal>
        </>

    );
};

ModalSD.propTypes = {
    modal: propTypes.bool,
    setModal: propTypes.func
};

export default ModalSD;