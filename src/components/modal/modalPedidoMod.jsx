import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import propTypes from 'prop-types';

import React, { useState, useContext } from 'react';
import { Button } from 'reactstrap';
import Header from '../headerBar';
import Footer from '../footer';
import Semester from '../adminStudent/semester';
import Group from '../adminStudent/group';
import Calendar from '../adminStudent/calendar';
import Gender from '../adminStudent/gender';
import Club from '../adminStudent/club';
import { Link } from 'react-router-dom';
import BloodType from '../adminStudent/bloodType';
import { AllContext } from '../context/AllContext';
import { gql, useQuery, useMutation, useLazyQuery } from '@apollo/client';
import moment from 'moment';
import { Route, Redirect, useHistory } from "react-router-dom";
import Swal from 'sweetalert2';
import { Formik, useFormik, useField, useFormikContext } from 'formik';
import Taller from '../registerStudentX/taller';

import * as Yup from 'yup';

const OBTENER_ESTUDIANTES_USUARIO = gql`
  query obtenerEstudiantesUsuario{
       obtenerEstudiantesUsuario{
            id
            nombre
            paterno
            materno
            email
            nacimiento
            semestre
            genero
            curp    
            sangre
            alergias
            padecimientos
            matricula
        }
    }    
`;
const OBTENER_TALLERES = gql`
query obtenerTalleres {
    obtenerTalleres {
    id
    nombre
    existencia
    dia
    fechaInicio
    fechaTermino
    horaInicio
    horaTermino
    maestro{
      id
      nombre
      paterno
      materno
      email
      telefono
      celular
      curp
      sangre
      alergias
      padecimientos
      creado
      grupo
    }
    aula
    creado    
    }
}
  
`;
const NUEVO_PEDIDO = gql`
    mutation nuevoPedido($input: PedidoInput) {
        nuevoPedido(input: $input) {
        id        
        }
    }
`;
const ACTUALIZAR_PEDIDO = gql`
mutation actualizarPedido($id:ID!,$input: PedidoInput) {
    actualizarPedido(id:$id,input: $input) {
      id
      estudiante{      
        nombre
        paterno
        materno
      }
      pedido {
        id
        cantidad
      }
      total
      grupo
      estado
    }
  }
  `;
const ACTUALIZAR_HISTORIAL = gql`
  mutation actualizarHistorial($id:ID!,$input: HistorialInput) {
    actualizarHistorial(id:$id,input: $input) {
      id
      estudiante
      pedido {
        id
        cantidad
      }
      total
      grupo
      estado
    }
  }
  `;
const NUEVO_HISTORIAL = gql`
mutation nuevoHistorial($input: HistorialInput) {
    nuevoHistorial(input: $input) {
      id
      estudiante
      pedido {
        id
        cantidad
      }
      total
      grupo
      estado
    }
  }
`;

const OBTENER_PEDIDOS = gql`
    query obtenerPedidos {
        obtenerPedidos{
        id
        estudiante{
            id
            nombre
            email
            semestre
        }
        estado
        }
    }  
`;
const OBTENER_CLUBES = gql`
    query obtenerClubes {
        obtenerClubes {
        id
        nombre
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino
        maestro{
        id
        nombre
        paterno
        materno
        email
        telefono
        celular
        curp
        sangre
        alergias
        padecimientos
        creado
        grupo
        }
        aula
        creado    
        }
    }

`;

const OBTENER_CLUBES_FEMENINOS = gql`
    query obtenerClubesFemeninos {
        obtenerClubesFemeninos {
        id
        nombre
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino
        maestro{
        id
        nombre
        paterno
        materno
        email
        telefono
        celular
        curp
        sangre
        alergias
        padecimientos
        creado
        grupo
        }
        aula
        creado    
        }
    }

`;

const OBTENER_CLUBES_MASCULINOS = gql`
    query obtenerClubesMasculinos {
        obtenerClubesMasculinos {
        id
        nombre
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino
        maestro{
        id
        nombre
        paterno
        materno
        email
        telefono
        celular
        curp
        sangre
        alergias
        padecimientos
        creado
        grupo
        }
        aula
        creado    
        }
    }

`;

const OBTENER_TALLER = gql`
    query obtenerTaller($id:ID!){
        obtenerTaller(id:$id){
        id
        nombre
        clave
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino
        maestro{
            id
            nombre
            paterno
            materno
        }
        aula        
        }
    }
`;

const OBTENER_CLUB = gql`
    query obtenerClub($id:ID!){
        obtenerClub(id:$id){
        id
        nombre
        clave
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino
        maestro{
            id
            nombre
            paterno
            materno
        }
        aula        
        }
    }
`;

const OBTENER_CLUB_FEMENINO = gql`
    query obtenerClubFemenino($id:ID!){
        obtenerClubFemenino(id:$id){
        id
        nombre
        clave
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino
        maestro{
            id
            nombre
            paterno
            materno
        }
        aula        
        }
    }
`;

const OBTENER_CLUB_MASCULINO = gql`
    query obtenerClubMasculino($id:ID!){
        obtenerClubMasculino(id:$id){
        id
        nombre
        clave
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino
        maestro{
            id
            nombre
            paterno
            materno
        }
        aula        
        }
    }
`;

const OBTENER_HISTORIAL = gql`
    query obtenerHistorialUsuario{
        obtenerHistorialUsuario{
        id
        pedido{
            id
            cantidad
        }
        total
        estudiante
        grupo
        fecha
        estado
        }
    }
  `;

const ModalPedido = (props) => {
    const {
        modal4,
        setModal4,
        dataH,
        player,
        club,
        setClub,
        studentX,
    } = props;
    let history = useHistory();
    const value = useContext(AllContext);
    //console.log('Value.Student', value.student);

    const [mensaje, setMensaje] = useState(null);
    const [mensajeX, setMensajeX] = useState(null);

    const [cargando, setCargando] = useState(true);
    const [taller, setTaller] = useState(null);
    const [loadOpt, loadOptions] = useState(null);
    const [loadHead, loadHeader] = useState(null);
    const [loadHead2, loadHeader2] = useState(null);
    const [loadHead3, loadHeader3] = useState(null);
    //const [studentX, setStudentX] = useState(null);
    //const [club, setClub] = useState([]);
    const clubH = club;
    const [email, setEmail] = useState('');
    const [lastName2, setLastName2] = useState('');
    const [realPrefix, setRealPrefix] = useState(null);
    const [realName, setRealName] = useState(null);
    const [realKey, setRealKey] = useState(null);
    const [realQuota, setRealQuota] = useState(null);
    const [realDay, setRealDay] = useState(null);
    const [realScheduleStart, setRealScheduleStart] = useState(null);
    const [realScheduleEnd, setRealScheduleEnd] = useState(null);
    const [realCalendarStart, setRealCalendarStart] = useState(null);
    const [realCalendarEnd, setRealCalendarEnd] = useState(null);
    const [realTeacherName, setRealTeacherName] = useState(null);
    const [realTeacherLastName, setRealTeacherLastName] = useState(null);
    const [realTeacherLastName2, setRealTeacherLastName2] = useState(null);
    const [realClassroom, setRealClassroom] = useState(null);

    console.log('Este es clubHHH', clubH);

    // var realKey = '';
    // var realQuota = 555;
    // var realDay = '';
    // var realScheduleStart = '';
    // var realScheduleEnd = '';
    // var realCalendarStart = '';
    // var realcalendarEnd = '';
    // var realTeacherName = '';
    // var realTeacherLastName = '';
    // var realTeacherLastName2 = '';
    // var realClassroom = '';

    var cantidad = 1;
    var total = 1;



    const validarPedido = () => {
        // console.log(club);
        return (club => club === 0) ? " opacity-50 cursor-not-allowed " : "";
    }

    const { data, loading, error } = useQuery(OBTENER_ESTUDIANTES_USUARIO);

    //(const { data: dataH, loading: loadingH, error: errorH } = useQuery(OBTENER_HISTORIAL);

    const { data: dataP, error: errorP, loading: loadingP } = useQuery(OBTENER_PEDIDOS);

    const { data: dataR, error: errorR, loading: loadingR } = useQuery(OBTENER_TALLERES);

    const { data: dataC, loading: loadingC, error: errorC } = useQuery(OBTENER_CLUBES);

    const { data: dataF, loading: loadingF, error: errorF } = useQuery(OBTENER_CLUBES_FEMENINOS);

    const { data: dataM, loading: loadingM, error: errorM } = useQuery(OBTENER_CLUBES_MASCULINOS);

    // const [getHistorial] = useLazyQuery(OBTENER_HISTORIAL, {
    //     onCompleted: thisData => {
    //         console.log('Si estoy llegando al historial o no?')
    //         if (thisData.total == 2) {
    //             console.log('Talleres concluidos');
    //             history.push('/');
    //             //Mensaje de éxito                                
    //             Swal.fire(
    //                 'Error',
    //                 'Ya concluiste con tus talleres',
    //                 'error'
    //             )
    //         }
    //         if (thisData.pedido[0].id == taller) {
    //             Swal.fire(
    //                 'Error',
    //                 'Ya cursaste este taller/club',
    //                 'error'
    //             )
    //         } else {
    //             console.log('Hola que hace dentro de historial')
    //             history.push('/');
    //             //Mensaje de éxito                                
    //             Swal.fire(
    //                 'Correcto',
    //                 'El taller se registró correctamente',
    //                 'success'
    //             )
    //             nuevoPedido({
    //                 variables: { input: { pedido: [{ id: club, cantidad: cantidad }], total: total, estudiante: data.obtenerEstudiantesUsuario[0].id, estado: "COMPLETADO" } }
    //             });

    //         }

    //     }
    // });

    const [nuevoHistorial] = useMutation(NUEVO_HISTORIAL);


    const [nuevoPedido] = useMutation(NUEVO_PEDIDO);

    const [actualizarPedido] = useMutation(ACTUALIZAR_PEDIDO);

    const [actualizarHistorial] = useMutation(ACTUALIZAR_HISTORIAL);

    //console.log(data);
    // console.log(loading);
    // console.log(error);
    // console.log(dataR);
    // console.log(loadingR);
    // console.log(errorR);

    const [getTaller] = useLazyQuery(OBTENER_TALLER, {
        onCompleted: thisData => {
            // console.log('esto es thisData', thisData);
            // console.log('esto es thisData', thisData.obtenerTaller.maestro.nombre);
            setRealPrefix('Taller de ');
            setRealName(thisData.obtenerTaller.nombre);
            setRealKey(thisData.obtenerTaller.clave);
            setRealDay(thisData.obtenerTaller.dia);
            setRealQuota(thisData.obtenerTaller.existencia);
            setRealScheduleStart(thisData.obtenerTaller.horaInicio);
            setRealScheduleEnd(thisData.obtenerTaller.horaTermino);
            setRealCalendarStart(thisData.obtenerTaller.fechaInicio);
            setRealCalendarEnd(thisData.obtenerTaller.fechaTermino);
            setRealTeacherName(thisData.obtenerTaller.maestro.nombre);
            setRealTeacherLastName(thisData.obtenerTaller.maestro.paterno);
            setRealTeacherLastName2(thisData.obtenerTaller.maestro.materno);
            setRealClassroom(thisData.obtenerTaller.aula);
            // realKey = thisData.obtenerTaller.clave;
            // realDay = thisData.obtenerTaller.dia;
            // realScheduleStart = thisData.obtenerTaller.horaInicio;
            // realScheduleEnd = thisData.obtenerTaller.horaTermino;
            // realCalendarStart = thisData.obtenerTaller.fechaInicio;
            // realCalendarEnd = thisData.obtenerTaller.fechaTermino;
            // realTeacherName = thisData.obtenerTaller.maestro.nombre;
            // realTeacherLastName = thisData.obtenerTaller.maestro.paterno;
            // realTeacherLastName2 = thisData.obtenerTaller.maestro.materno;
            // realClassroom = thisData.obtenerTaller.aula;
            console.log('esto es quota', realQuota);

            setMensajeX(true);



        }
    });

    const [getClub] = useLazyQuery(OBTENER_CLUB, {
        onCompleted: thisData => {
            console.log('esto es thisData', thisData);
            console.log('esto es thisData', thisData.obtenerClub.maestro.nombre);
            setRealPrefix('Club de ');
            setRealName(thisData.obtenerClub.nombre);
            setRealKey(thisData.obtenerClub.clave);
            setRealDay(thisData.obtenerClub.dia);
            setRealQuota(thisData.obtenerClub.existencia);
            setRealScheduleStart(thisData.obtenerClub.horaInicio);
            setRealScheduleEnd(thisData.obtenerClub.horaTermino);
            setRealCalendarStart(thisData.obtenerClub.fechaInicio);
            setRealCalendarEnd(thisData.obtenerClub.fechaTermino);
            setRealTeacherName(thisData.obtenerClub.maestro.nombre);
            setRealTeacherLastName(thisData.obtenerClub.maestro.paterno);
            setRealTeacherLastName2(thisData.obtenerClub.maestro.materno);
            setRealClassroom(thisData.obtenerClub.aula);
            setMensajeX(true);

        }
    });

    const [getClubFemenino] = useLazyQuery(OBTENER_CLUB_FEMENINO, {
        onCompleted: thisData => {
            console.log('esto es thisData', thisData);
            console.log('esto es thisData', thisData.obtenerClubFemenino.maestro.nombre);
            setRealPrefix('Club de ');
            setRealName(thisData.obtenerClubFemenino.nombre);
            setRealKey(thisData.obtenerClubFemenino.clave);
            setRealDay(thisData.obtenerClubFemenino.dia);
            setRealQuota(thisData.obtenerClubFemenino.existencia);
            setRealScheduleStart(thisData.obtenerClubFemenino.horaInicio);
            setRealScheduleEnd(thisData.obtenerClubFemenino.horaTermino);
            setRealCalendarStart(thisData.obtenerClubFemenino.fechaInicio);
            setRealCalendarEnd(thisData.obtenerClubFemenino.fechaTermino);
            setRealTeacherName(thisData.obtenerClubFemenino.maestro.nombre);
            setRealTeacherLastName(thisData.obtenerClubFemenino.maestro.paterno);
            setRealTeacherLastName2(thisData.obtenerClubFemenino.maestro.materno);
            setRealClassroom(thisData.obtenerClubFemenino.aula);
            setMensajeX(true);


        }
    });

    const [getClubMasculino] = useLazyQuery(OBTENER_CLUB_MASCULINO, {
        onCompleted: thisData => {
            console.log('esto es thisData', thisData);
            console.log('esto es thisData', thisData.obtenerClubMasculino.maestro.nombre);
            setRealPrefix('Club de ');
            setRealName(thisData.obtenerClubMasculino.nombre);
            setRealKey(thisData.obtenerClubMasculino.clave);
            setRealDay(thisData.obtenerClubMasculino.dia);
            setRealQuota(thisData.obtenerClubMasculino.existencia);
            setRealScheduleStart(thisData.obtenerClubMasculino.horaInicio);
            setRealScheduleEnd(thisData.obtenerClubMasculino.horaTermino);
            setRealCalendarStart(thisData.obtenerClubMasculino.fechaInicio);
            setRealCalendarEnd(thisData.obtenerClubMasculino.fechaTermino);
            setRealTeacherName(thisData.obtenerClubMasculino.maestro.nombre);
            setRealTeacherLastName(thisData.obtenerClubMasculino.maestro.paterno);
            setRealTeacherLastName2(thisData.obtenerClubMasculino.maestro.materno);
            setRealClassroom(thisData.obtenerClubMasculino.aula);
            setMensajeX(true);


        }
    });

    if (loading || loadingP || loadingF || loadingR || loadingM || loadingC) return 'Cargando...';


    //console.log('Esto es data jejeje', data.obtenerEstudiantesUsuario.id);
    //setHistorialX(data.obtenerEstudiantesUsuario.id);

    //console.log('Este es datahHistorial', dataH.obtenerHistorialUsuario[0].pedido[0].id);
    //console.log('Este es taller para ver si son iguales', taller);
    // console.log("Esto es obtener estudiantes al inicio: " + data.obtenerEstudiantesUsuario.email);
    // console.log("Esto es obtener pedidos al inicio: " + dataP);
    // console.log("Esto es talleres al inicio: " + dataR);
    // console.log("Esto es clubes generales al inicio: " + dataC);
    // console.log("Esto es clubes femeninos al inicio: " + dataF);
    // console.log("Esto es clubes masculinos al inicio: " + dataM);


    // console.log('Esto es dataH al inicio', dataH)
    var result1 = data.hasOwnProperty(email);
    // console.log(result1);
    // console.log(data);

    var resultX = Object.values(data.obtenerEstudiantesUsuario);

    var result2 = Object.values(dataP.obtenerPedidos);
    var result3 = Object.entries(dataP.obtenerPedidos);
    var result4 = Object.values(dataC.obtenerClubes);
    // console.log("Esto es result2", result2);
    // console.log(result3);
    // console.log(result4);

    var marvelHeroes = result2.filter(function (hero) {
        //console.log('Esto es hero estudiante email' + hero.estudiante.email)
        return hero.estudiante.email === resultX[0].email;
    });
    // console.log("MArvel heroes", marvelHeroes);
    // console.log("MArvel heroes length", marvelHeroes.length);

    // Validación de taller en curso
    // if (marvelHeroes.length > 0) {
    //     //Redireccionar
    //     history.push('/');
    //     //Mensaje de éxito                                
    //     Swal.fire({
    //         icon: 'error',
    //         title: 'Oops...',
    //         text: 'Ya tienes un taller registrado!',
    //         //footer: '<a href>Why do I have this issue?</a>'
    //     })
    // }

    // function filterItems(query) {
    //     return result2.filter(function (el) {
    //         return el.query;
    //     })
    // }

    // console.log(filterItems('estudiante')); // ['apple', 'grapes']
    // console.log(filterItems('estado')); // ['banana', 'mango', 'orange']


    // const dateB = Date(data.obtenerEstudiantesUsuario.map(estudiante => estudiante.nacimiento));
    // const dateString = Date(data.obtenerEstudiantesUsuario.map(estudiante => estudiante.nacimiento)).toString();

    const dateB = Date(data.obtenerEstudiantesUsuario.map(estudiante => estudiante.nacimiento)).toString();
    var formattedDate = moment(data.obtenerEstudiantesUsuario.map(estudiante => estudiante.nacimiento)).format("LL");
    //console.log(dateB);
    moment.locale('es');
    var formattedDate = moment(data.obtenerEstudiantesUsuario.map(estudiante => estudiante.nacimiento)).locale('es').format("LLLL");

    var genero = "";
    //console.log("String vacío" + typeof genero);
    genero = (data.obtenerEstudiantesUsuario.map(estudiante => estudiante.genero));
    //moment.locale(false);
    genero = genero.toString();
    var genero2 = genero;
    var esTaller = true;
    var esClub = true;
    var placeToHold = "Elija Taller o Club...";

    const mostrarMensaje = () => {
        return (
            <div className='headersGrid' >
                <p>{mensaje}</p>
            </div>
        );

    }

    const mostrarB1 = () => {
        return (
            <>
                <div className='headersGrid' >
                    <p>Ya estas inscrito{mensaje}</p>
                </div>
                {/* <Button
                    style={{ width: '10vw', height: '3.5vw', marginLeft: '7.6vw', marginTop: '3.5vh' }}
                    type='submit'
                    disabled={club.length === 0}
                    onClick={async () => {
                        try {
                            //Redireccionar
                            history.push('/');
                            //Mensaje de éxito                                
                            Swal.fire(
                                'Correcto',
                                'El taller se registró correctamente',
                                'success'
                            )
                            await nuevoPedido({
                                variables: { input: { pedido: [{ id: club, cantidad: cantidad }], total: total, estudiante: data.obtenerEstudiantesUsuario[0].id, estado: "COMPLETADO" } }
                            });
                            console.log(error);
                            console.log(mensaje);



                        } catch (error) {
                            setMensaje(error.message.replace('GraphQL error: ', ''));
                            console.log(mensaje);

                            setTimeout(() => {
                                setMensaje(null);
                            }, 3000);
                        }
                    }
                    }
                //className={`${validarPedido()}`}
                >
                    Guardar
                </Button> */}
            </>

        );

    }
    // const mostrarB2 = () => {
    //     return (
    //         // <Button
    //         //     style={{ width: '10vw', height: '3.5vw', marginLeft: '7.6vw', marginTop: '3.5vh' }}
    //         //     type='submit'
    //         //     disabled={club.length === 0}
    //         //     onClick={async () => {
    //         //         try {
    //         //             //Redireccionar
    //         //             history.push('/');
    //         //             //Mensaje de éxito                                
    //         //             Swal.fire(
    //         //                 'Correcto',
    //         //                 'El taller se registró correctamente',
    //         //                 'success'
    //         //             )
    //         //             console.log(error);
    //         //             console.log(mensaje);


    //         //         } catch (error) {
    //         //             setMensaje(error.message.replace('GraphQL error: ', ''));
    //         //             console.log(mensaje);

    //         //             setTimeout(() => {
    //         //                 setMensaje(null);
    //         //             }, 3000);
    //         //         }
    //         //     }
    //         //     }
    //         // //className={`${validarPedido()}`}
    //         // >
    //         //     Salir
    //         // </Button>
    //         0
    //     );

    // }
    const mostrarContenido = () => {
        return (
            <form id='myForm' onSubmit={e => { e.preventDefault(); }} className="container-fluid" style={{ display: 'flex', flexDirection: 'row', paddingLeft: '0vw', paddingRight: '0vw', marginBottom: '0vh' }}>
                <div className='container1X' style={{ marginRight: '0vw', paddingBottom: '3.35vh' }}>
                    {/* <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Nombre</div>
                            <div className="input-group" style={{ marginLeft: '0.5vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0vh', width: '17vw' }}>

                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.5vw' }}><i className="fas fa-user-graduate"></i></span>
                                <input
                                    id="name"
                                    type="text"
                                    className="form-control"
                                    name="name"
                                    //onChange={updateName}
                                    value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.nombre)}
                                    value={realName}
                                />
                            </div> */}
                    {/* <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Apellido Paterno</div>
                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', width: '16.9vw', marginTop: '0' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.3vw' }}> <i className="fas fa-parking"></i></span>
                                <input
                                    id="teacherName"
                                    type="text"
                                    className="form-control"
                                    name="teacherName"
                                    value={realTeacherName}
                                //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.paterno)} 
                                />
                            </div> */}
                    <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Maestro</div>


                    <div className="input-group" style={{ marginLeft: '0.5vw', paddingLeft: '0vw', paddingRight: '1vw', marginTop: '0vh', width: '17.3vw', marginRight: '1vw' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.2vw' }}><i className="fas fa-graduation-cap"></i></span>
                        <input
                            id="teacher"
                            type="text"
                            className="form-control"
                            name="teacher"
                            value={realTeacherName + ' ' + realTeacherLastName + ' ' + realTeacherLastName2}
                        //onChange={data => setGender(data.target.value)}

                        />
                    </div>


                    <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Cupo</div>

                    <div className="input-group" style={{ marginLeft: '0.5vw', paddingLeft: '0vw', paddingRight: '1vw', marginTop: '0vh', width: '17.3vw', marginRight: '1vw' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.2vw' }}><i className="fas fa-venus-mars"></i></span>
                        <input
                            id="quota"
                            type="text"
                            className="form-control"
                            name="quota"
                            value={realQuota}
                        //onChange={data => setGender(data.target.value)}

                        />
                    </div>
                    <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Día a cursar</div>

                    <div className="input-group" style={{ marginLeft: '0.5vw', paddingLeft: '0vw', paddingRight: '1vw', marginTop: '0vh', width: '17.3vw', marginRight: '1vw' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.7vw', marginLeft: '0.3vw' }}><i className="fas fa-file-medical-alt"></i></span>
                        <input
                            id="day"
                            type="text"
                            className="form-control"
                            name="day"
                            //onChange={e => setLastName2(e.target.value)}
                            // value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.materno)}
                            value={realDay}
                        />
                    </div>


                </div>
                <div className='container2X'>

                    <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Aula</div>


                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0vh', width: '17vw' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-university"></i></span>
                        <input
                            id="classroom"
                            type="text"
                            className="form-control"
                            name="classroom"
                            //onChange={updateCurp}
                            //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.curp)} 
                            value={realClassroom}

                        />
                    </div>
                    {/* <div className="input-group" style={{ paddingLeft: '0.3vw', paddingRight: '1vw' }}>
                        <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                        <input id="semester" type="text" className="form-control" name="semester" placeholder="Semestre" />
                    </div> */}
                    {/* <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Email</div> */}

                    {/* <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0vh', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-envelope"></i></span>
                                <input
                                    id="teacher"
                                    type="text"
                                    className="form-control"
                                    name="teacher"
                                    //onChange={data => setEmail(data.target.value)}
                                    //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.email)} 
                                    value={realTeacherName}
                                />
                            </div> */}

                    <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Hora de inicio</div>

                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0vh', width: '17vw' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-atlas"></i></span>
                        <input
                            id="scheduleStart"
                            type="text"
                            className="form-control"
                            name="scheduleStart"
                            //onChange={updateCurp}
                            //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.curp)} 
                            value={realScheduleStart}

                        />
                    </div>
                    <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Hora de término</div>

                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0vh', width: '17vw' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-university"></i></span>
                        <input
                            id="scheduleEnd"
                            type="text"
                            className="form-control"
                            name="scheduleEnd"
                            // onChange={data => setSemester(data.target.value)}
                            //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.semestre)} 
                            value={realScheduleEnd}
                        />
                    </div>

                    {/* <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Tipo de Sangre</div>

                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '-0.1vw', marginBottom: '3.35vh' }}><i className="fas fa-syringe"></i></span>
                                <input
                                    id="bloodType"
                                    type="text"
                                    className="form-control"
                                    name="bloodType"
                                    // onChange={data => setBloodType(data.target.value)}
                                    //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.sangre)} 
                                    value={realBloodType}
                                />
                            </div> */}
                    {/* <div className="input-group" style={{ marginTop: '0vh', marginLeft: '0.5vw', paddingLeft: '0.3vw', paddingRight: '1vw', marginRight: '1vw', width: '18.5vw' }}>
                        <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                        <input id="bloodType" type="text" className="form-control" name="bloodType" placeholder="Tipo de Sangre" />
                    </div> */}


                </div>
                <div className='container2X'>

                    <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Clave</div>

                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0', width: '17vw' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.3vw' }}><i className="fas fa-key"></i></span>
                        <input
                            id="key"
                            type="text"
                            className="form-control"
                            name="key"
                            // onChange={e => setAllergies(e.target.value)}
                            //  value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.alergias)} 
                            value={realKey}
                        />
                    </div>
                    {/* <div className="input-group" style={{ paddingLeft: '0.3vw', paddingRight: '1vw' }}>
<span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
<input id="semester" type="text" className="form-control" name="semester" placeholder="Semestre" />
</div> */}
                    {/* <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Fecha de Nacimiento</div>

                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.3vw' }}><i className="fas fa-birthday-cake"></i></span>
                                <input
                                    id="classroom"
                                    type="text"
                                    className="form-control"
                                    name="classroom"
                                    value={realClassroom}
                                // onChange={data => setBirthday(data.target.value)}
                                //placeholder={formattedDate} 
                                />
                            </div> */}
                    <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Fecha de inicio</div>

                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0', width: '17vw' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.3vw' }}><i className="fas fa-allergies"></i></span>
                        <input
                            id="calendarStart"
                            type="text"
                            className="form-control"
                            name="calendarStart"
                            // onChange={e => setAllergies(e.target.value)}
                            //  value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.alergias)} 
                            value={realCalendarStart}
                        />
                    </div>
                    <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Fecha de término</div>

                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0', width: '17vw' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-disease"></i></span>
                        <input
                            id="calendarEnd"
                            type="text"
                            className="form-control"
                            name="calendarEnd"
                            // onChange={e => setDiseases(e.target.value)}
                            // value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.padecimientos)} 
                            value={realCalendarEnd}
                        />
                    </div>
                    {/* <div className="input-group" style={{ marginTop: '0vh', marginLeft: '0.5vw', paddingLeft: '0.3vw', paddingRight: '1vw', marginRight: '1vw', width: '18.5vw' }}>
<span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
<input id="bloodType" type="text" className="form-control" name="bloodType" placeholder="Tipo de Sangre" />
</div> */}
                    {/* <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Matrícula</div> */}

                    {/* <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '1vw', paddingRight: '0.6vw', marginTop: '0', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '-0.1vw', marginBottom: '3.35vh' }}><i className="fas fa-key"></i></span>
                                <input
                                    id="enrollment"
                                    type="text"
                                    className="form-control"
                                    name="enrollment"
                                    // onChange={data => setBloodType(data.target.value)}
                                    //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.sangre)} 
                                    value={realEnrollment}
                                />
                            </div> */}
                    {/* {mostrarB1 && marvelHeroes.length == 0}
                    {mostrarB2 && marvelHeroes.length > 0} */}
                    <div style={{ display: 'flex' }}>
                        {/* '7.6vw' */}
                        <Button
                            style={{ width: '8vw', height: '3.5vw', marginLeft: '0.6vw', marginTop: '3.5vh', padding: '0' }}
                            type='submit'
                            //disabled={marvelHeroes.length > 0}
                            onClick={async () => {
                                try {
                                    console.log(dataH)


                                    // console.log('Comienza el enviar')
                                    console.log('student modify', studentX);
                                    //Redireccionar
                                    await actualizarHistorial({
                                        variables: { id: clubH, input: { pedido: [{ id: taller, cantidad: 1 }], total: 2, estudiante: studentX, estado: "COMPLETADO" } }
                                    });
                                    await actualizarPedido({
                                        variables: { id: taller, input: { pedido: [{ id: club, cantidad: cantidad }], total: total, estudiante: studentX, estado: "COMPLETADO" } }
                                    });
                                    Swal.fire(
                                        'Correcto',
                                        'El taller se actualizó correctamente',
                                        'success'
                                    )
                                    // history.push('/');
                                    //Mensaje de éxito                              






                                    console.log(error);
                                    console.log(mensaje);



                                } catch (error) {
                                    setMensaje(error.message.replace('GraphQL error: ', ''));
                                    console.log(mensaje);

                                    setTimeout(() => {
                                        setMensaje(null);
                                    }, 3000);
                                }
                            }
                            }
                        //className={`${validarPedido()}`}
                        >
                            Guardar
                </Button>

                    </div>
                </div>

            </form>
        );

    }
    return (
        <>
            <Modal style={{ left: '0vw', top: '8vh', fontFamily: 'Roboto' }} isOpen={modal4} toggle={modal4} size='lg' contentEditable='false' >
                <ModalHeader className='headersGridModal' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>
                    <span className="fas fa-eye" > Ver Detalles</span>
                </ModalHeader>
                <ModalBody>
                    <form id='myForm' onSubmit={e => { e.preventDefault(); }} className="container-fluid" style={{ display: 'flex', flexDirection: 'row', paddingLeft: '0vw', paddingRight: '0vw', marginBottom: '0vh' }}>
                        <div style={{ width: '45vw' }}>
                            <Taller
                                club={club}
                                setClub={setClub}
                                data={data}
                                dataR={dataR}
                                loadingR={loadingR}
                                errorR={errorR}
                                dataC={dataC}
                                loadingC={loadingC}
                                errorC={errorC}
                                dataF={dataF}
                                loadingF={loadingF}
                                errorF={errorF}
                                dataM={dataM}
                                loadingM={loadingM}
                                errorM={errorM}
                                setTaller={setTaller}
                                setCargando={setCargando}
                                loadOptions={loadOptions}
                                loadHeader={loadHeader}
                                loadHeader2={loadHeader2}
                                loadHeader3={loadHeader3}
                                getTaller={getTaller}
                                getClub={getClub}
                                getClubFemenino={getClubFemenino}
                                getClubMasculino={getClubMasculino}
                                genero={genero}
                                genero2={genero2}
                                esTaller={esTaller}
                                esClub={esClub}
                                placeToHold={placeToHold}
                            />
                            <div style={{ display: 'flex', flexDirection: 'column', opacity: '0.9', width: '45vw' }}>
                                <pre style={{ color: '#fff', fontFamily: 'Roboto', fontSize: '24px', marginBottom: '0vh' }}>
                                    Bienvenido {data.obtenerEstudiantesUsuario.map(estudiante => estudiante.nombre)}{' '}
                                    {data.obtenerEstudiantesUsuario.map(estudiante => estudiante.paterno)}{' '}
                                    {data.obtenerEstudiantesUsuario.map(estudiante => estudiante.materno)}{' '}
                                    {'                                                    '}
                                    {'Matrícula: '}
                                    {data.obtenerEstudiantesUsuario.map(estudiante => estudiante.matricula)}
                                </pre>
                                <div className='headersGridX' >
                                    <span class="fas fa-user-graduate" style={{ marginLeft: '-0.5vw', marginRight: '0.3vw' }}></span>
                                     Datos Generales
                                </div>
                                <div className="input-group" style={{ marginLeft: '0vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0vh', backgroundColor: 'white' }}>

                                    <span className="input-group-addon" style={{ color: 'purple', marginRight: '0vw', marginLeft: '0.5vw', backgroundColor: 'white' }}><i className="fas fa-guitar"></i></span>
                                    <div style={{ color: 'purple', fontSize: '20px' }}> {realPrefix}{realName}</div>
                                </div>


                                {mensaje && mostrarMensaje()}
                                {mostrarContenido()}


                            </div>

                        </div>

                    </form>
                </ModalBody>
                <ModalFooter style={{ padding: '0', height: '10vh' }}>
                    <Button onClick={() => setModal4(false)} type='toggle' style={{ marginLeft: '6.9vw', width: '10vw', height: '3.5vw', marginRight: '1vw', marginTop: '1vh', marginBottom: '1vh' }}>Regresar</Button>
                    {/* <Button onClick={() => setModal(false)} type='submit' color='primary'>Guardar</Button>
                    <Button onClick={() => setModal(false)} color='secondary'>Cancelar</Button> */}
                </ModalFooter>
            </Modal>
        </>

    );
};

ModalPedido.propTypes = {
    modal4: propTypes.bool,
    setModal4: propTypes.func,
    dataH: propTypes.object,
    club: propTypes.any,
    setClub: propTypes.func,
    studentX: propTypes.any,
};

export default ModalPedido;