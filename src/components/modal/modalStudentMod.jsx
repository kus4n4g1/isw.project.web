import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import propTypes from 'prop-types';

import React, { useState, useContext } from 'react';
import { Button } from 'reactstrap';
import Header from '../headerBar';
import Footer from '../footer';
import Semester from '../adminStudent/semester';
import Group from '../adminStudent/group';
import Calendar from '../adminStudent/calendar';
import Gender from '../adminStudent/gender';
import Club from '../adminStudent/club';
import { Link } from 'react-router-dom';
import BloodType from '../adminStudent/bloodType';
import { AllContext } from '../context/AllContext';
import { gql, useQuery, useLazyQuery, useMutation } from '@apollo/client';
import moment from 'moment';
import { Route, Redirect, useHistory } from "react-router-dom";
import Swal from 'sweetalert2';
import { Formik, useFormik, useField, useFormikContext } from 'formik';

import * as Yup from 'yup';

const OBTENER_ALUMNO = gql`
query obtenerEstudiante($id:ID!){
    obtenerEstudiante(id:$id){
      id
      nombre
      paterno
      materno
      email
      nacimiento
      semestre
      genero
      curp
      sangre
      alergias
      padecimientos
      matricula    
    }
  }
`;
const NUEVO_ESTUDIANTE = gql`
mutation nuevoEstudiante($input:EstudianteInput){
    nuevoEstudiante(input:$input){
      id
      nombre
      paterno
      materno
      email
      nacimiento
      semestre
      genero
      curp
      sangre
      alergias
      padecimientos
      matricula
      creado
      grupo
    }
  }
`;
const OBTENER_USUARIO_EMAIL = gql`
query obtenerUsuarioEmail($email:String!){
    obtenerUsuarioEmail(email:$email){
      id
      nombre
      email
    }
  }
`
const OBTENER_ESTUDIANTE_EMAIL = gql`
query obtenerEstudianteEmail($email:String!){
    obtenerEstudianteEmail(email:$email){
      id
      nombre
      email
      grupo
    }
  }
`
const ACTUALIZAR_GPO_EST = gql`
mutation actualizarGpoEst($id:ID!, $input:ActualizarGpoEstInput){
    actualizarGpoEst(id:$id,input:$input){
      id
      nombre
      paterno
      grupo
    }
  }
`;
const ACTUALIZAR_ESTUDIANTE = gql`
mutation actualizarEstudiante($id:ID!,$input:EstudianteInput){
    actualizarEstudiante(id:$id,input:$input){
      id
      nombre
      paterno
      materno
      email
      nacimiento
      semestre
      genero
      curp
      sangre
      alergias
      padecimientos
      matricula
      creado
      grupo
    }
  }
  `;

const ModalStudent = (propsZ) => {
    const {
        modal2,
        setModal2,
        studentX,
        setStudentX,
        player,
    } = propsZ;

    //console.log("on open modal", studentX);

    let history = useHistory();
    const value = useContext(AllContext);
    //console.log(value.student);
    const [name, setName] = useState('');
    const [lastName, setLastName] = useState('');
    const [lastName2, setLastName2] = useState('');
    const [curp, setCurp] = useState('');
    const [allergies, setAllergies] = useState('');
    const [enrollment, setEnrollment] = useState('');
    const [email, setEmail] = useState('');
    const [semester, setSemester] = useState('');
    const [group, setGroup] = useState('');
    const [birthday, setBirthday] = useState(null);
    const [gender, setGender] = useState('');
    const [club, setClub] = useState([]);
    const [bloodType, setBloodType] = useState('');
    const [diseases, setDiseases] = useState('');
    const [mensaje, setMensaje] = useState(null);
    const [cargando, setCargando] = useState(true);
    const [taller, setTaller] = useState(null);
    const [loadOpt, loadOptions] = useState(null);
    const [loadHead, loadHeader] = useState(null);
    const [loadHead2, loadHeader2] = useState(null);
    const [loadHead3, loadHeader3] = useState(null);
    const [idUsuario, setIdUsuario] = useState(null);
    const [idEstudiante, setIdEstudiante] = useState(null);
    const [newStudent, setNewStudent] = useState(false);

    const getUser = useQuery(OBTENER_USUARIO_EMAIL, {
        variables: { email: value.email }
    });

    const [getStudent] = useLazyQuery(OBTENER_ESTUDIANTE_EMAIL, {
        variables: { email: value.email },
        onCompleted: data => {
            console.log('data ', data);
            console.log('data obtener estudiante email', data.obtenerEstudianteEmail.id);
            setIdEstudiante(data.obtenerEstudianteEmail.id);
            var ide = data.obtenerEstudianteEmail.id;
            console.log(ide);
            console.log(idUsuario);
            const { data2 } = actualizarEstudiante({
                variables: {
                    id: ide,
                    input: {
                        input: {
                            nombre: name,
                            paterno: lastName,
                            materno: lastName2,
                            email,
                            nacimiento: birthday,
                            semestre: semester,
                            genero: gender,
                            curp,
                            sangre: bloodType,
                            alergias: allergies,
                            padecimientos: diseases,
                            matricula: enrollment,
                            grupo: idUsuario
                        }
                    }
                }
            });
            Swal.fire({
                title: 'Correcto',
                text: "El Estudiante se registró con éxito!",
                icon: 'success',
                showCancelButton: true,
                confirmButtonColor: '#9400D3',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Volver a Inicio',
                cancelButtonText: 'Seguir registrando'
            }).then((result) => {
                if (result.value) {
                    history.push('/dashboard');
                    Swal.fire(
                        'Correcto',
                        'El Estudiante se registró correctamente',
                        'success'
                    )
                } else {
                    history.push('/dashboard/admin/user')
                }
            })
        }
    });

    const { data, error, loading } = useQuery(OBTENER_ALUMNO);

    const [nuevoEstudiante] = useMutation(NUEVO_ESTUDIANTE);
    const [actualizarGpoEst] = useMutation(ACTUALIZAR_GPO_EST);
    const [actualizarEstudiante] = useMutation(ACTUALIZAR_ESTUDIANTE);


    var genero = "";
    var cantidad = 1;
    var total = 1;

    const updateName = (e) => {
        setName(e.target.value);
    }

    const updateCurp = (e) => {
        setCurp(e.target.value);
    }

    const updateLastName = (e) => {
        setLastName(e.target.value);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        value.setStudent(prevStudent => [...prevStudent, {
            name: name,
            lastName: lastName,
            lastName2: lastName2,
            email: email,
            birthday: birthday,
            semester: semester,
            gender: gender,
            club: club,
            curp: curp,
            bloodType: bloodType,
            allergies: allergies,
            diseases: diseases
        }]);
        //console.log(value.student)
    }



    //const [nuevoPedido] = useMutation(NUEVO_PEDIDO);

    // console.log(data);
    // console.log(loading);
    // console.log(error);
    // console.log(dataR);
    // console.log(loadingR);
    // console.log(errorR);

    var marvelHeroes;

    if (!marvelHeroes == 3) {
        //Redireccionar
        history.push('/');
        //Mensaje de éxito                                
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Ya tienes un taller registrado!',
            //footer: '<a href>Why do I have this issue?</a>'
        })
    }

    moment.locale();

    const dateB = Date.now;
    const formattedDate = moment(dateB).format("LL");


    const mostrarMensaje = () => {
        return (
            <div className='headersGrid' >
                <p>{mensaje}</p>
            </div>
        );

    }
    var realName = '';
    var realLastName = '';
    var realLastName2 = '';
    var realEmail = '';
    var realBirthDay = '';
    var realSemester = '';
    var realGender = '';
    var realCurp = '';
    var realBloodType = '';
    var realAllergies = '';
    var realDiseases = '';
    var realEnrollment = '';




    if (studentX != null) {

        realName = studentX.nombre;
        realLastName = studentX.paterno;
        realLastName2 = studentX.materno
        realEmail = studentX.email
        realBirthDay = studentX.nacimiento
        realSemester = studentX.semestre
        realGender = studentX.genero
        realCurp = studentX.curp
        realBloodType = studentX.sangre
        realAllergies = studentX.alergias
        realDiseases = studentX.padecimientos
        realEnrollment = studentX.matricula
    }
    //console.log(modal);

    const schemaValidacion = Yup.object({
        name: Yup.string()
            .required('Nombre es obligatorio*'),
        lastName: Yup.string()
            .required('Apellido paterno es obligatorio*'),
        lastName2: Yup.string()
            .required('Apellido materno es obligatorio*'),
        email: Yup.string()
            .email('El email no es válido')
            .required('Email es obligatorio*'),
        gender: Yup.string()
            .required('Género es obligatorio*'),
        birthday: Yup.date()
            .required('Fecha de nacimiento obligatoria*'),
        semester: Yup.string()
            .required('Semestre es obligatorio*'),
        enrollment: Yup.string()
            .required('Matrícula es obligatoria*')
            .matches(/^(\d{8})$/, {
                message: 'Matrícula en 8 dígitos',
                excludeEmptyString: true
            }),
        curp: Yup.string()
            .required('Curp es obligatorio*')
            .matches(/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/,
                {
                    message: 'Verifique el CURP',
                    excludeEmptyString: true
                }),
        bloodType: Yup.string()
            .required('Tipo de sangre es obligatorio*'),
        allergies: Yup.string()
            .required('Alergias es obligatorio*'),
        diseases: Yup.string()
            .required('Padecimientos es obligatorio*'),

    });

    const initialValues = {
        name: '',
        lastName: '',
        lastName2: '',
        email: '',
        gender: '',
        enrollment: '',
        birthday: '',
        bloodType: '',
        allergies: '',
        diseases: '',
        semester: '',
        curp: '',
    };

    return (
        <Formik
            validationSchema={schemaValidacion}
            enableReinitialize
            //initialvalues={ obtenerMaestros}
            initialValues={initialValues}
            onSubmit={async (valores, funciones) => {
                console.log('enviando');
                console.log('Entró la validación de formik!!!!!!!!');
                //console.log('Esto es id de usuario' + getUser.data.obtenerUsuarioEmail.id);
                //setIdUsuario(getUser.data.obtenerUsuarioEmail.id);
                //console.log(valores);
                const { name, lastName, lastName2, email, gender, enrollment, birthday, bloodType, allergies, diseases, semester, curp } = valores
                try {
                    //Crear nuevo estudiante
                    console.log('Esto es dentro del actualizar y id', studentX.id)
                    const { data } = await actualizarEstudiante({
                        variables: {
                            id: studentX.id,
                            input: {
                                nombre: name,
                                paterno: lastName,
                                materno: lastName2,
                                email,
                                nacimiento: birthday,
                                semestre: semester,
                                genero: gender,
                                curp,
                                sangre: bloodType,
                                alergias: allergies,
                                padecimientos: diseases,
                                matricula: enrollment
                            }
                        }
                    })
                    // if (data) {
                    //     getStudent();
                    // }

                } catch (error) {
                    console.log(error);
                }
            }}


        >
            {props => {
                console.log('props', props)
                return (

                    <Modal style={{ left: '0vw', top: '8vh', fontFamily: 'Roboto' }} isOpen={modal2} toggle={modal2} size='lg' contentEditable='false' >
                        <ModalHeader className='headersGridModal' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>
                            <span className="fas fa-eye" > Ver Detalles</span>
                        </ModalHeader>
                        <ModalBody>
                            <form onSubmit={props.handleSubmit} onKeyPress={e => { if (e.key === 'Enter') { e.preventDefault(); document.getElementById("submitBtn").click() } }} className="container-fluid" style={{ display: 'flex', flexDirection: 'row', paddingLeft: '0vw', paddingRight: '0vw', marginBottom: '0vh' }} >
                                <div className='container1X' style={{ marginRight: '0vw', paddingBottom: '3.35vh' }}>
                                    <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Nombre</div>
                                    {props.touched.name && props.errors.name ? (
                                        <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '3.1vw', marginBottom: '-3vh' }}>
                                            <p>{props.errors.name}</p>
                                        </div>
                                    ) : null}
                                    <div className="input-group" style={{ marginLeft: '0.5vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0vh', width: '17vw' }}>

                                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.5vw' }}><i className="fas fa-user-graduate"></i></span>
                                        <input
                                            id="name"
                                            type="text"
                                            className="form-control"
                                            name="name"
                                            value={props.values.name}
                                            onChange={props.handleChange}
                                            onBlur={props.handleBlur}
                                            //onChange={updateName}
                                            //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.nombre)}
                                            placeholder={realName}
                                        />
                                    </div>
                                    {props.touched.lastName && props.errors.lastName ? (
                                        <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '3.1vw', marginBottom: '-3vh', marginTop: '14vh' }}>
                                            <p>{props.errors.lastName}</p>
                                        </div>
                                    ) : null}
                                    <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Apellido Paterno</div>
                                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', width: '16.9vw', marginTop: '0' }}>
                                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.3vw' }}> <i className="fas fa-parking"></i></span>
                                        <input
                                            id="lastName"
                                            type="text"
                                            className="form-control"
                                            name="lastName"
                                            placeholder={realLastName}
                                            value={props.values.lastName}
                                            onChange={props.handleChange}
                                            onBlur={props.handleBlur}
                                        //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.paterno)} 
                                        />
                                    </div>
                                    {props.touched.lastName2 && props.errors.lastName2 ? (
                                        <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '3.1vw', marginBottom: '-3vh', marginTop: '28vh' }}>
                                            <p>{props.errors.lastName2}</p>
                                        </div>
                                    ) : null}
                                    <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Apellido Materno</div>

                                    <div className="input-group" style={{ marginLeft: '0.5vw', paddingLeft: '0vw', paddingRight: '1vw', marginTop: '0vh', width: '17.3vw', marginRight: '1vw' }}>
                                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.7vw', marginLeft: '0.3vw' }}><i className="fas fa-file-medical-alt"></i></span>
                                        <input
                                            id="lastName2"
                                            type="text"
                                            className="form-control"
                                            name="lastName2"
                                            value={props.values.lastName2}
                                            onChange={props.handleChange}
                                            onBlur={props.handleBlur}
                                            //onChange={e => setLastName2(e.target.value)}
                                            // value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.materno)}
                                            placeholder={realLastName2}
                                        />
                                    </div>
                                    {props.touched.gender && props.errors.gender ? (
                                        <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '3.1vw', marginBottom: '-3vh', marginTop: '42vh' }}>
                                            <p>{props.errors.gender}</p>
                                        </div>
                                    ) : null}
                                    <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Género</div>

                                    <div className="input-group" style={{ marginLeft: '0.5vw', paddingLeft: '0vw', paddingRight: '1vw', marginTop: '0vh', width: '17.3vw', marginRight: '1vw' }}>
                                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.2vw', marginBottom: '3.35vh' }}><i className="fas fa-venus-mars"></i></span>
                                        <input
                                            id="gender"
                                            type="text"
                                            className="form-control"
                                            name="gender"
                                            value={props.values.gender}
                                            onChange={props.handleChange}
                                            onBlur={props.handleBlur}
                                            placeholder={realGender}
                                        //onChange={data => setGender(data.target.value)}

                                        />
                                    </div>



                                </div>
                                <div className='container2X'>

                                    {/* <div className="input-group" style={{ paddingLeft: '0.3vw', paddingRight: '1vw' }}>
                        <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                        <input id="semester" type="text" className="form-control" name="semester" placeholder="Semestre" />
                    </div> */}
                                    {props.touched.email && props.errors.email ? (
                                        <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '3.1vw', marginBottom: '-3vh' }}>
                                            <p>{props.errors.email}</p>
                                        </div>
                                    ) : null}
                                    <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Email</div>

                                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0vh', width: '17vw' }}>
                                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-envelope"></i></span>
                                        <input
                                            id="email"
                                            type="text"
                                            className="form-control"
                                            name="email"
                                            value={props.values.email}
                                            onChange={props.handleChange}
                                            onBlur={props.handleBlur}
                                            //onChange={data => setEmail(data.target.value)}
                                            //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.email)} 
                                            placeholder={realEmail}
                                        />
                                    </div>
                                    {props.touched.curp && props.errors.curp ? (
                                        <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '3.1vw', marginBottom: '-3vh', marginTop: '14vh' }}>
                                            <p>{props.errors.curp}</p>
                                        </div>
                                    ) : null}
                                    <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Curp</div>

                                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0vh', width: '17vw' }}>
                                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-atlas"></i></span>
                                        <input
                                            id="curp"
                                            type="text"
                                            className="form-control"
                                            name="curp"
                                            value={props.values.curp}
                                            onChange={props.handleChange}
                                            onBlur={props.handleBlur}
                                            //onChange={updateCurp}
                                            //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.curp)} 
                                            placeholder={realCurp}

                                        />
                                    </div>
                                    {props.touched.semester && props.errors.semester ? (
                                        <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '3.1vw', marginBottom: '-3vh', marginTop: '28vh' }}>
                                            <p>{props.errors.semester}</p>
                                        </div>
                                    ) : null}
                                    <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Semestre</div>

                                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0vh', width: '17vw' }}>
                                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-university"></i></span>
                                        <input
                                            id="semester"
                                            type="text"
                                            className="form-control"
                                            name="semester"
                                            value={props.values.semester}
                                            onChange={props.handleChange}
                                            onBlur={props.handleBlur}
                                            // onChange={data => setSemester(data.target.value)}
                                            //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.semestre)} 
                                            placeholder={realSemester}
                                        />
                                    </div>
                                    {props.touched.bloodType && props.errors.bloodType ? (
                                        <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '3.1vw', marginBottom: '-3vh', marginTop: '42vh' }}>
                                            <p>{props.errors.bloodType}</p>
                                        </div>
                                    ) : null}
                                    <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Tipo de Sangre</div>

                                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0', width: '17vw' }}>
                                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '-0.1vw', marginBottom: '3.35vh' }}><i className="fas fa-syringe"></i></span>
                                        <input
                                            id="bloodType"
                                            type="text"
                                            className="form-control"
                                            name="bloodType"
                                            value={props.values.bloodType}
                                            onChange={props.handleChange}
                                            onBlur={props.handleBlur}
                                            // onChange={data => setBloodType(data.target.value)}
                                            //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.sangre)} 
                                            placeholder={realBloodType}
                                        />
                                    </div>
                                    <Button
                                        style={{ marginLeft: '6.9vw', width: '10vw', height: '3.5vw', marginRight: '1vw', marginTop: '1vh', marginBottom: '1vh' }}
                                        type='submit'
                                        id="submitBtn"
                                    >
                                        Guardar
                                </Button>
                                    {/* <div className="input-group" style={{ marginTop: '0vh', marginLeft: '0.5vw', paddingLeft: '0.3vw', paddingRight: '1vw', marginRight: '1vw', width: '18.5vw' }}>
                        <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                        <input id="bloodType" type="text" className="form-control" name="bloodType" placeholder="Tipo de Sangre" />
                    </div> */}


                                </div>
                                <div className='container2X'>

                                    {/* <div className="input-group" style={{ paddingLeft: '0.3vw', paddingRight: '1vw' }}>
<span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
<input id="semester" type="text" className="form-control" name="semester" placeholder="Semestre" />
</div> */}
                                    {props.touched.birthday && props.errors.birthday ? (
                                        <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '3.1vw', marginBottom: '-3vh' }}>
                                            <p>{props.errors.birthday}</p>
                                        </div>
                                    ) : null}
                                    <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Fecha de Nacimiento</div>

                                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0', width: '17vw' }}>
                                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.3vw' }}><i className="fas fa-birthday-cake"></i></span>
                                        <input
                                            id="birthday"
                                            type="text"
                                            className="form-control"
                                            name="birthday"
                                            value={props.values.birthday}
                                            onChange={props.handleChange}
                                            onBlur={props.handleBlur}
                                            // onChange={data => setBirthday(data.target.value)}
                                            //placeholder={formattedDate} 
                                            placeholder={realBirthDay}
                                        />
                                    </div>
                                    {props.touched.allergies && props.errors.allergies ? (
                                        <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '3.1vw', marginBottom: '-3vh', marginTop: '14vh' }}>
                                            <p>{props.errors.allergies}</p>
                                        </div>
                                    ) : null}
                                    <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Alergias</div>

                                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0', width: '17vw' }}>
                                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.3vw' }}><i className="fas fa-allergies"></i></span>
                                        <input
                                            id="allergies"
                                            type="text"
                                            className="form-control"
                                            name="allergies"
                                            value={props.values.allergies}
                                            onChange={props.handleChange}
                                            onBlur={props.handleBlur}
                                            // onChange={e => setAllergies(e.target.value)}
                                            //  value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.alergias)} 
                                            placeholder={realAllergies}
                                        />
                                    </div>
                                    {props.touched.diseases && props.errors.diseases ? (
                                        <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '3.1vw', marginBottom: '-3vh', marginTop: '28vh' }}>
                                            <p>{props.errors.diseases}</p>
                                        </div>
                                    ) : null}
                                    <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Padecimientos</div>

                                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0', width: '17vw' }}>
                                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-disease"></i></span>
                                        <input
                                            id="diseases"
                                            type="text"
                                            className="form-control"
                                            name="diseases"
                                            value={props.values.diseases}
                                            onChange={props.handleChange}
                                            onBlur={props.handleBlur}
                                            // onChange={e => setDiseases(e.target.value)}
                                            // value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.padecimientos)} 
                                            placeholder={realDiseases}
                                        />
                                    </div>
                                    {/* <div className="input-group" style={{ marginTop: '0vh', marginLeft: '0.5vw', paddingLeft: '0.3vw', paddingRight: '1vw', marginRight: '1vw', width: '18.5vw' }}>
<span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
<input id="bloodType" type="text" className="form-control" name="bloodType" placeholder="Tipo de Sangre" />
</div> */}
                                    {props.touched.enrollment && props.errors.enrollment ? (
                                        <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '3.1vw', marginBottom: '-3vh', marginTop: '42vh' }}>
                                            <p>{props.errors.enrollment}</p>
                                        </div>
                                    ) : null}
                                    <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Matrícula</div>

                                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '1vw', paddingRight: '0.6vw', marginTop: '0', width: '17vw' }}>
                                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '-0.1vw', marginBottom: '3.35vh' }}><i className="fas fa-key"></i></span>
                                        <input
                                            id="enrollment"
                                            type="text"
                                            className="form-control"
                                            name="enrollment"
                                            value={props.values.enrollment}
                                            onChange={props.handleChange}
                                            onBlur={props.handleBlur}
                                            // onChange={data => setBloodType(data.target.value)}
                                            //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.sangre)} 
                                            placeholder={realEnrollment}
                                        />

                                    </div>
                                    <Button onClick={() => setModal2(false)} type='toggle' style={{ marginLeft: '2vw', width: '10vw', height: '3.5vw', marginRight: '1vw', marginTop: '1vh', marginBottom: '1vh' }}>Regresar</Button>

                                </div>
                            </form>
                        </ModalBody>
                        <ModalFooter style={{ padding: '0', height: '10vh' }}>



                            {/* <Button onClick={() => setModal(false)} type='submit' color='primary'>Guardar</Button>
                    <Button onClick={() => setModal(false)} color='secondary'>Cancelar</Button> */}
                        </ModalFooter>
                    </Modal>


                );
            }}
        </Formik>
    );
};

ModalStudent.propTypes = {
    modal2: propTypes.bool,
    setModal2: propTypes.func,
    setStudentX: propTypes.func,
    studentX: propTypes.object
};

export default ModalStudent;