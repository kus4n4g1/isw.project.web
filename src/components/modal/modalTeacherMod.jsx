import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import propTypes from 'prop-types';

import React, { useState, useContext } from 'react';
import { Button } from 'reactstrap';
import Header from '../headerBar';
import Footer from '../footer';
import Semester from '../adminStudent/semester';
import Group from '../adminStudent/group';
import Calendar from '../adminStudent/calendar';
import Gender from '../adminStudent/gender';
import Club from '../adminStudent/club';
import { Link } from 'react-router-dom';
import BloodType from '../adminStudent/bloodType';
import { AllContext } from '../context/AllContext';
import { gql, useQuery, useMutation } from '@apollo/client';
import moment from 'moment';
import { Route, Redirect, useHistory } from "react-router-dom";
import Swal from 'sweetalert2';
import { Formik, useFormik, useField, useFormikContext } from 'formik';

import * as Yup from 'yup';

const OBTENER_MAESTRO = gql`
query obtenerMaestro($id:ID!){
    obtenerMaestro(id:$id){
      id
      nombre
      paterno
      materno
      email
      telefono
      celular
      curp
      sangre
      alergias
      padecimientos   
    }
  }
`;
const ACTUALIZAR_MAESTRO = gql`
mutation actualizarMaestro($id:ID!,$input:MaestroInput){
    actualizarMaestro(id:$id,input:$input){
      id
      nombre
      paterno
      materno
      email
      telefono
      celular
      curp
      sangre
      alergias
      padecimientos    
      creado
      grupo
    }
  }
  `;

const ModalStudent = (propsZ) => {
    const {
        modal2,
        setModal2,
        teacherX,
        setTeacherX,
        player,
    } = propsZ;

    //console.log("on open modal", teacherX);

    let history = useHistory();
    const value = useContext(AllContext);
    //console.log(value.student);
    const [name, setName] = useState('');
    const [lastName, setLastName] = useState('');
    const [lastName2, setLastName2] = useState('');
    const [curp, setCurp] = useState('');
    const [allergies, setAllergies] = useState('');
    const [email, setEmail] = useState('');
    const [semester, setSemester] = useState('');
    const [group, setGroup] = useState('');
    const [birthday, setBirthday] = useState(null);
    const [gender, setGender] = useState('');
    const [club, setClub] = useState([]);
    const [bloodType, setBloodType] = useState('');
    const [diseases, setDiseases] = useState('');
    const [mensaje, setMensaje] = useState(null);
    const [cargando, setCargando] = useState(true);
    const [taller, setTaller] = useState(null);
    const [loadOpt, loadOptions] = useState(null);
    const [loadHead, loadHeader] = useState(null);
    const [loadHead2, loadHeader2] = useState(null);
    const [loadHead3, loadHeader3] = useState(null);


    const { data, error, loading } = useQuery(OBTENER_MAESTRO);
    const [actualizarMaestro] = useMutation(ACTUALIZAR_MAESTRO);

    var genero = "";
    var cantidad = 1;
    var total = 1;

    const updateName = (e) => {
        setName(e.target.value);
    }

    const updateCurp = (e) => {
        setCurp(e.target.value);
    }

    const updateLastName = (e) => {
        setLastName(e.target.value);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        value.setStudent(prevStudent => [...prevStudent, {
            name: name,
            lastName: lastName,
            lastName2: lastName2,
            email: email,
            birthday: birthday,
            semester: semester,
            gender: gender,
            club: club,
            curp: curp,
            bloodType: bloodType,
            allergies: allergies,
            diseases: diseases
        }]);
        //console.log(value.student)
    }



    //const [nuevoPedido] = useMutation(NUEVO_PEDIDO);

    // console.log(data);
    // console.log(loading);
    // console.log(error);
    // console.log(dataR);
    // console.log(loadingR);
    // console.log(errorR);




    moment.locale();

    const dateB = Date.now;
    const formattedDate = moment(dateB).format("LL");


    const mostrarMensaje = () => {
        return (
            <div className='headersGrid' >
                <p>{mensaje}</p>
            </div>
        );

    }
    var realName = '';
    var realLastName = '';
    var realLastName2 = '';
    var realEmail = '';
    var realPhone = '';
    var realMobile = '';
    var realCurp = '';
    var realBloodType = '';
    var realAllergies = '';
    var realDiseases = '';




    if (teacherX != null) {

        realName = teacherX.nombre;
        realLastName = teacherX.paterno;
        realLastName2 = teacherX.materno
        realEmail = teacherX.email;
        realPhone = teacherX.telefono;
        realMobile = teacherX.celular;
        realCurp = teacherX.curp;
        realBloodType = teacherX.sangre
        realAllergies = teacherX.alergias
        realDiseases = teacherX.padecimientos
        if (realPhone === '') {
            realPhone = 'No proporcionó';
        }
    }
    //console.log(modal);

    const formik = useFormik({
        initialValues: {
            name: '',
            lastName: '',
            lastName2: '',
            email: '',
            phone: '',
            cellPhone: '',
            curp: '',
            bloodType: '',
            allergies: '',
            diseases: '',

        },
        validationSchema: Yup.object({
            name: Yup.string()
                .required('Nombre es obligatorio*'),
            lastName: Yup.string()
                .required('Apellido paterno es obligatorio*'),
            lastName2: Yup.string()
                .required('Apellido materno es obligatorio*'),
            email: Yup.string()
                .email('El email no es válido')
                .required('Email es obligatorio*'),
            // phone: Yup.string(),
            // //.test('len', 'Teléfono de 10 dígitos', val => val.length === 10),
            phone: Yup.string()
                .matches(/^(\d{10})$/, {
                    message: 'Teléfono en 10 dígitos',
                    excludeEmptyString: true
                }),
            cellPhone: Yup.string()
                .required('Celular es obligatorio*')
                .matches(/^(\d{10})$/, {
                    message: 'Celular en 10 dígitos',
                    excludeEmptyString: true
                }),
            //.test('len', 'Teléfono de 10 dígitos', val => val.length === 10),
            curp: Yup.string()
                .required('Curp es obligatorio*')
                .matches(/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/,
                    {
                        message: 'Verifique el CURP',
                        excludeEmptyString: true
                    }),
            //.test('len', 'Curp inválido', val => val.length === 18),
            bloodType: Yup.string()
                .required('Tipo de sangre es obligatorio*'),
            allergies: Yup.string()
                .required('Alergias es obligatorio*'),
            diseases: Yup.string()
                .required('Padecimientos es obligatorio*'),
        }),
        onSubmit: async valores => {
            console.log('enviando');
            console.log(valores);
            const { name, lastName, lastName2, email, phone, cellPhone, curp, bloodType, allergies, diseases } = valores
            try {
                const { data } = await actualizarMaestro({
                    variables: {
                        id: teacherX.id,
                        input: {
                            nombre: name,
                            paterno: lastName,
                            materno: lastName2,
                            email: email,
                            telefono: phone,
                            celular: cellPhone,
                            curp: curp,
                            sangre: bloodType,
                            alergias: allergies,
                            padecimientos: diseases
                        }
                    }
                })
                // //Redireccionar
                Swal.fire({
                    title: 'Correcto',
                    text: "El maestro se registró con éxito!",
                    icon: 'success',
                    showCancelButton: true,
                    confirmButtonColor: '#9400D3',
                    cancelButtonColor: '#3085d6',
                    confirmButtonText: 'Volver a Inicio',
                    cancelButtonText: 'Seguir registrando'
                }).then((result) => {
                    if (result.value) {
                        history.push('/dashboard');
                        Swal.fire(
                            'Correcto',
                            'El maestro se registró correctamente',
                            'success'
                        )
                    } else {
                        history.push('/dashboard/admin/teacher')
                    }
                })
                // history.push('/dashboard');
                // //Mensaje de éxito                                
                // Swal.fire(
                //     'Correcto',
                //     'El maestro se registró correctamente',
                //     'success'
                // )
            } catch (error) {
                console.log(error);
            }


        }
    });

    return (
        <>
            <Modal style={{ left: '0vw', top: '8vh', fontFamily: 'Roboto' }} isOpen={modal2} toggle={modal2} size='lg' contentEditable='false' >
                <ModalHeader className='headersGridModal' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>
                    <span className="fas fa-eye" > Ver Detalles</span>
                </ModalHeader>
                <ModalBody>
                    <form id='myForm' className="container-fluid " onSubmit={formik.handleSubmit} style={{ display: 'flex', flexDirection: 'row', paddingLeft: '0vw', paddingRight: '0vw', marginBottom: '0vh', width: '40vw' }}>
                        <div className='container1X' style={{ marginLeft: '1vw', marginRight: '0vw', paddingBottom: '3.35vh', width: '30vw !important' }}>
                            {formik.touched.name && formik.errors.name ? (
                                <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginBottom: '-3vh' }}>
                                    <p>{formik.errors.name}</p>
                                </div>
                            ) : null}

                            <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Nombre</div>
                            <div className="input-group" style={{ marginLeft: '0.5vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0vh', width: '17vw' }}>

                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.5vw' }}><i className="fas fa-user-graduate"></i></span>
                                <input
                                    id="name"
                                    type="text"
                                    className="form-control"
                                    name="name"
                                    //onChange={updateName}
                                    //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.nombre)}
                                    placeholder={realName}
                                    value={formik.values.name}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                />
                            </div>
                            {formik.touched.lastName && formik.errors.lastName ? (
                                <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginBottom: '-3vh', marginTop: '14vh' }}>
                                    <p>{formik.errors.lastName}</p>
                                </div>
                            ) : null}
                            <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Apellido Paterno</div>
                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', width: '16.9vw', marginTop: '0' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.3vw' }}> <i className="fas fa-parking"></i></span>
                                <input
                                    id="lastName"
                                    type="text"
                                    className="form-control"
                                    name="lastName"
                                    placeholder={realLastName}
                                    value={formik.values.lastName}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.paterno)} 
                                />
                            </div>
                            {formik.touched.lastName2 && formik.errors.lastName2 ? (
                                <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginBottom: '-3vh', marginTop: '28vh' }}>
                                    <p>{formik.errors.lastName2}</p>
                                </div>
                            ) : null}
                            <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Apellido Materno</div>

                            <div className="input-group" style={{ marginLeft: '0.5vw', paddingLeft: '0vw', paddingRight: '1vw', marginTop: '0vh', width: '17.3vw', marginRight: '1vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.7vw', marginLeft: '0.3vw' }}><i className="fas fa-file-medical-alt"></i></span>
                                <input
                                    id="lastName2"
                                    type="text"
                                    className="form-control"
                                    name="lastName2"
                                    onChange={e => setLastName2(e.target.value)}
                                    // value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.materno)}
                                    placeholder={realLastName2}
                                    value={formik.values.lastName2}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                />
                            </div>
                            {formik.touched.phone && formik.errors.phone ? (
                                <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginBottom: '-3vh', marginTop: '42vh' }}>
                                    <p>{formik.errors.phone}</p>
                                </div>
                            ) : null}
                            <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Teléfono</div>

                            <div className="input-group" style={{ marginLeft: '0.5vw', paddingLeft: '0vw', paddingRight: '1vw', marginTop: '0vh', width: '17.3vw', marginRight: '1vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.2vw', marginBottom: '3.35vh' }}><i className="fas fa-venus-mars"></i></span>
                                <input
                                    id="phone"
                                    type="text"
                                    className="form-control"
                                    name="phone"
                                    placeholder={realPhone}
                                    value={formik.values.phone}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                //onChange={data => setGender(data.target.value)}

                                />
                            </div>
                            {formik.touched.cellPhone && formik.errors.cellPhone ? (
                                <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginBottom: '-3vh', marginTop: '56vh' }}>
                                    <p>{formik.errors.cellPhone}</p>
                                </div>
                            ) : null}
                            <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Celular</div>

                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0vh', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-university"></i></span>
                                <input
                                    id="cellPhone"
                                    type="text"
                                    className="form-control"
                                    name="cellPhone"
                                    // onChange={data => setSemester(data.target.value)}
                                    //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.semestre)} 
                                    placeholder={realMobile}
                                    value={formik.values.cellPhone}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                />
                            </div>
                            <Button
                                style={{ marginLeft: '6.9vw', width: '10vw', height: '3.5vw', marginRight: '1vw', marginTop: '1vh', marginBottom: '1vh' }}
                                type='submit'
                                id="submitBtn"
                            >
                                Guardar
                                </Button>


                        </div>
                        <div className='container2X' style={{ marginRight: '-10vw' }}>

                            {/* <div className="input-group" style={{ paddingLeft: '0.3vw', paddingRight: '1vw' }}>
                        <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                        <input id="semester" type="text" className="form-control" name="semester" placeholder="Semestre" />
                    </div> */}
                            {formik.touched.email && formik.errors.email ? (
                                <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginBottom: '-3vh' }}>
                                    <p>{formik.errors.email}</p>
                                </div>
                            ) : null}
                            <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Email</div>

                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0vh', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-envelope"></i></span>
                                <input
                                    id="email"
                                    type="text"
                                    className="form-control"
                                    name="email"
                                    //onChange={data => setEmail(data.target.value)}
                                    //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.email)} 
                                    placeholder={realEmail}
                                    value={formik.values.email}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                />
                            </div>
                            {formik.touched.curp && formik.errors.curp ? (
                                <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginBottom: '-3vh', marginTop: '14vh' }}>
                                    <p>{formik.errors.curp}</p>
                                </div>
                            ) : null}
                            <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Curp</div>

                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0vh', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-atlas"></i></span>
                                <input
                                    id="curp"
                                    type="text"
                                    className="form-control"
                                    name="curp"
                                    //onChange={updateCurp}
                                    //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.curp)} 
                                    placeholder={realCurp}
                                    value={formik.values.curp}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}

                                />
                            </div>
                            {formik.touched.bloodType && formik.errors.bloodType ? (
                                <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginBottom: '-3vh', marginTop: '28vh' }}>
                                    <p>{formik.errors.bloodType}</p>
                                </div>
                            ) : null}
                            <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Tipo de Sangre</div>

                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '-0.1vw', marginBottom: '3.35vh' }}><i className="fas fa-syringe"></i></span>
                                <input
                                    id="bloodType"
                                    type="text"
                                    className="form-control"
                                    name="bloodType"
                                    // onChange={data => setBloodType(data.target.value)}
                                    //value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.sangre)} 
                                    placeholder={realBloodType}
                                    value={formik.values.bloodType}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                />
                            </div>
                            {formik.touched.allergies && formik.errors.allergies ? (
                                <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginBottom: '-3vh', marginTop: '42vh' }}>
                                    <p>{formik.errors.allergies}</p>
                                </div>
                            ) : null}
                            <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Alergias</div>

                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.3vw' }}><i className="fas fa-allergies"></i></span>
                                <input
                                    id="allergies"
                                    type="text"
                                    className="form-control"
                                    name="allergies"
                                    // onChange={e => setAllergies(e.target.value)}
                                    //  value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.alergias)} 
                                    placeholder={realAllergies}
                                    value={formik.values.allergies}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                />
                            </div>
                            {formik.touched.diseases && formik.errors.diseases ? (
                                <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginBottom: '-3vh', marginTop: '56vh' }}>
                                    <p>{formik.errors.diseases}</p>
                                </div>
                            ) : null}
                            <div style={{ color: 'purple', fontWeight: 'normal', marginLeft: '2.4vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>Padecimientos</div>

                            <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '0', width: '17vw' }}>
                                <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-disease"></i></span>
                                <input
                                    id="diseases"
                                    type="text"
                                    className="form-control"
                                    name="diseases"
                                    // onChange={e => setDiseases(e.target.value)}
                                    // value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.padecimientos)} 
                                    placeholder={realDiseases}
                                    value={formik.values.diseases}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                />
                            </div>

                            {/* <div className="input-group" style={{ marginTop: '0vh', marginLeft: '0.5vw', paddingLeft: '0.3vw', paddingRight: '1vw', marginRight: '1vw', width: '18.5vw' }}>
                        <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                        <input id="bloodType" type="text" className="form-control" name="bloodType" placeholder="Tipo de Sangre" />
                    </div> */}

                            <Button onClick={() => setModal2(false)} type='toggle' style={{ marginLeft: '2vw', width: '10vw', height: '3.5vw', marginRight: '1vw', marginTop: '1vh', marginBottom: '1vh' }}>Regresar</Button>

                        </div>

                    </form>
                </ModalBody>
                <ModalFooter style={{ padding: '0', height: '10vh' }}>
                    {/* <Button onClick={() => setModal(false)} type='toggle' style={{ marginLeft: '6.9vw', width: '10vw', height: '3.5vw', marginRight: '1vw', marginTop: '1vh', marginBottom: '1vh' }}>Regresar</Button> */}

                    {/* <Button onClick={() => setModal(false)} type='submit' color='primary'>Guardar</Button>
                    <Button onClick={() => setModal(false)} color='secondary'>Cancelar</Button> */}
                </ModalFooter>
            </Modal>
        </>

    );
};

ModalStudent.propTypes = {
    modal2: propTypes.bool,
    setModal2: propTypes.func,
    setTeacherX: propTypes.func,
    teacherX: propTypes.object
};

export default ModalStudent;