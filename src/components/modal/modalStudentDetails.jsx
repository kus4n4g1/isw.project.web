import React, { useState, useContext } from 'react';
import { Button } from 'reactstrap';
import Header from '../headerBar';
import Footer from '../footer';
import Semester from '../adminStudent/semester';
import Group from '../adminStudent/group';
import Calendar from '../adminStudent/calendar';
import Gender from '../adminStudent/gender';
import Club from '../adminStudent/club';
import { Link } from 'react-router-dom';
import BloodType from '../adminStudent/bloodType';
import { AllContext } from '../context/AllContext';
import { gql, useQuery, useMutation } from '@apollo/client';
import moment from 'moment';
import { Route, Redirect, useHistory } from "react-router-dom";
import Swal from 'sweetalert2';
import { useFormik } from 'formik';
import * as Yup from 'yup';


const ModalStudentDetails = (props) => {
    const {
        modal,
        setModal
    } = props;
    let history = useHistory();
    const value = useContext(AllContext);
    //console.log(value.student);
    const [name, setName] = useState('');
    const [lastName, setLastName] = useState('');
    const [lastName2, setLastName2] = useState('');
    const [curp, setCurp] = useState('');
    const [allergies, setAllergies] = useState('');
    const [email, setEmail] = useState('');
    const [semester, setSemester] = useState('');
    const [group, setGroup] = useState('');
    const [birthday, setBirthday] = useState(null);
    const [gender, setGender] = useState('');
    const [club, setClub] = useState([]);
    const [bloodType, setBloodType] = useState('');
    const [diseases, setDiseases] = useState('');
    const [mensaje, setMensaje] = useState(null);
    const [cargando, setCargando] = useState(true);
    const [taller, setTaller] = useState(null);
    const [loadOpt, loadOptions] = useState(null);


    var cantidad = 1;
    var total = 1;

    const updateName = (e) => {
        setName(e.target.value);
    }

    const updateCurp = (e) => {
        setCurp(e.target.value);
    }

    const updateLastName = (e) => {
        setLastName(e.target.value);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        value.setStudent(prevStudent => [...prevStudent, {
            name: name,
            lastName: lastName,
            lastName2: lastName2,
            email: email,
            birthday: birthday,
            semester: semester,
            gender: gender,
            club: club,
            curp: curp,
            bloodType: bloodType,
            allergies: allergies,
            diseases: diseases
        }]);
        //console.log(value.student)
    }



    //const [nuevoPedido] = useMutation(NUEVO_PEDIDO);

    // console.log(data);
    // console.log(loading);
    // console.log(error);
    // console.log(dataR);
    // console.log(loadingR);
    // console.log(errorR);

    var marvelHeroes;

    if (!marvelHeroes == 3) {
        //Redireccionar
        history.push('/');
        //Mensaje de éxito                                
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Ya tienes un taller registrado!',
            //footer: '<a href>Why do I have this issue?</a>'
        })
    }

    moment.locale();

    const dateB = Date.now;
    const formattedDate = moment(dateB).format("LL");


    const mostrarMensaje = () => {
        return (
            <div className='headersGrid' >
                <p>{mensaje}</p>
            </div>
        );

    }

    console.log(modal);
    return (
        <>
            <Modal className='modal-dialog-centered' style={{ left: '9vw', top: '-5vh', opacity: '1' }} isOpen={modal} toggle={modal} size='lg' contentEditable='false' >
                <ModalHeader className='headersGridModal' >
                    <span className="fas fa-eye"> Ver Detalles</span>
                </ModalHeader>
                <ModalBody>
                    <form id='myForm' className="container-fluid" style={{ display: 'flex', flexDirection: 'row', paddingLeft: '0vw', paddingRight: '0vw' }}>
                        <div className='containerX' style={{ paddingLeft: '0.75vw' }} >
                            <div className="input-group" style={{ marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '2vh', width: '17.9vw' }}>
                                <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                                <input
                                    id="studentName"
                                    type="text"
                                    className="form-control"
                                    name="name"
                                    value={name}
                                    onChange={updateName}
                                    placeholder='Nombre' />
                            </div>
                            <div className="input-group" style={{ paddingLeft: '0vw', paddingRight: '1vw', marginTop: '6vh', width: '18.3vw' }}>
                                <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                                <input
                                    id="lastName"
                                    type="text"
                                    className="form-control"
                                    name="lastName"
                                    value={lastName}
                                    onChange={updateLastName}
                                    placeholder={'Apellido Paterno'} />
                            </div>
                            <div className="input-group" style={{ paddingRight: '0.6vw', marginTop: '6vh', width: '17.9vw' }}>
                                <span className="input-group-addon"  > <i className="glyphicon glyphicon-user"></i></span>
                                <input
                                    id="lastName2"
                                    type="text"
                                    className="form-control"
                                    name="lastName2"
                                    value={lastName2}
                                    onChange={e => setLastName2(e.target.value)}
                                    placeholder={'Apellido Materno'} />
                            </div >
                            <div className="input-group " style={{ marginLeft: '0vw', paddingLeft: '0', paddingRight: '1vw', width: '20.2vw', marginBottom: '3vh', marginTop: '6vh' }}>
                                <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                                <Gender style={{ width: '20.2vw !important' }}
                                    setGender={setGender}
                                />
                            </div>




                            {/* <Button
                                style={{ width: '10vw', height: '3.5vw', marginLeft: '7vw' }}
                                type='submit'
                                disabled={club.length == 0}
                                onClick={async () => {
                                    try {
                                        //console.log(error);
                                        console.log(mensaje);

                                        //Redireccionar
                                        history.push('/');
                                        //Mensaje de éxito                                
                                        Swal.fire(
                                            'Correcto',
                                            'El alumno se registró correctamente',
                                            'success'
                                        )

                                    } catch (error) {
                                        //setMensaje(error.message.replace('GraphQL error: ', ''));
                                        console.log(mensaje);

                                        setTimeout(() => {
                                            setMensaje(null);
                                        }, 3000);
                                    }
                                }
                                }
                            //className={`${validarPedido()}`}
                            >
                                Guardar
                    </Button> */}

                        </div>
                        <div className='containerX' style={{ marginLeft: '0', paddingLeft: '0vw' }}>
                            <div className="input-group" style={{ marginLeft: '0.7vw', paddingLeft: '0', paddingRight: '1vw', marginRight: '1vw', width: '17.7vw', marginBottom: '0vh', marginTop: '2.0vh' }}>
                                <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                                <input
                                    id="email" type="text"
                                    className="form-control"
                                    name="email"
                                    value={email}
                                    onChange={data => setEmail(data.target.value)}
                                    placeholder={'email'} />
                            </div>


                            {/* <div className="input-group" style={{ paddingLeft: '0.3vw', paddingRight: '1vw' }}>
                        <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                        <input id="semester" type="text" className="form-control" name="semester" placeholder="Semestre" />
                    </div> */}

                            <div className="input-group" style={{ marginLeft: '0.7vw', paddingLeft: '0', paddingRight: '1vw', marginRight: '1vw', width: '17.7vw', marginBottom: '0vh', marginTop: '6vh' }}>
                                <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                                <BloodType
                                    value={bloodType}
                                    setBloodType={setBloodType}
                                    placeholder={'Tipo de Sangre'} />
                            </div>
                            <div className="input-group" style={{ marginLeft: '0.7vw', paddingLeft: '0', paddingRight: '1vw', marginRight: '1vw', width: '17.7vw', marginBottom: '0vh', marginTop: '5.7vh' }}>
                                <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                                <input
                                    id="semester" type="text"
                                    className="form-control"
                                    name="semester"
                                    value={semester}
                                    onChange={data => setSemester(data.target.value)}
                                    placeholder={'Semestre'} />
                            </div>


                            {/* <div className="input-group" style={{ marginTop: '0vh', marginLeft: '0.5vw', paddingLeft: '0.3vw', paddingRight: '1vw', marginRight: '1vw', width: '18.5vw' }}>
                        <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                        <input id="bloodType" type="text" className="form-control" name="bloodType" placeholder="Tipo de Sangre" />
                    </div> */}

                            <div className="input-group" style={{ marginLeft: '0.5vw', paddingLeft: '0.3vw', paddingRight: '1vw', marginRight: '1vw', width: '17.6vw', marginTop: '6vh', marginBottom: '0.5vw' }}>
                                <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                                <input
                                    id="diseases"
                                    type="text"
                                    className="form-control"
                                    name="diseases"
                                    value={diseases}
                                    onChange={e => setDiseases(e.target.value)}
                                    placeholder={'Padecimientos'} />
                            </div>
                            {/* <Link to='/dashboard'>
                                <Button style={{ marginLeft: '0.5vw', width: '10vw', height: '3.5vw', marginRight: '5vw' }}>Regresar</Button>
                            </Link> */}
                        </div>
                        <div className='containerX' style={{ marginLeft: '0', paddingLeft: '0.7vw' }}>
                            <div className="input-group" style={{ marginLeft: '0.5vw', paddingLeft: '0.3vw', paddingRight: '1vw', marginRight: '1vw', width: '17.6vw', marginTop: '6vh', marginBottom: '0.5vw' }}>
                                <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                                <input
                                    id="diseases"
                                    type="text"
                                    className="form-control"
                                    name="diseases"
                                    value={diseases}
                                    onChange={e => setDiseases(e.target.value)}
                                    placeholder={'Padecimientos'} />
                            </div>
                            <div className="input-group" style={{ marginRight: '0.5vw', zIndex: '-1', paddingRight: '0.6vw', width: '17.7vw', marginTop: '6vh', transform: 'tranlateZ(-1)!important' }}>
                                <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                                <input
                                    id="curp"
                                    type="text"
                                    className="form-control"
                                    name="curp"
                                    value={curp}
                                    onChange={updateCurp}
                                    placeholder={'CURP'} />
                            </div>

                            <div className="input-group" style={{ marginRight: '0.5vw', paddingRight: '0.6vw', width: '17.7vw', marginTop: '6vh' }}>
                                <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                                <input
                                    id="allergies"
                                    type="text"
                                    className="form-control"
                                    name="allergies"
                                    value={allergies}
                                    onChange={e => setAllergies(e.target.value)}
                                    placeholder={'Alergias'} />
                            </div>

                            <div className="input-group" style={{ marginLeft: '0.5vw', paddingLeft: '0.3vw', paddingRight: '1vw', marginRight: '1vw', width: '17.6vw', marginTop: '6vh', marginBottom: '0.5vw' }}>
                                <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                                <input
                                    id="diseases"
                                    type="text"
                                    className="form-control"
                                    name="diseases"
                                    value={diseases}
                                    onChange={e => setDiseases(e.target.value)}
                                    placeholder={'Padecimientos'} />
                            </div>
                        </div>
                    </form>
                </ModalBody>
                <ModalFooter style={{ padding: '0' }}>
                    <Button onClick={() => setModal(false)} type='submit' color='primary'>Guardar</Button>
                    <Button onClick={() => setModal(false)} color='secondary'>Cancelar</Button>
                </ModalFooter>
            </Modal>
        </>

    );
};

ModalStudent.propTypes = {
    modal: propTypes.bool,
    setModal: propTypes.func
};

export default ModalStudentDetails;