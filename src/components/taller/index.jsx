import React, { useState } from 'react';
import SideBar from '../sideBar';
import Header from '../headerBar';
import { gql, useQuery, useLazyQuery, useMutation } from '@apollo/client';
import { Button } from 'reactstrap';
import ReactTable from 'react-table';
import * as ReactBootstrap from 'react-bootstrap';
import Taller from '../registerStudent/taller';
// import { Glyphicon } from 'react-bootstrap';
// import "react-table/react-table.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import ModalTaller from '../modal/modalTaller';
import ModalStudent from '../modal/modalStudent';
import ModalStudentMod from '../modal/modalStudentMod';
import ModalPedido from '../modal/modalPedido';
import ModalPedidoMod from '../modal/modalPedidoMod';


import jsPDF from 'jspdf';
import 'jspdf-autotable';
import Swal from 'sweetalert2';
import { useHistory } from "react-router-dom";
import { Link } from 'react-router-dom';

//import AdminTeacher from '../adminTeachers';

const OBTENER_TALLERES = gql`
query obtenerTalleres {
    obtenerTalleres {
    id
    nombre
    clave
    existencia
    dia
    fechaInicio
    fechaTermino
    horaInicio
    horaTermino
    maestro{
      id
      nombre
      paterno
      materno
      email
      telefono
      celular
      curp
      sangre
      alergias
      padecimientos
      creado
      grupo
    }
    aula
    creado    
    }
}  
`;
const OBTENER_TALLERESPY = gql`
query obtenerTalleres {
    obtenerTalleres {
        nombre
        clave
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino    
        aula
    }
}  
`;

const OBTENER_TALLER = gql`
    query obtenerTaller($id:ID!){
        obtenerTaller(id:$id){
        id
        nombre
        clave
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino
        maestro{
            id
            nombre
            paterno
            materno
            email
            telefono
            celular
            curp
            sangre
            alergias
            padecimientos      
        }
        aula        
        }
    }
  `;
const OBTENER_PEDIDOS = gql`
    query obtenerPedidos {
        obtenerPedidos{
        id
        estudiante{
            id
            nombre
            email
            semestre
        }
        estado
        }
    }  
`;
const OBTENER_PEDIDOS_TALLER = gql`
    query obtenerPedidosTaller($id:ID!) {
        obtenerPedidosTaller(id:$id){
        id
        pedido{
            id
            cantidad
        }
        estudiante{
            id
            nombre
            paterno
            materno
            email
            nacimiento
            semestre
            genero
            curp
            sangre
            alergias
            padecimientos     
        }
        total    
        }
    }
  `;
  const OBTENER_HISTORIAL = gql`
  query obtenerHistorial($id:ID!){
    obtenerHistorial(id:$id){
      id
      pedido{
        id
        cantidad
      }
      total
      estudiante
      grupo
      fecha
      estado
    }
  }
  `;

  const OBTENER_ESTUDIANTE = gql`
  query obtenerEstudiante($id:ID!){
      obtenerEstudiante(id:$id){
        id
        nombre
        paterno
        materno
        email
        nacimiento
        semestre
        genero
        curp
        sangre
        alergias
        padecimientos
        matricula
      }
    }
  `;
const ELIMINAR_PEDIDO = gql`
    mutation eliminarPedido($id:ID!){
        eliminarPedido(id:$id)
    }  
  `;

  const ELIMINAR_TALLER = gql`
  mutation eliminarTaller($id:ID!){
      eliminarTaller(id:$id)
  }  
`;



const Dashboard = () => {
    let history = useHistory();
    const [club, setClub] = useState([]);
    const [cargando, setCargando] = useState(true);
    const [taller, setTaller] = useState(null);
    const [pedido, setPedido] = useState(null);
    const [studentX, setStudentX] = useState(null);
    const [studentModify, setStudentModify] = useState(null);
    const [realPrefix, setRealPrefix] = useState(null);

    const [realName, setRealName] = useState(null);
    const [realKey, setRealKey] = useState(null);
    const [realQuota, setRealQuota] = useState(null);
    const [realDay, setRealDay] = useState(null);
    const [realScheduleStart, setRealScheduleStart] = useState(null);
    const [realScheduleEnd, setRealScheduleEnd] = useState(null);
    const [realCalendarStart, setRealCalendarStart] = useState(null);
    const [realCalendarEnd, setRealCalendarEnd] = useState(null);
    const [realTeacherName, setRealTeacherName] = useState(null);
    const [realTeacherLastName, setRealTeacherLastName] = useState(null);
    const [realTeacherLastName2, setRealTeacherLastName2] = useState(null);
    const [realClassroom, setRealClassroom] = useState(null);
    const [mensajeX, setMensajeX] = useState(null);
    const [dataH, setDataH] = useState(null);


    var p3d1d0 = null;
    var p3d1d02 = null;

    const [modal, setModal] = useState(null);
    const [modal2, setModal2] = useState(null);
    const [modal3, setModal3] = useState(null);
    const [modal4, setModal4] = useState(null);

    //const [expanded, setExpanded] = useState();
    //const [defaultExpandedRows, setDefaultExpandedRow] = useState();


    // const columns = [
    //     {
    //         Header: "Nombre",
    //         accessor: "nombre"
    //     },
    //     {
    //         Header: "Cupo",
    //         accessor: "existencia"
    //     },
    //     {
    //         Header: "Maestro",
    //         accessor: "maestro"
    //     }
    // ];
    const [eliminarTaller] = useMutation(ELIMINAR_PEDIDO);
    const [eliminarTallerO] = useMutation(ELIMINAR_TALLER);
  


    //const [eliminarTaller] = useMutation(ELIMINAR_PEDIDO);

    //     update(cache) {
    //         // Obtener una copia del objeto de cache
    //         const {obtenerPedidosTaller} = cache.readQuery({ query: OBTENER_PEDIDOS_TALLER });

    //         // Reescribir el cache
    //         cache.writeQuery({
    //             query: OBTENER_PEDIDOS_TALLER,
    //             data: {
    //                 obtenerPedidosTaller: obtenerPedidosTaller.filter(estudianteActual => estudianteActual.id != p3d1d0)
    //             }
    //         })
    //     }
    // });
    const [getTaller] = useLazyQuery(OBTENER_TALLER, {
        variables: { id: taller },
        onCompleted: thisData => {
            // console.log('esto es thisData', thisData);
            // console.log('esto es thisData', thisData.obtenerTaller.maestro.nombre);
            setRealPrefix('Taller de ');
            setRealName(thisData.obtenerTaller.nombre);
            setRealKey(thisData.obtenerTaller.clave);
            setRealDay(thisData.obtenerTaller.dia);
            setRealQuota(thisData.obtenerTaller.existencia);
            setRealScheduleStart(thisData.obtenerTaller.horaInicio);
            setRealScheduleEnd(thisData.obtenerTaller.horaTermino);
            setRealCalendarStart(thisData.obtenerTaller.fechaInicio);
            setRealCalendarEnd(thisData.obtenerTaller.fechaTermino);
            setRealTeacherName(thisData.obtenerTaller.maestro.nombre);
            setRealTeacherLastName(thisData.obtenerTaller.maestro.paterno);
            setRealTeacherLastName2(thisData.obtenerTaller.maestro.materno);
            setRealClassroom(thisData.obtenerTaller.aula);
            // realKey = thisData.obtenerTaller.clave;
            // realDay = thisData.obtenerTaller.dia;
            // realScheduleStart = thisData.obtenerTaller.horaInicio;
            // realScheduleEnd = thisData.obtenerTaller.horaTermino;
            // realCalendarStart = thisData.obtenerTaller.fechaInicio;
            // realCalendarEnd = thisData.obtenerTaller.fechaTermino;
            // realTeacherName = thisData.obtenerTaller.maestro.nombre;
            // realTeacherLastName = thisData.obtenerTaller.maestro.paterno;
            // realTeacherLastName2 = thisData.obtenerTaller.maestro.materno;
            // realClassroom = thisData.obtenerTaller.aula;
            console.log('esto es quota', realQuota);
            setMensajeX(true);
            setModal3(true);



        }
    });
    const [getHistorialX] = useLazyQuery(OBTENER_HISTORIAL, {
        onCompleted: thisData => {

            
            // setStudentX(p3d1d0);
            // console.log('studentx', studentX)
            // console.log('pedido', p3d1d0);
            setModal4(true);
            //setStudentX(p3d1d0);
            setDataH(thisData);
            p3d1d0 = thisData.obtenerHistorial.estudiante; 
            //console.log('Esto es pedido cuando llega', p3d1d0);
            //setStudentX(p3d1d0);
           
           
        }
    });
    const [getStudentX] = useLazyQuery(OBTENER_ESTUDIANTE, {
        onCompleted: thisData => {

            p3d1d0 = thisData.obtenerEstudiante;
            setStudentX(p3d1d0);
            console.log('studentx', studentX)
            console.log('pedido', p3d1d0);
            setModal(true);
            setStudentX(p3d1d0);
           
        }
    });
    const [getStudentY] = useLazyQuery(OBTENER_ESTUDIANTE, {
        onCompleted: thisData => {

            p3d1d0 = thisData.obtenerEstudiante;
            setStudentX(p3d1d0);
            console.log('studentx', studentX)
            console.log('pedido', p3d1d0);
            setModal2(true);
            setStudentX(p3d1d0);
        }
    });
    const { data: dataR, error: errorR, loading: loadingR } = useQuery(OBTENER_TALLERES);
    
    const { data: dataRY, error: errorRY, loading: loadingRY } = useQuery(OBTENER_TALLERESPY);

    const { data, error, loading } = useQuery(OBTENER_PEDIDOS);

    const [loadHeader, { data: dataT, error: errorT, loading: loadingT }] = useLazyQuery(OBTENER_TALLER, {
        variables: { id: taller },
    });

    var loadHeader2 = function () {

    }
    var loadHeader3 = function () {

    }

    // const [loadOptions, { data: players, error: errorZ, loading: loadingZ }] = useLazyQuery(OBTENER_TALLER, {
    //     variables: { id: taller },
    //     // onCompleted: data => {
    //     //     console.log('Variable donde se almacenarán los pedidos por taller' + pedido);
    //     //     console.log("Que es data" + data.obtenerPedidosTaller[0]);
    //     //     console.log('Que es players' + players.obtenerPedidosTaller[0]);
    //     //     setPedido(players);
    //     //     console.log("Pedidos por taller después de setear: " + pedido);

    //     // }
    //     // skip: { id: null }
    // });
    const [loadOptions, { data: players, error: errorZ, loading: loadingZ }] = useLazyQuery(OBTENER_PEDIDOS_TALLER, {
        variables: { id: taller },
        onCompleted: thisData => {

            p3d1d0 = thisData.obtenerPedidosTaller;

            console.log('pedidotaller', p3d1d02);

        }
        // skip: { id: null }
    });
    if (loadingZ || loadingR || loading || loadingT || loadingRY) return 'Cargando ...';
    // <p>Cargando ...</p>;
    console.log("players después de loading" + players);
    //return <h1>Hello {players}!</h1>;
    //const { data: players, error: errorZ, loading: loadingZ } = useQuery(OBTENER_PEDIDOS_TALLER);


    console.log(dataR);
    // console.log(players.obtenerPedidosTaller);
    console.log('Este es Club: ' + club);
    console.log('Este es cargando' + cargando);

    console.log(data);
    console.log(loading);
    console.log(error);
    console.log('Este es taller, que trae el id, del taller seleccionado: ' + taller)
    //const { id, estudiante, estado } = dataR.obtenerPedidos;

    // const content = data.obtenerTalleres.map((taller) =>
    //     <div key={taller.id}>
    //         <h3>{taller.nombre}</h3>
    //         <p>{taller.cupo}</p>
    //     </div>
    // );

    // const content2 = data.obtenerTalleres.map((taller) =>
    //     <div key={taller.id}>
    //         <h3>{taller.cupo}</h3>
    //         <p>{taller.maestro}</p>
    //     </div>
    // );

    // const content3 = data.obtenerTalleres.map((taller) =>
    //     <div key={taller.id}>
    //         <h3>{taller.existencia}</h3>
    //     </div>
    // );
    // const content4 = data.obtenerTalleres.map((taller) =>
    //     <div key={taller.id}>
    //         <h3>{taller.aula}</h3>
    //     </div>
    // );

    // const players = [
    //     { position: 'forward', name: 'Jordan', team: 'Bulls' },
    //     { position: 'forward', name: 'Kobe', team: 'Lakers' },
    //     { position: 'forward', name: 'Lebron', team: 'Mavericks' },
    //     { position: 'guard', name: 'Rodman', team: 'Bulls' },
    //     { position: 'guard', name: 'Yeah', team: 'Cool' },
    // ];

    // const renderPlayer = (player, index) => {
    //     return (
    //         <tr key={index} >
    //             <td> {player.name} </td>
    //             <td> {player.position} </td>
    //             <td> {player.team} </td>
    //         </tr>
    //     )
    // }

    //const defaultExpandedRows = data.map((element, index) => { return { index: true } });

    // const onExpandedChange = (newExpanded) => {
    //     setExpanded(newExpanded);


    // };
    // const rTeacher = () => {
    //     return <AdminTeacher />
    // }

    var esTaller = true;
    var esClub = false;

    var placeToHold = "Elija Taller...";
    console.log(players);
    //console.log(players.obtenerPedidosTaller);
    const renderPlayer = (player, index) => {
        return (
            <tr key={player.id} >
                <td> {player.estudiante.nombre} </td>
                <td> {player.estudiante.paterno} </td>
                <td> {player.estudiante.materno} </td>
                <td> {player.estudiante.curp} </td>                
                <td> {player.estudiante.semestre} </td>
                <td> {player.estudiante.email} </td>
                <td  >
                    {/* <a href="#" class="btn btn-success btn-lg">
                        <span class="glyphicon glyphicon-remove"></span> Borrar
                    </a> */}

                    <a style={{ color: '#6A5ACD', marginRight: '0.5vw' }} onClick={() => getStudentX({ variables: { id: player.estudiante.id }})}>
                        <span className="fas fa-eye"></span>
                        <ModalStudent
                            modal={modal}
                            setModal={setModal}
                            studentX={studentX}
                        />
                    </a>


                    <a style={{ color: '#9400D3', marginRight: '0.5vw' }} onClick={() => {setStudentX(player.estudiante.id);getHistorialX({ variables: { id: player.estudiante.id },  })}}>
                        <span className="fas fa-edit"></span>
                        <ModalPedidoMod
                            modal4={modal4}
                            setModal4={setModal4}
                            studentX={studentX}
                            dataH={dataH}
                            club={club}
                            setClub={setClub}
                            p3d1d0={p3d1d0}

                        />
                    </a>
                    <a style={{ color: 'red', marginRight: '0vw' }} onClick={() =>
                        Swal.fire({
                            title: '¿Deseas eliminar del taller este estudiante?',
                            text: "Esta acción no podrá deshacerse.",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: 'red',
                            cancelButtonColor: '#9400D3',
                            confirmButtonText: 'Sí, Elimínalo',
                            cancelButtonText: 'No, Cancelar'
                        }).then(async (result) => {
                            if (result) {
                                try {
                                    console.log("justo antes de eliminar" + player.estudiante.id);
                                    p3d1d0 = player.id;
                                    console.log("3st3 3s p3d1d0", p3d1d0);
                                    const { data } = await eliminarTaller({
                                        variables: {
                                            id: player.id
                                        },
                                        refetchQueries: [
                                            {
                                                query: OBTENER_PEDIDOS_TALLER,
                                                variables: { id: player.id }
                                            }
                                        ]

                                    })
                                    renderPlayer();
                                } catch (error) {
                                    p3d1d0 = player.id;
                                    console.log("3st3 3s p3d1d0", p3d1d0);
                                    console.log("justo antes de eliminar" + player.id);
                                    console.log(error);
                                }
                                if (result.value) {
                                    history.push('/dashboard/taller');
                                    Swal.fire(
                                        'Eliminado',
                                        "El estudiante se ha eliminado exitosamente",
                                        'success'
                                    )
                                }
                            }

                        })

                    }>
                        <span className="fas fa-trash-alt"></span>
                    </a>



                    {/* <button type="button" class="buttonDelete">
                    </button> */}

                </td>
            </tr>
        )
    }

    const headerReady = () => {
        return (
            <tr style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>
                <th colSpan='5' style={{ fontSize: '22px', paddingBottom: '3vh' }}>{dataT.obtenerTaller.nombre}</th>
                <th colSpan='1' style={{ paddingTop: '0', paddingBottom: '0' }}>
                    <tr>
                        Maestro:
                  
                    </tr>
                    <tr style={{ fontSize: '12px' }}>
                        {dataT.obtenerTaller.maestro.nombre} {dataT.obtenerTaller.maestro.paterno} {dataT.obtenerTaller.maestro.materno}
                    </tr>
                    <tr style={{ fontSize: '12px' }}>
                        {dataT.obtenerTaller.aula}
                    </tr>

                </th>
                <th colSpan='1' style={{ paddingTop: '0', paddingBottom: '0' }}>
                    {/* <tr style={{marginBottom: '0.5vw'}}>
                        Opciones
                    </tr> */}
                    <tr>
                        <a style={{ color: '#9400D3', marginRight: '0.5vw' }} onClick={() => getTaller()}>
                        <span className="fas fa-eye"></span>
                        <ModalPedido
                            modal3={modal3}
                            setModal3={setModal3}
                            studentX={studentX}
                            taller={taller}
                            realName={realName}
                            realKey ={realKey}
                            realQuota = {realQuota}
                            realDay = {realDay}
                            realScheduleStart={realScheduleStart}
                            realScheduleEnd = {realScheduleEnd}
                            realCalendarStart={realCalendarStart}
                            realCalendarEnd={realCalendarEnd}
                            realDay={realDay}
                            realTeacherName={realTeacherName}
                            realTeacherLastName={realTeacherLastName}
                            realTeacherLastName2={realTeacherLastName2}
                            realClassroom={realClassroom} 
                            realPrefix={realPrefix}
                            mensajeX={mensajeX}
                            setMensajeX={setMensajeX}
                        />
                        </a>
                        <a style={{ color: '#9400D3', marginRight: '0.5vw' }} href="#">
                            <span className="fas fa-edit"></span>
                        </a>
                        <a style={{ color: 'red', marginRight: '0vw' }} onClick={() => 
                        
                        Swal.fire({
                            title: '¿Deseas eliminar del taller este estudiante?',
                            text: "Esta acción no podrá deshacerse.",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: 'red',
                            cancelButtonColor: '#9400D3',
                            confirmButtonText: 'Sí, Elimínalo',
                            cancelButtonText: 'No, Cancelar'
                        }).then(async (result) => {
                            if (result) {
                                try {
                                    console.log("justo antes de eliminar" + dataT.obtenerTaller.id);
                                    p3d1d0 = dataT.obtenerTaller.id;
                                    console.log("3st3 3s p3d1d0", p3d1d0);
                                    // const { data } = await eliminarTallerO({
                                    //     variables: {
                                    //         id: dataT.obtenerTaller.id
                                    //     },
                                    //     refetchQueries: [
                                    //         {
                                    //             query: OBTENER_TALLER,
                                    //             variables: { id: dataT.obtenerTaller.id }
                                    //         }
                                    //     ]

                                    // })
                                    renderPlayer();
                                } catch (error) {
                                    p3d1d0 = dataT.obtenerTaller.id;
                                    console.log("3st3 3s p3d1d0", p3d1d0);
                                    console.log("justo antes de eliminar" + dataT.obtenerTaller.id);
                                    console.log(error);
                                }
                                if (result.value) {
                                    history.push('/dashboard/taller');
                                    Swal.fire(
                                        'Eliminado',
                                        "El estudiante se ha eliminado exitosamente",
                                        'success'
                                    )
                                }
                            }

                        })

                    }>
                        <span className="fas fa-trash-alt"></span>
                    </a>
                        {/* <a style={{ color: 'red', marginRight: '0vw' }} href="#"
                            
                        // onClick={() =>

                        //     Swal.fire({
                        //         title: '¿Deseas eliminar este Taller?',
                        //         text: "Esta acción no podrá deshacerse.",
                        //         icon: 'warning',
                        //         showCancelButton: true,
                        //         confirmButtonColor: 'red',
                        //         cancelButtonColor: '#9400D3',
                        //         confirmButtonText: 'Sí, Elimínalo',
                        //         cancelButtonText: 'No, Cancelar'
                        //     }).then(async (result) => {
                        //         if (result) {
                        //             try {
                        //                 console.log("justo antes de eliminar" + taller);
                        //                 console.log("3st3 3s t4ll3r", taller);
                        //                 const { data } = await eliminarTaller({
                        //                     variables: {
                        //                         id: taller
                        //                     },
                        //                     refetchQueries: [
                        //                         {
                        //                             query: OBTENER_PEDIDOS_TALLER,
                        //                             variables: { taller }
                        //                         }
                        //                     ]

                        //                 })
                        //                 renderPlayer();
                        //             } catch (error) {
                        //                 console.log("3st3 3s t4ll3r", taller);
                        //                 console.log(error);
                        //             }
                        //             if (result.value) {
                        //                 Swal.fire(
                        //                     'Eliminado',
                        //                     "El Taller se ha eliminado exitosamente",
                        //                     'success'
                        //                 )
                        //             }
                        //         }

                        //     })


                        // }
                        >
                            <span className="fas fa-trash-alt"></span>
                        </a> */}

                    </tr>
                </th>

            </tr>
        );
        // if (taller === '5ebfe0272b0a583094431c36') {
        //     return (
        //         <tr>
        //             <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE DESARROLLO DOCENTE</th>
        //         </tr>
        //     );
        // }
        // if (taller === '5ebfb65bc4e0b63dc0f52f37') {
        //     return (
        //         <tr>
        //             <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE MÚSICA</th>
        //         </tr>
        //     );
        // }
        // if (taller === '5ebfb615c4e0b63dc0f52f36') {
        //     return (
        //         <tr>
        //             <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE DANZA</th>
        //         </tr>
        //     );
        // }
        // if (taller === '5ebfb5dfc4e0b63dc0f52f35') {
        //     return (
        //         <tr>
        //             <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE APRECIACIÓN DE LAS ARTES</th>
        //         </tr>
        //     );
        // }

    }
    // const expandRow = {
    //     renderer: row => (
    //         <div>
    //             <p>{`This Expand row is belong to rowKey ${row.id}`}</p>
    //             <p>You can render anything here, also you can add additional data on every row object</p>
    //             <p>expandRow.renderer callback will pass the origin row object to you</p>
    //         </div>
    //     )
    // };
    // const tablePDF = () => {
    //     const doc = new jsPDF();

    //     doc.autoTable({ tableReady });

    //     doc.save('tableReady.pdf');
    // }

    var resultXY = Object.values(dataRY.obtenerTalleres);

    var arrTallersXY = [];
     for (var i = 0; i < resultXY.length; i++) {
        arrTallersXY.push(Object.values(resultXY[i]));
        }

    var columns = [
        { title: "Tipo", dataKey: "Tipo" },
        { title: "Nombre", dataKey: "Nombre" },
        { title: "Clave", dataKey: "Clave" },
        { title: "Cupo", dataKey: "Cupo" },
        { title: "Día", dataKey: "Día" },
        { title: "Inicio", dataKey: "Inicio" },
        { title: "Término", dataKey: "Término" },
        // { title: "T.Sangre", dataKey: "Sangre" },
        // { title: "Alergias", dataKey: "Alergias" },
        // { title: "Padecimientos", dataKey: "Padecimientos" },
    ];

    // const tablePDF = () => {
    //     var doc = new jsPDF('p', 'pt');
    //     doc.autoTable(columns, arrTallersXY);
    //     doc.save('tableReady.pdf');
    //     // const doc = new jsPDF();

    //     // doc.autoTable({ tableReady });

    //     // doc.save('tableReady.pdf');
    // }

    const tableReady = () => {
        if (cargando === true || taller === null) {
            return (
                <div className='dashboardHeader' style={{ marginLeft: '0.5vw', marginTop: '0.7vh', borderRadius: '3px', width: '18.5vw', backgroundImage: 'linear-gradient(to right, #8A2BE2, #DA70D6)', opacity: '0.9', textAlign: 'center' }}>
                    <p>No hay aún datos para mostrar</p>
                </div>
            );
        }
        return (
            <ReactBootstrap.Table striped bordered hover style={{ backgroundColor: 'white', marginBottom: '0vh' }}
            // SubComponent={row => {
            //     return (
            //         <div style={{ 'border': '1px solid rgba(0, 0, 0, 0.05)' }}>
            //             {row.row.summary}
            //         </div>
            //     )
            // }}
            // onExpandedChange={newExpanded => onExpandedChange(newExpanded)}


            >
                <thead style={{ fontWeight: '500px', font: 'Roboto', color: 'white', backgroundImage: 'linear-gradient(to right, #BA55D3, #DDA0DD)' }}>
                    {headerReady()}
                    <tr>
                        <th>Nombre</th>
                        <th>Paterno</th>
                        <th>Materno</th>
                        <th>CURP</th>                        
                        <th>Semestre</th>
                        <th>Email</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    {players.obtenerPedidosTaller.map(renderPlayer)}
                </tbody>
            </ReactBootstrap.Table>
        );
    }

    return (
        <div className='main'>
            <Header />
            <SideBar />
            <div className='content' style={{ marginLeft: '0vw' }}>

                <Taller
                    club={club}
                    setClub={setClub}
                    dataR={dataR}

                    loadingR={loadingR}
                    errorR={errorR}
                    setTaller={setTaller}
                    setCargando={setCargando}
                    loadHeader={loadHeader}
                    loadHeader2={loadHeader2}
                    loadHeader3={loadHeader3}
                    loadOptions={loadOptions}
                    esTaller={esTaller}
                    esClub={esClub}

                    placeToHold={placeToHold}
                />
                <div style={{ display: 'flex', flexDirection: 'column', marginLeft: '1vw' }}>
                    {Taller && tableReady()}
                    <div style={{ display: 'flex', marginLeft: '-0.3vw', marginTop: '1vh' }}>
                        <Button style={{ borderRadius: '3px', opacity: '0.9', width: '10vw !important', paddingLeft: "0px !important", paddingRight: "0px !important", height: '8vh' }}
                          onClick={()=>{
                            var doc = new jsPDF('p', 'pt');
                            doc.autoTable(columns, arrTallersXY);
                            doc.save('tableReady.pdf');
                          }}
                        >
                            <span class="fas fa-print"></span> Reporte
                        </Button>
                        <Link to='/dashboard'>
                            <Button style={{ borderRadius: '3px', opacity: '0.9', width: '10vw !important', paddingLeft: "0px !important", paddingRight: "0px !important", height: '8vh' }}
                            // onClick={tablePDF()}
                            >
                                <span class="fas fa-home"></span> Regresar
                            </Button>
                        </Link>
                    </div>

                </div>



            </div>
            <div className='footer'>
                <div>
                    <span class="fas fa-university" style={{ marginRight: '0.3vw' }}></span>
                    Copyright © 2020, B3st0 Team
                </div>
            </div>
        </div>
    );

}

export default Dashboard;