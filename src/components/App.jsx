import React from 'react';
import '../styles.css';
import Login from './login';
import Dashboard from './dashboard';
import Contact from './contact';
import Register from './registerStudent';
import RegisterX from './registerStudentX';

import Taller from './taller';
//import Clubs from './registerClub';
import Club from './club';
import Student from './student';
import Teacher from './teacher';
import Support from './support';
import Agenda from './agenda';
import User from './users';
import AdminStudent from './adminStudent';
import AdminTeacher from './adminTeacher';
import AdminTaller from './adminTaller';
import AdminClub from './adminClub';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { AllProvider } from './context/AllContext';
import gql from 'graphql-tag';
import { useQuery } from '@apollo/react-hooks';
import { ApolloProvider } from '@apollo/client';
import Client from '../config/apollo';

const App = ({ Component, pageProps }) => {

  return (
    <ApolloProvider client={Client}>
      <AllProvider>
        <Router>
          <Switch>
            <Route path="/" exact component={Login} />
            <Route path="/dashboard" exact component={Dashboard} />
            <Route path="/dashboard/contact" component={Contact} />
            <Route path="/dashboard/register" component={RegisterX} />
            <Route path="/dashboard/taller" component={Taller} />
            <Route path="/dashboard/club" component={Club} />
            <Route path="/dashboard/student" component={Student} />
            <Route path="/dashboard/teacher" component={Teacher} />
            <Route path="/dashboard/support" component={Support} />
            <Route path="/dashboard/agenda" component={Agenda} />
            <Route path="/dashboard/admin/student" exact component={AdminStudent} />
            <Route path="/dashboard/admin/teacher" exact component={AdminTeacher} />
            <Route path="/dashboard/admin/taller" exact component={AdminTaller} />
            <Route path="/dashboard/admin/club" exact component={AdminClub} />
            <Route path="/dashboard/admin/user" exact component={User} />
            <Route path="/external" component={() => { window.location = 'http://enso.creson.edu.mx/'; return null; }} />
          </Switch>
        </Router>
      </AllProvider>
    </ApolloProvider>

  );

};

export default App;
