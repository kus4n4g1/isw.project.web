import React from 'react';

const Form = () => {

    return (
        <div className='footer'>
            <div>
                <span class="fas fa-university" style={{ marginRight: '0.3vw' }}></span>
                Copyright © 2020, B3st0 Team
            </div>
        </div>
    );
}

export default Form;