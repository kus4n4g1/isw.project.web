import React, { useState } from 'react';
import Select from 'react-select';
import propTypes from 'prop-types';

const ClubGender = (props) => {

    const {
        value,
        handleChange,
        onChange,
        onBlur,
        errors,
        touched,
        setClubGender,
    } = props;
    const [selectedGender, setSelectedGender] = useState("");

    const handleGenderChange = (selectedGender, value) => {
        value = selectedGender.value;
        console.log(selectedGender);
        setSelectedGender(selectedGender);
        setClubGender(selectedGender.value);
    };
    const options = [
        { value: 'Club General', label: 'Club General' },
        { value: 'Club Femenil', label: 'Club Femenil' },
        { value: 'Club Varonil', label: 'Club Varonil' },

    ];

    return (
        <Select
            id="clubGender"
            name="clubGender"
            placeholder={'Tipo de Club '}
            className='selectStyle4X'
            options={options}
            style={{ fontSize: '16px' }}
            menuPlacement="auto"
            menuPosition="fixed"
            errors={errors}
            touched={touched}
            onBlur={onBlur}
            onChange={selectedOption => {
                handleGenderChange(selectedOption);
                console.log("values", value);
                handleChange("clubGender")(selectedOption.value);;
            }}

        // style={{ width: '18.2vw' }}
        />
    );
}

ClubGender.propTypes = {
    value: propTypes.object,
    handleChange: propTypes.func,
    onChange: propTypes.func,
    onBlur: propTypes.func,
    errors: propTypes.object,
    touched: propTypes.object,
    setClubGender: propTypes.func,
};

export default ClubGender;