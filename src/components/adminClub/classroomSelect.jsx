import React, { useState } from 'react';
import Select from 'react-select';
import propTypes from 'prop-types';

const ClassroomSelect = (props) => {
    const {
        value,
        handleChange,
        onChange,
        onBlur,
        errors,
        touched,
    } = props;

    const [selectedClassroom, setSelectedClassroom] = useState("");

    const handleClassroomChange = (selectedClassroom, value) => {
        value = selectedClassroom.value;
        console.log(selectedClassroom);
        setSelectedClassroom(selectedClassroom);
    };

    const options = [
        { value: 'Aula 01', label: 'Aula 01' },
        { value: 'Aula 02', label: 'Aula 02' },
        { value: 'Aula 03', label: 'Aula 03' },
        { value: 'Aula 04', label: 'Aula 04' },
        { value: 'Aula 05', label: 'Aula 05' },
        { value: 'Aula 06', label: 'Aula 06' },
        { value: 'Aula 07', label: 'Aula 07' },
        { value: 'Aula 08', label: 'Aula 08' },
        { value: 'Aula 09', label: 'Aula 09' },
        { value: 'Aula Magna', label: 'Aula Magna' },
        { value: 'Area Deportiva', label: 'Area Deportiva' },
        { value: 'Biblioteca', label: 'Biblioteca' },

    ];

    return (
        <Select
            id="classroom"
            name="classroom"
            options={options}
            className='selectStyle3X'
            // style={{ width: '20vw !important', zIndex: '999' }}
            placeholder='Elija el aula'
            menuPlacement="bottom"
            menuPosition="fixed"
            errors={errors}
            touched={touched}
            onBlur={onBlur}
            onChange={selectedOption => {
                handleClassroomChange(selectedOption);
                console.log("values", value);
                handleChange("classroom")(selectedOption.value);;
            }}
        />
    );

}
ClassroomSelect.propTypes = {
    value: propTypes.object,
    handleChange: propTypes.func,
    onChange: propTypes.func,
    onBlur: propTypes.func,
    errors: propTypes.object,
    touched: propTypes.object
};
export default ClassroomSelect;