import React, { useState, useContext } from 'react';
import { Button } from 'reactstrap';
import Header from '../headerBar';
import SideBar from '../sideBar';
import Footer from '../footer';
import Taller from '../registerStudent/taller';
import { Link } from 'react-router-dom';
import { AllContext } from '../context/AllContext';
import { gql, useQuery, useMutation } from '@apollo/client';
import { Route, Redirect, useHistory } from "react-router-dom";
import Swal from 'sweetalert2';
import { Formik, useFormik, useField, useFormikContext } from 'formik';
//import { Form } from 'react-formik-ui';

import * as Yup from 'yup';
import CalendarStart from '../calendar/calendarStart';
import CalendarEnd from '../calendar/calendarEnd';
import ScheduleStart from '../calendar/scheduleStart';
import ScheduleEnd from '../calendar/scheduleEnd';
import DaySelect from './daySelect';
import TeacherSelect from './teacherSelect';
import ClassroomSelect from './classroomSelect';
import ClubGender from './clubGender';

const NUEVO_TALLER = gql`
mutation nuevoTaller($input:TallerInput){
    nuevoTaller(input:$input){
      id
      nombre
      existencia
      clave
      dia
      fechaInicio
      fechaTermino
      horaInicio
      horaTermino    
      aula
      creado
    }
  }
`;
const NUEVO_CLUB = gql`
mutation nuevoClub($input:ClubInput){
    nuevoClub(input:$input){
      id
      nombre
      clave
      existencia
      dia
      fechaInicio
      fechaTermino
      horaInicio
      horaTermino
      aula
      creado
    }
  }
`;
const NUEVO_CLUB_MASCULINO = gql`
mutation nuevoClubMasculino($input:ClubMasculinoInput){
    nuevoClubMasculino(input:$input){
      id
      nombre
      existencia
      dia
      fechaInicio
      fechaTermino
      horaInicio
      horaTermino     
      aula
      creado
    }
  }
`;
const NUEVO_CLUB_FEMENINO = gql`
mutation nuevoClubFemenino($input:ClubFemeninoInput){
    nuevoClubFemenino(input:$input){
      id
      nombre
      existencia
      dia
      fechaInicio
      fechaTermino
      horaInicio
      horaTermino    
      aula
      creado
    }
  }
`;

const OBTENER_MAESTROS = gql`
    query obtenerMaestros{
        obtenerMaestros{
        id
        nombre
        paterno
        materno
        email
        telefono
        celular
        curp
        sangre
        alergias
        padecimientos
        creado
        grupo
        }
    }  
`;

const FormAdminClub = () => {
    let history = useHistory();
    const value = useContext(AllContext);

    // const [name, setName] = useState('');
    // const [lastName, setLastName] = useState('');
    // const [lastName2, setLastName2] = useState('');
    // const [curp, setCurp] = useState('');
    // const [phone, setPhone] = useState('');
    // const [allergies, setAllergies] = useState('');
    // const [bloodType, setBloodType] = useState('');
    // const [email, setEmail] = useState('');
    // const [diseases, setDiseases] = useState('');
    // const [cellPhone, setCellPhone] = useState('');
    // const [club, setClub] = useState(null);
    const [mensaje, guardarMensaje] = useState(null);
    const [clubGender, setClubGender] = useState(null);

    const [nuevoTaller] = useMutation(NUEVO_TALLER);
    const [nuevoClub] = useMutation(NUEVO_CLUB);
    const [nuevoClubMasculino] = useMutation(NUEVO_CLUB_MASCULINO);
    const [nuevoClubFemenino] = useMutation(NUEVO_CLUB_FEMENINO);
    const { data: dataR, error: errorR, loading: loadingR } = useQuery(OBTENER_MAESTROS);

    //Schema de validación
    const schemaValidacion = Yup.object({
        clubGender: Yup.string()
            .required('Tipo de club es obligatorio*'),
        name: Yup.string()
            .required('Nombre es obligatorio*'),
        availability: Yup.number()
            .required('Cupo es obligatorio*')
            .positive()
            .integer(),
        clubKey: Yup.string()
            .required('Clave Club/Taller obligatoria*'),
        day: Yup.string()
            .required('Dia es obligatorio*'),
        calendarStart: Yup.date()
            .required('Fecha de inicio obligatoria*'),
        calendarEnd: Yup.date()
            .required('Fecha de término obligatoria*'),
        scheduleStart: Yup.date()
            .required('Hora de inicio obligatoria*'),
        scheduleEnd: Yup.date()
            .required('Hora de término obligatoria*'),
        teacher: Yup.string()
            .required('Maestro es obligatorio*'),
        classroom: Yup.string()
            .required('Aula es obligatoria*'),

    });

    const initialValues = {
        clubGender: '',
        name: '',
        availability: undefined,
        day: '',
        calendarStart: '',
        calendarEnd: '',
        scheduleStart: '',
        scheduleEnd: '',
        teacher: '',
        classroom: '',
        clubKey: '',
    };

    // const formik = useFormik({
    //     initialValues: {
    //         name: '',
    //         availability: '',
    //         day: '',
    //         calendarStart: '',
    //         calendarEnd: '',
    //         scheduleStart: '',
    //         scheduleEnd: '',
    //         teacher: '',
    //         classroom: '',
    //         clubKey: '',
    //     },
    //     validationSchema: Yup.object({
    //         name: Yup.string()
    //             .required('Nombre es obligatorio*'),
    //         availability: Yup.number()
    //             .required('Cupo es obligatorio*'),
    //         clubKey: Yup.string()
    //             .required('Clave Club/Taller obligatoria*'),
    //         day: Yup.string()
    //             .required('Dia es obligatorio*'),
    //         calendarStart: Yup.date()
    //             .required('Fecha de inicio obligatoria*'),
    //         calendarEnd: Yup.date()
    //             .required('Fecha de término obligatoria*'),
    //         scheduleStart: Yup.date()
    //             .required('Hora de inicio obligatoria*'),
    //         scheduleEnd: Yup.date()
    //             .required('Hora de término obligatoria*'),
    //         teacher: Yup.string()
    //             .required('Maestro es obligatorio*'),
    //         classroom: Yup.string()
    //             .required('Aula es obligatoria*'),
    //         //.test('len', 'Teléfono de 10 dígitos', val => val.length === 10),


    //     }),
    //     onSubmit: async valores => {
    //         console.log('enviando');
    //         console.log(valores);
    //         const { name, availability, clubKey, day, calendarStart, calendarEnd, scheduleStart, scheduleEnd, teacher, classroom } = valores
    //         try {
    //             const { data } = await nuevoTaller({
    //                 variables: {
    //                     input: {
    //                         nombre: name,
    //                         clave: clubKey,
    //                         existencia: availability,
    //                         dia: day,
    //                         fechaInicio: calendarStart,
    //                         fechaTermino: calendarEnd,
    //                         horaInicio: scheduleStart,
    //                         horaTermino: scheduleEnd,
    //                         maestro: teacher,
    //                         aula: classroom
    //                     }
    //                 }
    //             })
    //             // //Redireccionar
    //             Swal.fire({
    //                 title: 'Correcto',
    //                 text: "El taller/club se registró con éxito!",
    //                 icon: 'success',
    //                 showCancelButton: true,
    //                 confirmButtonColor: '#9400D3',
    //                 cancelButtonColor: '#3085d6',
    //                 confirmButtonText: 'Volver a Inicio',
    //                 cancelButtonText: 'Seguir registrando'
    //             }).then((result) => {
    //                 if (result.value) {
    //                     history.push('/dashboard');
    //                     Swal.fire(
    //                         'Correcto',
    //                         'El maestro se registró correctamente',
    //                         'success'
    //                     )
    //                 } else {
    //                     history.push('/dashboard/admin/teacher')
    //                 }
    //             })
    //             // history.push('/dashboard');
    //             // //Mensaje de éxito                                
    //             // Swal.fire(
    //             //     'Correcto',
    //             //     'El maestro se registró correctamente',
    //             //     'success'
    //             // )
    //         } catch (error) {
    //             console.log(error);
    //         }


    //     }
    // });

    if (loadingR) return 'Cargando...';

    console.log('Esto es obtenerMaestros: ' + dataR);


    // const token = localStorage.getItem('token', token);
    // console.log(token);
    //Mutation para autenticar nuevos usuarios en apollo
    //validación de formulario

    // const { obtenerMaestros } = dataR;
    console.log('thus oneeee', clubGender);

    return (

        <Formik
            validationSchema={schemaValidacion}
            enableReinitialize
            //initialvalues={ obtenerMaestros}
            initialValues={initialValues}
            onSubmit={async (valores, funciones) => {
                console.log('enviando');
                console.log(valores);
                const { name, availability, clubKey, day, calendarStart, calendarEnd, scheduleStart, scheduleEnd, teacher, classroom, clubGender } = valores
                try {
                    console.log("ClubGender antes de la mutación: " + clubGender);
                    console.log('Tipo de variable de ClubGender', clubGender);
                    if (clubGender === 'Club General') {
                        console.log('Entré a la condición de club general');
                        const { data } = await nuevoClub({
                            variables: {
                                input: {
                                    nombre: name,
                                    clave: clubKey,
                                    existencia: availability,
                                    dia: day,
                                    fechaInicio: calendarStart,
                                    fechaTermino: calendarEnd,
                                    horaInicio: scheduleStart,
                                    horaTermino: scheduleEnd,
                                    maestro: teacher,
                                    aula: classroom
                                }
                            }
                        })
                    }
                    if (clubGender === 'Club Femenil') {
                        const { data } = await nuevoClubFemenino({
                            variables: {
                                input: {
                                    nombre: name,
                                    clave: clubKey,
                                    existencia: availability,
                                    dia: day,
                                    fechaInicio: calendarStart,
                                    fechaTermino: calendarEnd,
                                    horaInicio: scheduleStart,
                                    horaTermino: scheduleEnd,
                                    maestro: teacher,
                                    aula: classroom
                                }
                            }
                        })
                    }
                    if (clubGender === 'Club Varonil') {
                        const { data } = await nuevoClubMasculino({
                            variables: {
                                input: {
                                    nombre: name,
                                    clave: clubKey,
                                    existencia: availability,
                                    dia: day,
                                    fechaInicio: calendarStart,
                                    fechaTermino: calendarEnd,
                                    horaInicio: scheduleStart,
                                    horaTermino: scheduleEnd,
                                    maestro: teacher,
                                    aula: classroom
                                }
                            }
                        })
                    }

                    // //Redireccionar
                    Swal.fire({
                        title: 'Correcto',
                        text: "El taller/club se registró con éxito!",
                        icon: 'success',
                        showCancelButton: true,
                        confirmButtonColor: '#9400D3',
                        cancelButtonColor: '#3085d6',
                        confirmButtonText: 'Volver a Inicio',
                        cancelButtonText: 'Seguir registrando'
                    }).then((result) => {
                        if (result.value) {
                            history.push('/dashboard');
                            Swal.fire(
                                'Correcto',
                                'El Taller se registró correctamente',
                                'success'
                            )
                        } else {
                            history.push('/dashboard/admin/teacher')
                        }
                    })
                    // history.push('/dashboard');
                    // //Mensaje de éxito                                
                    // Swal.fire(
                    //     'Correcto',
                    //     'El maestro se registró correctamente',
                    //     'success'
                    // )
                } catch (error) {
                    console.log(error);
                }
            }}




        >
            {props => {

                console.log(props);

                return (


                    <form onSubmit={props.handleSubmit} className="main2">
                        <Header></Header>
                        <SideBar />
                        <div className='content'>
                            <div className='headersGrid' style={{ marginLeft: '-8.8vw', marginRight: '3vw' }}>
                                <span class="fas fa-chess-king" style={{ marginLeft: '-0.5vw', marginRight: '0.3vw' }}></span>
                                Captura de Club</div>
                            {props.touched.clubGender && props.errors.clubGender ? (
                                <div style={{ fontSize: '15px', fontWeight: '500px', color: 'red', position: 'fixed', marginLeft: '2.9vw', marginTop: '6.5vh', background: 'white', height: '4vh' }}>
                                    <p>{props.errors.clubGender}</p>
                                </div>
                            ) : null}
                            <ClubGender
                                id="clubGender"
                                name="clubGender"
                                value={props.values.clubGender}
                                handleChange={props.handleChange}
                                onChange={props.handleChange}
                                onBlur={props.handleBlur}
                                errors={props.errors}
                                touched={props.touched}
                                setClubGender={setClubGender}
                            />
                            <div className='container1XX' style={{ marginLeft: '-8.8vw', marginTop: '-0.2vh', marginRight: '5.9vw' }}>
                                {props.touched.name && props.errors.name ? (
                                    <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginBottom: '-3vh' }}>
                                        <p>{props.errors.name}</p>
                                    </div>
                                ) : null}
                                <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '16.5vw' }}>
                                    <span className="input-group-addon" style={{ color: '#9400D3', marginRight: '0.5vw', marginLeft: '0vw', border: 'black' }}><i className="fas fa-wrench"></i></span>
                                    <input
                                        id="name"
                                        type="text"
                                        className="form-control"
                                        name="name"
                                        placeholder="Nombre del Club"
                                        value={props.values.name}
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                    />
                                </div>
                                {props.touched.clubKey && props.errors.clubKey ? (
                                    <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '9.1vh' }}>
                                        <p>{props.errors.clubKey}</p>
                                    </div>
                                ) : null}
                                <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '16.5vw' }}>
                                    <span className="input-group-addon" style={{ color: '#9400D3', marginRight: '0.5vw', marginLeft: '0vw', border: 'black' }}><i className="fas fa-key"></i></span>
                                    <input
                                        id="clubKey"
                                        type="text"
                                        className="form-control"
                                        name="clubKey"
                                        placeholder="Clave del Club"
                                        value={props.values.clubKey}
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                    />
                                </div>
                                {props.touched.availability && props.errors.availability ? (
                                    <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '18.3vh' }}>
                                        <p>{props.errors.availability}</p>
                                    </div>
                                ) : null}
                                <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '16.5vw' }}>
                                    <span className="input-group-addon" style={{ color: '#9400D3', marginRight: '0.7vw', marginLeft: '0vw', border: 'black' }}><i className="fas fa-globe-americas"></i></span>
                                    <input
                                        id="availability"
                                        type="number"
                                        className="form-control"
                                        name="availability"
                                        placeholder="Cupo del Club"
                                        value={props.values.availability}
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                    />
                                </div>
                                {props.touched.scheduleStart && props.errors.scheduleStart ? (
                                    <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '27.5vh' }}>
                                        <p>{props.errors.scheduleStart}</p>
                                    </div>
                                ) : null}
                                <div className="input-group" style={{ display: 'flex', flexDirection: 'row', marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '18vw' }}>
                                    <span className="input-group-addon" style={{ color: '#9400D3', marginRight: '0.7vw', marginLeft: '0.2vw', border: 'black' }}><i className="fas fa-hourglass-start"></i></span>
                                    <ScheduleStart
                                        //style={{ width: '8vw' }}
                                        className="form-control"
                                        value={props.values.scheduleStart}
                                        handleChange={props.handleChange}
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        errors={props.errors}
                                        touched={props.touched}
                                        useField={useField}
                                        useFormikContext={useFormikContext}
                                        props={props}
                                    //formik={formik}

                                    />
                                    {/* <input
                                    id="scheduleStart"
                                    type="text"
                                    className="form-control"
                                    name="scheduleStart"
                                    placeholder="Hora de inicio"
                                    value={formik.values.scheduleStart}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                /> */}
                                </div>
                                {props.touched.scheduleEnd && props.errors.scheduleEnd ? (
                                    <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '36.7vh' }}>
                                        <p>{props.errors.scheduleEnd}</p>
                                    </div>
                                ) : null}
                                <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '18vw' }}>
                                    <span className="input-group-addon" style={{ color: '#9400D3', marginRight: '0.7vw', marginLeft: '0.2vw', border: 'black' }}><i className="fas fa-hourglass-end"></i></span>
                                    <ScheduleEnd
                                        className="form-control"
                                        value={props.values.scheduleStart}
                                        handleChange={props.handleChange}
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        errors={props.errors}
                                        touched={props.touched}
                                        useField={useField}
                                        useFormikContext={useFormikContext}
                                        props={props}
                                    />
                                    {/* <input
                                    id="scheduleEnd"
                                    type="text"
                                    className="form-control"
                                    name="scheduleEnd"
                                    placeholder="Hora Fin"
                                    value={formik.values.scheduleEnd}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                /> */}
                                </div>


                                <Button
                                    style={{ width: '10vw', height: '3.5vw', marginLeft: '7.9vw', marginTop: '3.35vh' }}
                                    type='submit'


                                //className={`${validarPedido()}`}
                                >
                                    Guardar
                            </Button>
                            </div>
                            <div className='container2XX' style={{ marginLeft: '-6vw', marginRight: '3vw', marginTop: '-0.2vh' }}>
                                {props.touched.day && props.errors.day ? (
                                    <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginBottom: '-3vh' }}>
                                        <p>{props.errors.day}</p>
                                    </div>
                                ) : null}
                                <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0vw', paddingRight: '0.6vw', marginTop: '2.9vh', width: '18vw' }}>
                                    <span className="input-group-addon" style={{ color: '#9400D3', marginRight: '0vw', marginLeft: '0vw', border: 'black' }}><i className="fas fa-sun"></i></span>
                                    <DaySelect
                                        id="day"
                                        name="day"
                                        value={props.values.day}
                                        handleChange={props.handleChange}
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        errors={props.errors}
                                        touched={props.touched}

                                    />
                                    {/* <input
                                    id="day"
                                    type="text"
                                    className="form-control"
                                    name="day"
                                    placeholder="Día del taller"
                                    value={formik.values.day}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                /> */}
                                </div>
                                {props.touched.teacher && props.errors.teacher ? (
                                    <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '9.1vh' }}>
                                        <p>{props.errors.teacher}</p>
                                    </div>
                                ) : null}
                                <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '2.9vh', width: '18vw' }}>
                                    <span className="input-group-addon" style={{ color: '#9400D3', marginRight: '0.2vw', marginLeft: '0vw', border: 'black' }}><i className="fas fa-user-graduate"></i></span>
                                    <TeacherSelect
                                        id="teacher"
                                        name="teacher"
                                        className="form-control"
                                        value={props.values.teacher}
                                        handleChange={props.handleChange}
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        dataR={dataR}
                                        errors={props.errors}
                                        touched={props.touched}
                                    />
                                    {/* <input
                                    id="teacher"
                                    type="text"
                                    className="form-control"
                                    name="teacher"
                                    placeholder="Maestro del taller"
                                    value={formik.values.teacher}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                /> */}
                                </div>


                                {/* <div className="input-group" style={{ paddingLeft: '0.3vw', paddingRight: '1vw' }}>
                                <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                                <input id="semester" type="text" className="form-control" name="semester" placeholder="Semestre" />
                            </div> */}


                                {props.touched.classroom && props.errors.classroom ? (
                                    <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '18.7vh' }}>
                                        <p>{props.errors.classroom}</p>
                                    </div>
                                ) : null}
                                <div className="input-group" style={{ marginLeft: '0.6vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '2.9vh', width: '18vw' }}>
                                    <span className="input-group-addon" style={{ color: '#9400D3', marginRight: '-0.1vw', marginLeft: '0vw', border: 'black' }}><i className="fas fa-house-user"></i></span>
                                    <ClassroomSelect
                                        id="classroom"
                                        name="classroom"
                                        className="form-control"
                                        value={props.values.classroom}
                                        handleChange={props.handleChange}
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        errors={props.errors}
                                        touched={props.touched}
                                    />
                                    {/* <input
                                    id="classroom"
                                    type="text"
                                    className="form-control"
                                    name="classroom"
                                    placeholder="Aula del taller"
                                    value={formik.values.classroom}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                /> */}
                                </div>
                                {props.touched.calendarStart && props.errors.calendarStart ? (
                                    <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '27.9vh' }}>
                                        <p>{props.errors.calendarStart}</p>
                                    </div>
                                ) : null}
                                <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '2.9vh', width: '18vw' }}>
                                    <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.7vw', marginLeft: '0.1vw', border: 'black' }}><i className="fas fa-calendar-alt"></i></span>
                                    <CalendarStart
                                        id="calendarStart"
                                        name="calendarStart"
                                        className="form-control"
                                        value={props.values.calendarStart}
                                        handleChange={props.handleChange}
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        errors={props.errors}
                                        touched={props.touched}
                                        useField={useField}
                                        useFormikContext={useFormikContext}
                                        props={props}
                                    />
                                    {/* <input
                                    id="calendarStart"
                                    type="text"
                                    className="form-control"
                                    name="calendarStart"
                                    placeholder="Inicio del taller"
                                    value={formik.values.calendarStart}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                /> */}
                                </div>
                                {props.touched.calendarEnd && props.errors.calendarEnd ? (
                                    <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '36.9vh' }}>
                                        <p>{props.errors.calendarEnd}</p>
                                    </div>
                                ) : null}
                                <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '2.9vh', width: '18vw' }}>
                                    <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.7vw', marginLeft: '0.1vw', border: 'black' }}><i className="fas fa-calendar-alt"></i></span>
                                    <CalendarEnd
                                        id="calendarEnd"
                                        name="calendarEnd"
                                        className="form-control"
                                        value={props.values.calendarEnd}
                                        handleChange={props.handleChange}
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        errors={props.errors}
                                        touched={props.touched}
                                        useField={useField}
                                        useFormikContext={useFormikContext}
                                        props={props}
                                    />
                                    {/* <input
                                    id="calendarEnd"
                                    type="text"
                                    className="form-control"
                                    name="calendarEnd"
                                    placeholder="Fin del taller"
                                    value={formik.values.calendarEnd}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                /> */}
                                </div>

                                <Link to='/dashboard'>
                                    <Button style={{ marginLeft: '0.7vw', paddingLeft: '0vw', paddingRight: '1vw', marginTop: '3.35vh', width: '10vw' }}>Regresar</Button>
                                </Link>
                            </div>


                        </div>

                        <Footer />


                    </form >
                );


            }}
        </Formik >
    );

};

export default FormAdminClub;