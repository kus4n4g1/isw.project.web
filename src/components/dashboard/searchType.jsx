import React, { useState } from 'react';
import Select from 'react-select';
import propTypes from 'prop-types';

const Search = (props) => {
    const {
        value,
        handleChange,
        onChange,
        onBlur,
        errors,
        touched,
        selectedSearchType,
        setSelectedSearchType,
    } = props;

    //const [selectedSearchType, setSelectedSearchType] = useState("");

    const handleSearchTypeChange = (selectedSearchType, value) => {
        value = selectedSearchType.value;
        console.log(selectedSearchType);
        setSelectedSearchType(selectedSearchType.value);
    };
    const options = [
        { value: 'Alumno', label: 'Alumno' },
        { value: 'Maestro', label: 'Maestro' },
        { value: 'Club', label: 'Club' },
        { value: 'Club Femenino', label: 'Club Femenino' },
        { value: 'Club Masculino', label: 'Club Masculino' },
        { value: 'Taller', label: 'Taller' },
    ];
    return (
        <Select
            id="searchType"
            name="searchType"
            options={options}
            className='selectStyle3XYZ'
            placeholder='¿Qué desea buscar?'
            errors={errors}
            touched={touched}
            onBlur={onBlur}
            onChange={selectedOption => {
                handleSearchTypeChange(selectedOption);
                console.log("values", value);
                console.log("selected search type", selectedOption.value);

                handleChange("searchType")(selectedOption.value);
            }}
            menuPlacement="bottom"
            menuPosition="fixed"
        />
    )
}

Search.propTypes = {
    value: propTypes.object,
    handleChange: propTypes.func,
    onChange: propTypes.func,
    onBlur: propTypes.func,
    errors: propTypes.object,
    touched: propTypes.object,
    selectedSearchType: propTypes.any,
    setSelectedSearchType: propTypes.func,
}

export default Search;