import React, { useState } from 'react';
import SideBar from '../sideBar';
import Header from '../headerBar';
import { Button } from 'reactstrap';
import SearchType from './searchType';
import { gql, useQuery, useLazyQuery, useMutation } from '@apollo/client';
import ReactTable from 'react-table';
import * as ReactBootstrap from 'react-bootstrap';
//import Taller from '../registerStudent/taller';
// import { Glyphicon } from 'react-bootstrap';
// import "react-table/react-table.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
//import AdminTeacher from '../adminTeacher';
import propTypes from 'prop-types';
import { Formik, useFormik, useField, useFormikContext } from 'formik';
import * as Yup from 'yup';
import ModalStudent from '../modal/modalStudent';
import ModalTaller from '../modal/modalTaller';
import ModalTeacher from '../modal/modalTeacher';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import Swal from 'sweetalert2';
import { useHistory } from "react-router-dom";
import { Link } from 'react-router-dom';
import ModalStudentMod from '../modal/modalStudentMod';

const BUSCAR_ESTUDIANTE = gql`
    query buscarEstudiante($texto:String!){
        buscarEstudiante(texto:$texto){
            id
            nombre
            paterno
            materno
            email
            nacimiento
            semestre
            genero
            curp
            grupo
        }
    }
`;
const BUSCAR_MAESTRO = gql`
    query buscarMaestro($texto:String!){
        buscarMaestro(texto:$texto){
        id
        nombre
        paterno
        materno
        email
        telefono
        celular
        curp
        sangre
        alergias
        padecimientos    
        }
    }
`;
const BUSCAR_TALLER = gql`
    query buscarTaller($texto:String!){
        buscarTaller(texto:$texto){
        id
        nombre
        clave
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino
        aula
        }
    }
    `;
const BUSCAR_CLUB = gql`
    query buscarClub($texto:String!){
        buscarClub(texto:$texto){
        id
        nombre
        clave
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino
        aula
        }
    }
`;
const BUSCAR_CLUB_FEMENINO = gql`
    query buscarClubFemenino($texto:String!){
        buscarClubFemenino(texto:$texto){
        id
        nombre
        clave
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino
        aula
        }
    }
`;
const BUSCAR_CLUB_MASCULINO = gql`
    query buscarClubMasculino($texto:String!){
        buscarClubMasculino(texto:$texto){
        id
        nombre
        clave
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino
        aula
        }
    }
`;
const OBTENER_ESTUDIANTE = gql`

query obtenerEstudiante($id:ID!){
    obtenerEstudiante(id:$id){
      id
      nombre
      paterno
      materno
      email
      nacimiento
      semestre
      genero
      curp
      sangre
      alergias
      padecimientos
      matricula
    }
  }
`;
const OBTENER_MAESTRO = gql`
    query obtenerMaestro($id:ID!){
        obtenerMaestro(id:$id){
        id
        nombre
        paterno
        materno
        email
        telefono
        celular
        curp
        sangre
        alergias
        padecimientos   
        }
    }
`;
const ELIMINAR_PEDIDO = gql`
    mutation eliminarPedido($id:ID!){
        eliminarPedido(id:$id)
    }
  `;

const ELIMINAR_TALLER = gql`
  mutation eliminarTaller($id:ID!){
      eliminarTaller(id:$id)
  }
`;
const OBTENER_PEDIDOS_TALLER = gql`
    query obtenerPedidosTaller($id:ID!) {
        obtenerPedidosTaller(id:$id){
        id
        pedido{
            id
            cantidad
        }
        estudiante{
            id
            nombre
            paterno
            materno
            email
            nacimiento
            semestre
            genero
            curp
            sangre
            alergias
            padecimientos
        }
        total
        }
    }
  `;

const Dashboard = () => {
    let history = useHistory();
    const [club, setClub] = useState([]);
    const [cargando, setCargando] = useState(true);
    const [taller, setTaller] = useState(null);
    const [modal, setModal] = useState(null);
    const [pedido, setPedido] = useState(null);
    const [studentX, setStudentX] = useState(null);
    const [activeStudent, setActiveStudent] = useState(null);
    const [selectedSearchType, setSelectedSearchType] = useState("");
    const [teacherX, setTeacherX] = useState(null);

    var p3d1d0 = null;
    //const [expanded, setExpanded] = useState();
    //const [defaultExpandedRows, setDefaultExpandedRow] = useState();
    var arrStudentsXY = [];

    const [eliminarTaller] = useMutation(ELIMINAR_PEDIDO);
    const [eliminarTallerO] = useMutation(ELIMINAR_TALLER);

    const [getTeacherX] = useLazyQuery(OBTENER_MAESTRO, {
        onCompleted: thisData => {

            p3d1d0 = thisData.obtenerMaestro;
            setTeacherX(p3d1d0);
            console.log('teacherx', teacherX)
            console.log('pedido', p3d1d0);
            setModal(true);
            setTeacherX(p3d1d0);
        }
    });
    const [getStudent, { data, error, loading }] = useLazyQuery(BUSCAR_ESTUDIANTE, {
        onCompleted: thisData => {
            console.log('thisdataaaz', thisData)


        }
        // skip: { id: null }
    });

    const [getTeacher, { data: dataZ, error: errorZ, loading: loadingZ }] = useLazyQuery(BUSCAR_MAESTRO, {
        onCompleted: thisData => {


        }
        // skip: { id: null }
    });

    const [getClub, { data: dataC, error: errorC, loading: loadingC }] = useLazyQuery(BUSCAR_CLUB, {
        variables: { id: taller },
        onCompleted: thisData => {



        }
        // skip: { id: null }
    });

    const [getClubF, { data: dataF, error: errorF, loading: loadingF }] = useLazyQuery(BUSCAR_CLUB_FEMENINO, {
        variables: { id: taller },
        onCompleted: thisData => {



        }
        // skip: { id: null }
    });

    const [getClubM, { data: dataM, error: errorM, loading: loadingM }] = useLazyQuery(BUSCAR_CLUB_MASCULINO, {
        variables: { id: taller },
        onCompleted: thisData => {



        }
        // skip: { id: null }
    });

    const [getTaller, { data: dataR, error: errorR, loading: loadingR }] = useLazyQuery(BUSCAR_TALLER, {
        variables: { id: taller },
        onCompleted: thisData => {



        }
        // skip: { id: null }
    });

    const [getStudentX] = useLazyQuery(OBTENER_ESTUDIANTE, {
        onCompleted: thisData => {

            p3d1d0 = thisData.obtenerEstudiante;
            setStudentX(p3d1d0);
            console.log('studentx', studentX)
            console.log('pedido', p3d1d0);
            setModal(true);
            setStudentX(p3d1d0);
        }
    });


    const schemaValidacion = Yup.object({
        searchType: Yup.string()
            .required('Tipo de búsqueda obligatorio*'),
        searchText: Yup.string()
            .required('Texto de busqueda obligatorio*')
            .min(3, 'Muy Corto!'),
    });

    const initialValues = {
        searchType: '',
        searchText: '',
    };
    const renderPlayer = (player, index) => {
        return (
            <tr key={player.id}  >
                <td> {player.nombre} </td>
                <td> {player.paterno} </td>
                <td> {player.materno} </td>
                <td> {player.curp} </td>
                <td> {player.email} </td>
                <td> {player.semestre} </td>
                <td style={{ display: 'flex' }} >
                    {/* <a href="#" class="btn btn-success btn-lg">
                        <span class="glyphicon glyphicon-remove"></span> Borrar
                    </a> */}

                    <a style={{ color: '#6A5ACD', marginRight: '0.5vw' }} onClick={() => getStudentX({ variables: { id: player.id } })}>
                        <span className="fas fa-eye"></span>
                        <ModalStudent
                            modal={modal}
                            setModal={setModal}
                            studentX={studentX}
                        />
                    </a>


                    <a style={{ color: '#9400D3', marginRight: '0.5vw' }} onClick={() => getStudentX({ variables: { id: player.id } })}>
                        <span className="fas fa-edit"></span>
                        <ModalStudentMod
                            modal={modal}
                            setModal={setModal}
                            studentX={studentX}
                        />
                    </a>
                    <a style={{ color: 'red', marginRight: '0vw', marginleft: '0.5vw' }} onClick={() => getStudent({ variables: { id: player.id } })


                    }>
                        <span className="fas fa-trash-alt"></span>

                    </a>



                    {/* <button type="button" class="buttonDelete">
                    </button> */}

                </td>
            </tr>
        )
    }

    const headerReady = () => {

        return (
            <tr>
                <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>
                    <span class="fas fa-user-graduate" style={{ marginRight: '0.3vw' }}></span>
                    TABLA DE ESTUDIANTES</th>
            </tr>
        );

    }
    // const expandRow = {
    //     renderer: row => (
    //         <div>
    //             <p>{`This Expand row is belong to rowKey ${row.id}`}</p>
    //             <p>You can render anything here, also you can add additional data on every row object</p>
    //             <p>expandRow.renderer callback will pass the origin row object to you</p>
    //         </div>
    //     )
    // };
    // const tablePDF = () => {
    //     const doc = new jsPDF();

    //     doc.autoTable({ tableReady });

    //     doc.save('tableReady.pdf');
    // }

    var columns = [
        { title: "Tipo", dataKey: "Tipo" },
        { title: "Nombre", dataKey: "Nombre" },
        { title: "Paterno", dataKey: "Paterno" },
        { title: "Materno", dataKey: "Materno" },
        { title: "Email", dataKey: "Email" },
        { title: "Semestre", dataKey: "Semestre" },
        { title: "Género", dataKey: "Género" },
        { title: "T.Sangre", dataKey: "Sangre" },
        { title: "Alergias", dataKey: "Alergias" },
        { title: "Padecimientos", dataKey: "Padecimientos" },
    ];

    const tablePDF = () => {
        var doc = new jsPDF('p', 'pt');
        doc.autoTable(columns, arrStudentsXY);
        doc.save('tableReady.pdf');
        // const doc = new jsPDF();

        // doc.autoTable({ tableReady });

        // doc.save('tableReady.pdf');
    }
    const tableReady = () => {

        return (
            <ReactBootstrap.Table striped bordered hover style={{ backgroundColor: 'white' }}
            // SubComponent={row => {
            //     return (
            //         <div style={{ 'border': '1px solid rgba(0, 0, 0, 0.05)' }}>
            //             {row.row.summary}
            //         </div>
            //     )
            // }}
            // onExpandedChange={newExpanded => onExpandedChange(newExpanded)}


            >
                <thead style={{ fontWeight: '500px', font: 'Roboto', color: 'white', backgroundImage: 'linear-gradient(to right, #BA55D3, #DDA0DD)' }}>
                    {headerReady()}
                    <tr>
                        <th>Nombre</th>
                        <th>Paterno</th>
                        <th>Materno</th>
                        <th>CURP</th>
                        <th>Email</th>
                        <th>Semestre</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    {data.buscarEstudiante.map(renderPlayer)}
                </tbody>
            </ReactBootstrap.Table>
        );
    }
    // if (thisData) {
    // return (
    // tableReady()
    // );

    // }

    //return <p>No se ha encontrado alumno con dicho apellido paterno</p>

    //Tabla Taller
    const renderPlayerT = (player, index) => {
        return (
            <tr key={player.id} >
                <td> {player.nombre} </td>
                <td> {player.aula} </td>
                <td> {player.existencia} </td>
                <td> {player.dia} </td>
                <td> {player.fechaInicio} </td>
                <td> {player.fechaTermino} </td>
                <td  >
                    {/* <a href="#" class="btn btn-success btn-lg">
                    <span class="glyphicon glyphicon-remove"></span> Borrar
                </a> */}

                    <a style={{ color: '#6A5ACD', marginRight: '0.5vw' }} onClick={() => getStudentX({ variables: { id: player.estudiante.id } })}>
                        <span className="fas fa-eye"></span>
                        <ModalStudent
                            modal={modal}
                            setModal={setModal}
                            studentX={studentX}
                        />
                    </a>


                    <a style={{ color: '#9400D3', marginRight: '0.5vw' }} href="#">
                        <span className="fas fa-edit"></span>
                    </a>
                    <a style={{ color: 'red', marginRight: '0vw' }} onClick={() =>
                        Swal.fire({
                            title: '¿Deseas eliminar del taller este estudiante?',
                            text: "Esta acción no podrá deshacerse.",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: 'red',
                            cancelButtonColor: '#9400D3',
                            confirmButtonText: 'Sí, Elimínalo',
                            cancelButtonText: 'No, Cancelar'
                        }).then(async (result) => {
                            if (result) {
                                try {
                                    console.log("justo antes de eliminar" + player.estudiante.id);
                                    p3d1d0 = player.id;
                                    console.log("3st3 3s p3d1d0", p3d1d0);
                                    const { data } = await eliminarTaller({
                                        variables: {
                                            id: player.id
                                        },
                                        refetchQueries: [
                                            {
                                                query: OBTENER_PEDIDOS_TALLER,
                                                variables: { id: player.id }
                                            }
                                        ]

                                    })
                                    renderPlayerT();
                                } catch (error) {
                                    p3d1d0 = player.id;
                                    console.log("3st3 3s p3d1d0", p3d1d0);
                                    console.log("justo antes de eliminar" + player.id);
                                    console.log(error);
                                }
                                if (result.value) {
                                    history.push('/dashboard/taller');
                                    Swal.fire(
                                        'Eliminado',
                                        "El estudiante se ha eliminado exitosamente",
                                        'success'
                                    )
                                }
                            }

                        })

                    }>
                        <span className="fas fa-trash-alt"></span>
                    </a>



                    {/* <button type="button" class="buttonDelete">
                </button> */}

                </td>
            </tr>
        )
    }

    const headerReadyT = () => {
        return (
            <tr style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>
                <th colSpan='7' style={{ fontSize: '22px', paddingBottom: '3vh' }}>Tabla de Talleres</th>
                {/* <th colSpan='1' style={{ paddingTop: '0', paddingBottom: '0' }}>
                    {/* <tr>
                    Maestro:

                </tr>
                <tr style={{ fontSize: '12px' }}>
                    {dataT.obtenerTaller.maestro.nombre} {dataT.obtenerTaller.maestro.paterno} {dataT.obtenerTaller.maestro.materno}
                </tr> 
                    {/* <tr style={{ fontSize: '12px' }}>
                        {dataR.buscarTaller.aula}
                    </tr> 

                </th>
                <th colSpan='1' style={{ paddingTop: '0', paddingBottom: '0' }}>
                    {/* <tr style={{marginBottom: '0.5vw'}}>
                    Opciones
                </tr> 
                    <tr>
                        <a style={{ color: '#9400D3', marginRight: '0.5vw' }} href="#">
                            <span className="fas fa-eye"></span>
                        </a>
                        <a style={{ color: '#9400D3', marginRight: '0.5vw' }} href="#">
                            <span className="fas fa-edit"></span>
                        </a>
                        <a style={{ color: 'red', marginRight: '0vw' }} onClick={() =>

                            Swal.fire({
                                title: '¿Deseas eliminar del taller este estudiante?',
                                text: "Esta acción no podrá deshacerse.",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: 'red',
                                cancelButtonColor: '#9400D3',
                                confirmButtonText: 'Sí, Elimínalo',
                                cancelButtonText: 'No, Cancelar'
                            }).then(async (result) => {
                                if (result) {
                                    try {
                                        //console.log("justo antes de eliminar" + dataT.obtenerTaller.id);
                                        p3d1d0 = dataR.buscarTaller.id;
                                        console.log("3st3 3s p3d1d0", p3d1d0);
                                        // const { data } = await eliminarTallerO({
                                        //     variables: {
                                        //         id: dataT.obtenerTaller.id
                                        //     },
                                        //     refetchQueries: [
                                        //         {
                                        //             query: OBTENER_TALLER,
                                        //             variables: { id: dataT.obtenerTaller.id }
                                        //         }
                                        //     ]

                                        // })
                                        renderPlayerT();
                                    } catch (error) {
                                        p3d1d0 = dataR.buscarTaller.id;
                                        console.log("3st3 3s p3d1d0", p3d1d0);
                                        console.log("justo antes de eliminar" + dataR.buscarTaller.id);
                                        console.log(error);
                                    }
                                    if (result.value) {
                                        history.push('/dashboard/taller');
                                        Swal.fire(
                                            'Eliminado',
                                            "El estudiante se ha eliminado exitosamente",
                                            'success'
                                        )
                                    }
                                }

                            })

                        }>
                            <span className="fas fa-trash-alt"></span>
                        </a>
                        {/* <a style={{ color: 'red', marginRight: '0vw' }} href="#"

                    // onClick={() =>

                    //     Swal.fire({
                    //         title: '¿Deseas eliminar este Taller?',
                    //         text: "Esta acción no podrá deshacerse.",
                    //         icon: 'warning',
                    //         showCancelButton: true,
                    //         confirmButtonColor: 'red',
                    //         cancelButtonColor: '#9400D3',
                    //         confirmButtonText: 'Sí, Elimínalo',
                    //         cancelButtonText: 'No, Cancelar'
                    //     }).then(async (result) => {
                    //         if (result) {
                    //             try {
                    //                 console.log("justo antes de eliminar" + taller);
                    //                 console.log("3st3 3s t4ll3r", taller);
                    //                 const { data } = await eliminarTaller({
                    //                     variables: {
                    //                         id: taller
                    //                     },
                    //                     refetchQueries: [
                    //                         {
                    //                             query: OBTENER_PEDIDOS_TALLER,
                    //                             variables: { taller }
                    //                         }
                    //                     ]

                    //                 })
                    //                 renderPlayer();
                    //             } catch (error) {
                    //                 console.log("3st3 3s t4ll3r", taller);
                    //                 console.log(error);
                    //             }
                    //             if (result.value) {
                    //                 Swal.fire(
                    //                     'Eliminado',
                    //                     "El Taller se ha eliminado exitosamente",
                    //                     'success'
                    //                 )
                    //             }
                    //         }

                    //     })


                    // }
                    >
                        <span className="fas fa-trash-alt"></span>
                    </a> 

                    </tr>
                </th> */}

            </tr>
        );
        // if (taller === '5ebfe0272b0a583094431c36') {
        //     return (
        //         <tr>
        //             <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE DESARROLLO DOCENTE</th>
        //         </tr>
        //     );
        // }
        // if (taller === '5ebfb65bc4e0b63dc0f52f37') {
        //     return (
        //         <tr>
        //             <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE MÚSICA</th>
        //         </tr>
        //     );
        // }
        // if (taller === '5ebfb615c4e0b63dc0f52f36') {
        //     return (
        //         <tr>
        //             <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE DANZA</th>
        //         </tr>
        //     );
        // }
        // if (taller === '5ebfb5dfc4e0b63dc0f52f35') {
        //     return (
        //         <tr>
        //             <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE APRECIACIÓN DE LAS ARTES</th>
        //         </tr>
        //     );
        // }

    }
    // const expandRow = {
    //     renderer: row => (
    //         <div>
    //             <p>{`This Expand row is belong to rowKey ${row.id}`}</p>
    //             <p>You can render anything here, also you can add additional data on every row object</p>
    //             <p>expandRow.renderer callback will pass the origin row object to you</p>
    //         </div>
    //     )
    // };
    // const tablePDF = () => {
    //     const doc = new jsPDF();

    //     doc.autoTable({ tableReady });

    //     doc.save('tableReady.pdf');
    // }

    // var resultXY = Object.values(dataR.buscarTaller);

    // var arrTallersXY = [];
    // for (var i = 0; i < resultXY.length; i++) {
    //     arrTallersXY.push(Object.values(resultXY[i]));
    // }

    var columns = [
        { title: "Tipo", dataKey: "Tipo" },
        { title: "Nombre", dataKey: "Nombre" },
        { title: "Clave", dataKey: "Clave" },
        { title: "Cupo", dataKey: "Cupo" },
        { title: "Día", dataKey: "Día" },
        { title: "Inicio", dataKey: "Inicio" },
        { title: "Término", dataKey: "Término" },
        // { title: "T.Sangre", dataKey: "Sangre" },
        // { title: "Alergias", dataKey: "Alergias" },
        // { title: "Padecimientos", dataKey: "Padecimientos" },
    ];

    // const tablePDFT = () => {
    //     var doc = new jsPDF('p', 'pt');
    //     doc.autoTable(columns, arrTallersXY);
    //     doc.save('tableReady.pdf');
    //     // const doc = new jsPDF();

    //     // doc.autoTable({ tableReady });

    //     // doc.save('tableReady.pdf');
    // }

    const tableReadyT = () => {
        // if (cargando === true || taller === null) {
        //     return (
        //         <div className='dashboardHeader' style={{ marginLeft: '0.5vw', marginTop: '0.7vh', borderRadius: '3px', width: '18.5vw', backgroundImage: 'linear-gradient(to right, #8A2BE2, #DA70D6)', opacity: '0.9', textAlign: 'center' }}>
        //             <p>No hay aún datos para mostrar</p>
        //         </div>
        //     );
        // }
        return (
            <ReactBootstrap.Table striped bordered hover style={{ backgroundColor: 'white', marginBottom: '0vh' }}
            // SubComponent={row => {
            //     return (
            //         <div style={{ 'border': '1px solid rgba(0, 0, 0, 0.05)' }}>
            //             {row.row.summary}
            //         </div>
            //     )
            // }}
            // onExpandedChange={newExpanded => onExpandedChange(newExpanded)}


            >
                <thead style={{ fontWeight: '500px', font: 'Roboto', color: 'white', backgroundImage: 'linear-gradient(to right, #BA55D3, #DDA0DD)' }}>
                    {headerReadyT()}
                    <tr>
                        <th>Nombre</th>
                        <th>Aula</th>
                        <th>Cupo</th>
                        <th>Día</th>
                        <th>Inicio</th>
                        <th>Término</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    {console.log('dataR que se va a mapear', dataR.buscarTaller)}
                    {dataR.buscarTaller.map(renderPlayerT)}
                </tbody>
            </ReactBootstrap.Table>
        );
    }

    //Tabla Club
    const renderPlayerC = (player, index) => {
        return (
            <tr key={player.id} >
                <td> {player.nombre} </td>
                <td> {player.aula} </td>
                <td> {player.existencia} </td>
                <td> {player.dia} </td>
                <td> {player.fechaInicio} </td>
                <td> {player.fechaTermino} </td>
                <td  >
                    {/* <a href="#" class="btn btn-success btn-lg">
                <span class="glyphicon glyphicon-remove"></span> Borrar
            </a> */}

                    <a style={{ color: '#6A5ACD', marginRight: '0.5vw' }} onClick={() => getStudentX({ variables: { id: player.estudiante.id } })}>
                        <span className="fas fa-eye"></span>
                        <ModalStudent
                            modal={modal}
                            setModal={setModal}
                            studentX={studentX}
                        />
                    </a>


                    <a style={{ color: '#9400D3', marginRight: '0.5vw' }} href="#">
                        <span className="fas fa-edit"></span>
                    </a>
                    <a style={{ color: 'red', marginRight: '0vw' }} onClick={() =>
                        Swal.fire({
                            title: '¿Deseas eliminar del taller este estudiante?',
                            text: "Esta acción no podrá deshacerse.",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: 'red',
                            cancelButtonColor: '#9400D3',
                            confirmButtonText: 'Sí, Elimínalo',
                            cancelButtonText: 'No, Cancelar'
                        }).then(async (result) => {
                            if (result) {
                                try {
                                    console.log("justo antes de eliminar" + player.estudiante.id);
                                    p3d1d0 = player.id;
                                    console.log("3st3 3s p3d1d0", p3d1d0);
                                    const { data } = await eliminarTaller({
                                        variables: {
                                            id: player.id
                                        },
                                        refetchQueries: [
                                            {
                                                query: OBTENER_PEDIDOS_TALLER,
                                                variables: { id: player.id }
                                            }
                                        ]

                                    })
                                    renderPlayerC();
                                } catch (error) {
                                    p3d1d0 = player.id;
                                    console.log("3st3 3s p3d1d0", p3d1d0);
                                    console.log("justo antes de eliminar" + player.id);
                                    console.log(error);
                                }
                                if (result.value) {
                                    history.push('/dashboard');
                                    Swal.fire(
                                        'Eliminado',
                                        "El estudiante se ha eliminado exitosamente",
                                        'success'
                                    )
                                }
                            }

                        })

                    }>
                        <span className="fas fa-trash-alt"></span>
                    </a>



                    {/* <button type="button" class="buttonDelete">
            </button> */}

                </td>
            </tr>
        )
    }

    const headerReadyC = () => {
        return (
            <tr style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>
                <th colSpan='7' style={{ fontSize: '22px', paddingBottom: '3vh' }}>Tabla de Clubes</th>
                {/* <th colSpan='1' style={{ paddingTop: '0', paddingBottom: '0' }}>
                    {/* <tr>
                Maestro:

            </tr>
            <tr style={{ fontSize: '12px' }}>
                {dataT.obtenerTaller.maestro.nombre} {dataT.obtenerTaller.maestro.paterno} {dataT.obtenerTaller.maestro.materno}
            </tr> 
                    {/* <tr style={{ fontSize: '12px' }}>
                    {dataR.buscarTaller.aula}
                </tr> 

                </th>
                <th colSpan='1' style={{ paddingTop: '0', paddingBottom: '0' }}>
                    {/* <tr style={{marginBottom: '0.5vw'}}>
                Opciones
            </tr> 
                    <tr>
                        <a style={{ color: '#9400D3', marginRight: '0.5vw' }} href="#">
                            <span className="fas fa-eye"></span>
                        </a>
                        <a style={{ color: '#9400D3', marginRight: '0.5vw' }} href="#">
                            <span className="fas fa-edit"></span>
                        </a>
                        <a style={{ color: 'red', marginRight: '0vw' }} onClick={() =>

                            Swal.fire({
                                title: '¿Deseas eliminar del taller este estudiante?',
                                text: "Esta acción no podrá deshacerse.",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: 'red',
                                cancelButtonColor: '#9400D3',
                                confirmButtonText: 'Sí, Elimínalo',
                                cancelButtonText: 'No, Cancelar'
                            }).then(async (result) => {
                                if (result) {
                                    try {
                                        //console.log("justo antes de eliminar" + dataT.obtenerTaller.id);
                                        p3d1d0 = dataC.buscarClub.id;
                                        console.log("3st3 3s p3d1d0", p3d1d0);
                                        // const { data } = await eliminarTallerO({
                                        //     variables: {
                                        //         id: dataT.obtenerTaller.id
                                        //     },
                                        //     refetchQueries: [
                                        //         {
                                        //             query: OBTENER_TALLER,
                                        //             variables: { id: dataT.obtenerTaller.id }
                                        //         }
                                        //     ]

                                        // })
                                        renderPlayerC();
                                    } catch (error) {
                                        p3d1d0 = dataC.buscarClub.id;
                                        console.log("3st3 3s p3d1d0", p3d1d0);
                                        console.log("justo antes de eliminar" + dataC.buscarClub.id);
                                        console.log(error);
                                    }
                                    if (result.value) {
                                        history.push('/dashboard/taller');
                                        Swal.fire(
                                            'Eliminado',
                                            "El estudiante se ha eliminado exitosamente",
                                            'success'
                                        )
                                    }
                                }

                            })

                        }>
                            <span className="fas fa-trash-alt"></span>
                        </a>
                        {/* <a style={{ color: 'red', marginRight: '0vw' }} href="#"

                // onClick={() =>

                //     Swal.fire({
                //         title: '¿Deseas eliminar este Taller?',
                //         text: "Esta acción no podrá deshacerse.",
                //         icon: 'warning',
                //         showCancelButton: true,
                //         confirmButtonColor: 'red',
                //         cancelButtonColor: '#9400D3',
                //         confirmButtonText: 'Sí, Elimínalo',
                //         cancelButtonText: 'No, Cancelar'
                //     }).then(async (result) => {
                //         if (result) {
                //             try {
                //                 console.log("justo antes de eliminar" + taller);
                //                 console.log("3st3 3s t4ll3r", taller);
                //                 const { data } = await eliminarTaller({
                //                     variables: {
                //                         id: taller
                //                     },
                //                     refetchQueries: [
                //                         {
                //                             query: OBTENER_PEDIDOS_TALLER,
                //                             variables: { taller }
                //                         }
                //                     ]

                //                 })
                //                 renderPlayer();
                //             } catch (error) {
                //                 console.log("3st3 3s t4ll3r", taller);
                //                 console.log(error);
                //             }
                //             if (result.value) {
                //                 Swal.fire(
                //                     'Eliminado',
                //                     "El Taller se ha eliminado exitosamente",
                //                     'success'
                //                 )
                //             }
                //         }

                //     })


                // }
                >
                    <span className="fas fa-trash-alt"></span>
                </a> 

                    </tr>
                </th> */}

            </tr>
        );
        // if (taller === '5ebfe0272b0a583094431c36') {
        //     return (
        //         <tr>
        //             <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE DESARROLLO DOCENTE</th>
        //         </tr>
        //     );
        // }
        // if (taller === '5ebfb65bc4e0b63dc0f52f37') {
        //     return (
        //         <tr>
        //             <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE MÚSICA</th>
        //         </tr>
        //     );
        // }
        // if (taller === '5ebfb615c4e0b63dc0f52f36') {
        //     return (
        //         <tr>
        //             <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE DANZA</th>
        //         </tr>
        //     );
        // }
        // if (taller === '5ebfb5dfc4e0b63dc0f52f35') {
        //     return (
        //         <tr>
        //             <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE APRECIACIÓN DE LAS ARTES</th>
        //         </tr>
        //     );
        // }

    }
    // const expandRow = {
    //     renderer: row => (
    //         <div>
    //             <p>{`This Expand row is belong to rowKey ${row.id}`}</p>
    //             <p>You can render anything here, also you can add additional data on every row object</p>
    //             <p>expandRow.renderer callback will pass the origin row object to you</p>
    //         </div>
    //     )
    // };
    // const tablePDF = () => {
    //     const doc = new jsPDF();

    //     doc.autoTable({ tableReady });

    //     doc.save('tableReady.pdf');
    // }

    // var resultXY = Object.values(dataR.buscarTaller);

    // var arrTallersXY = [];
    // for (var i = 0; i < resultXY.length; i++) {
    //     arrTallersXY.push(Object.values(resultXY[i]));
    // }

    var columns = [
        { title: "Tipo", dataKey: "Tipo" },
        { title: "Nombre", dataKey: "Nombre" },
        { title: "Clave", dataKey: "Clave" },
        { title: "Cupo", dataKey: "Cupo" },
        { title: "Día", dataKey: "Día" },
        { title: "Inicio", dataKey: "Inicio" },
        { title: "Término", dataKey: "Término" },
        // { title: "T.Sangre", dataKey: "Sangre" },
        // { title: "Alergias", dataKey: "Alergias" },
        // { title: "Padecimientos", dataKey: "Padecimientos" },
    ];

    // const tablePDFT = () => {
    //     var doc = new jsPDF('p', 'pt');
    //     doc.autoTable(columns, arrTallersXY);
    //     doc.save('tableReady.pdf');
    //     // const doc = new jsPDF();

    //     // doc.autoTable({ tableReady });

    //     // doc.save('tableReady.pdf');
    // }

    const tableReadyC = () => {
        // if (cargando === true || taller === null) {
        //     return (
        //         <div className='dashboardHeader' style={{ marginLeft: '0.5vw', marginTop: '0.7vh', borderRadius: '3px', width: '18.5vw', backgroundImage: 'linear-gradient(to right, #8A2BE2, #DA70D6)', opacity: '0.9', textAlign: 'center' }}>
        //             <p>No hay aún datos para mostrar</p>
        //         </div>
        //     );
        // }
        return (
            <ReactBootstrap.Table striped bordered hover style={{ backgroundColor: 'white', marginBottom: '0vh' }}
            // SubComponent={row => {
            //     return (
            //         <div style={{ 'border': '1px solid rgba(0, 0, 0, 0.05)' }}>
            //             {row.row.summary}
            //         </div>
            //     )
            // }}
            // onExpandedChange={newExpanded => onExpandedChange(newExpanded)}


            >
                <thead style={{ fontWeight: '500px', font: 'Roboto', color: 'white', backgroundImage: 'linear-gradient(to right, #BA55D3, #DDA0DD)' }}>
                    {headerReadyC()}
                    <tr>
                        <th>Nombre</th>
                        <th>Aula</th>
                        <th>Cupo</th>
                        <th>Día</th>
                        <th>Inicio</th>
                        <th>Término</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    {console.log('dataC que se va a mapear', dataC.buscarClub)}
                    {dataC.buscarClub.map(renderPlayerC)}
                </tbody>
            </ReactBootstrap.Table>
        );
    }
    //Tabla Club Masculino
    const renderPlayerM = (player, index) => {
        return (
            <tr key={player.id} >
                <td> {player.nombre} </td>
                <td> {player.aula} </td>
                <td> {player.existencia} </td>
                <td> {player.dia} </td>
                <td> {player.fechaInicio} </td>
                <td> {player.fechaTermino} </td>
                <td  >
                    {/* <a href="#" class="btn btn-success btn-lg">
            <span class="glyphicon glyphicon-remove"></span> Borrar
        </a> */}

                    <a style={{ color: '#6A5ACD', marginRight: '0.5vw' }} onClick={() => getStudentX({ variables: { id: player.estudiante.id } })}>
                        <span className="fas fa-eye"></span>
                        <ModalStudent
                            modal={modal}
                            setModal={setModal}
                            studentX={studentX}
                        />
                    </a>


                    <a style={{ color: '#9400D3', marginRight: '0.5vw' }} href="#">
                        <span className="fas fa-edit"></span>
                    </a>
                    <a style={{ color: 'red', marginRight: '0vw' }} onClick={() =>
                        Swal.fire({
                            title: '¿Deseas eliminar del taller este estudiante?',
                            text: "Esta acción no podrá deshacerse.",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: 'red',
                            cancelButtonColor: '#9400D3',
                            confirmButtonText: 'Sí, Elimínalo',
                            cancelButtonText: 'No, Cancelar'
                        }).then(async (result) => {
                            if (result) {
                                try {
                                    console.log("justo antes de eliminar" + player.estudiante.id);
                                    p3d1d0 = player.id;
                                    console.log("3st3 3s p3d1d0", p3d1d0);
                                    const { data } = await eliminarTaller({
                                        variables: {
                                            id: player.id
                                        },
                                        refetchQueries: [
                                            {
                                                query: OBTENER_PEDIDOS_TALLER,
                                                variables: { id: player.id }
                                            }
                                        ]

                                    })
                                    renderPlayerM();
                                } catch (error) {
                                    p3d1d0 = player.id;
                                    console.log("3st3 3s p3d1d0", p3d1d0);
                                    console.log("justo antes de eliminar" + player.id);
                                    console.log(error);
                                }
                                if (result.value) {
                                    history.push('/dashboard');
                                    Swal.fire(
                                        'Eliminado',
                                        "El estudiante se ha eliminado exitosamente",
                                        'success'
                                    )
                                }
                            }

                        })

                    }>
                        <span className="fas fa-trash-alt"></span>
                    </a>



                    {/* <button type="button" class="buttonDelete">
        </button> */}

                </td>
            </tr>
        )
    }

    const headerReadyM = () => {
        return (
            <tr style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>
                <th colSpan='7' style={{ fontSize: '22px', paddingBottom: '3vh' }}>Tabla de Clubes Varoniles</th>
                {/* <th colSpan='1' style={{ paddingTop: '0', paddingBottom: '0' }}>
                    {/* <tr>
            Maestro:

        </tr>
        <tr style={{ fontSize: '12px' }}>
            {dataT.obtenerTaller.maestro.nombre} {dataT.obtenerTaller.maestro.paterno} {dataT.obtenerTaller.maestro.materno}
        </tr> 
                    {/* <tr style={{ fontSize: '12px' }}>
                {dataR.buscarTaller.aula}
            </tr> 

                </th>
                <th colSpan='1' style={{ paddingTop: '0', paddingBottom: '0' }}>
                    {/* <tr style={{marginBottom: '0.5vw'}}>
            Opciones
        </tr> 
                    <tr>
                        <a style={{ color: '#9400D3', marginRight: '0.5vw' }} href="#">
                            <span className="fas fa-eye"></span>
                        </a>
                        <a style={{ color: '#9400D3', marginRight: '0.5vw' }} href="#">
                            <span className="fas fa-edit"></span>
                        </a>
                        <a style={{ color: 'red', marginRight: '0vw' }} onClick={() =>

                            Swal.fire({
                                title: '¿Deseas eliminar del taller este estudiante?',
                                text: "Esta acción no podrá deshacerse.",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: 'red',
                                cancelButtonColor: '#9400D3',
                                confirmButtonText: 'Sí, Elimínalo',
                                cancelButtonText: 'No, Cancelar'
                            }).then(async (result) => {
                                if (result) {
                                    try {
                                        //console.log("justo antes de eliminar" + dataT.obtenerTaller.id);
                                        p3d1d0 = dataC.buscarClub.id;
                                        console.log("3st3 3s p3d1d0", p3d1d0);
                                        // const { data } = await eliminarTallerO({
                                        //     variables: {
                                        //         id: dataT.obtenerTaller.id
                                        //     },
                                        //     refetchQueries: [
                                        //         {
                                        //             query: OBTENER_TALLER,
                                        //             variables: { id: dataT.obtenerTaller.id }
                                        //         }
                                        //     ]

                                        // })
                                        renderPlayerM();
                                    } catch (error) {
                                        p3d1d0 = dataM.buscarClubMasculino.id;
                                        console.log("3st3 3s p3d1d0", p3d1d0);
                                        console.log("justo antes de eliminar" + dataM.buscarClubMasculino.id);
                                        console.log(error);
                                    }
                                    if (result.value) {
                                        history.push('/dashboard');
                                        Swal.fire(
                                            'Eliminado',
                                            "El estudiante se ha eliminado exitosamente",
                                            'success'
                                        )
                                    }
                                }

                            })

                        }>
                            <span className="fas fa-trash-alt"></span>
                        </a>
                        {/* <a style={{ color: 'red', marginRight: '0vw' }} href="#"

            // onClick={() =>

            //     Swal.fire({
            //         title: '¿Deseas eliminar este Taller?',
            //         text: "Esta acción no podrá deshacerse.",
            //         icon: 'warning',
            //         showCancelButton: true,
            //         confirmButtonColor: 'red',
            //         cancelButtonColor: '#9400D3',
            //         confirmButtonText: 'Sí, Elimínalo',
            //         cancelButtonText: 'No, Cancelar'
            //     }).then(async (result) => {
            //         if (result) {
            //             try {
            //                 console.log("justo antes de eliminar" + taller);
            //                 console.log("3st3 3s t4ll3r", taller);
            //                 const { data } = await eliminarTaller({
            //                     variables: {
            //                         id: taller
            //                     },
            //                     refetchQueries: [
            //                         {
            //                             query: OBTENER_PEDIDOS_TALLER,
            //                             variables: { taller }
            //                         }
            //                     ]

            //                 })
            //                 renderPlayer();
            //             } catch (error) {
            //                 console.log("3st3 3s t4ll3r", taller);
            //                 console.log(error);
            //             }
            //             if (result.value) {
            //                 Swal.fire(
            //                     'Eliminado',
            //                     "El Taller se ha eliminado exitosamente",
            //                     'success'
            //                 )
            //             }
            //         }

            //     })


            // }
            >
                <span className="fas fa-trash-alt"></span>
            </a> 

                    </tr>
                </th> */}

            </tr>
        );
        // if (taller === '5ebfe0272b0a583094431c36') {
        //     return (
        //         <tr>
        //             <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE DESARROLLO DOCENTE</th>
        //         </tr>
        //     );
        // }
        // if (taller === '5ebfb65bc4e0b63dc0f52f37') {
        //     return (
        //         <tr>
        //             <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE MÚSICA</th>
        //         </tr>
        //     );
        // }
        // if (taller === '5ebfb615c4e0b63dc0f52f36') {
        //     return (
        //         <tr>
        //             <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE DANZA</th>
        //         </tr>
        //     );
        // }
        // if (taller === '5ebfb5dfc4e0b63dc0f52f35') {
        //     return (
        //         <tr>
        //             <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE APRECIACIÓN DE LAS ARTES</th>
        //         </tr>
        //     );
        // }

    }
    // const expandRow = {
    //     renderer: row => (
    //         <div>
    //             <p>{`This Expand row is belong to rowKey ${row.id}`}</p>
    //             <p>You can render anything here, also you can add additional data on every row object</p>
    //             <p>expandRow.renderer callback will pass the origin row object to you</p>
    //         </div>
    //     )
    // };
    // const tablePDF = () => {
    //     const doc = new jsPDF();

    //     doc.autoTable({ tableReady });

    //     doc.save('tableReady.pdf');
    // }

    // var resultXY = Object.values(dataR.buscarTaller);

    // var arrTallersXY = [];
    // for (var i = 0; i < resultXY.length; i++) {
    //     arrTallersXY.push(Object.values(resultXY[i]));
    // }

    var columns = [
        { title: "Tipo", dataKey: "Tipo" },
        { title: "Nombre", dataKey: "Nombre" },
        { title: "Clave", dataKey: "Clave" },
        { title: "Cupo", dataKey: "Cupo" },
        { title: "Día", dataKey: "Día" },
        { title: "Inicio", dataKey: "Inicio" },
        { title: "Término", dataKey: "Término" },
        // { title: "T.Sangre", dataKey: "Sangre" },
        // { title: "Alergias", dataKey: "Alergias" },
        // { title: "Padecimientos", dataKey: "Padecimientos" },
    ];

    // const tablePDFT = () => {
    //     var doc = new jsPDF('p', 'pt');
    //     doc.autoTable(columns, arrTallersXY);
    //     doc.save('tableReady.pdf');
    //     // const doc = new jsPDF();

    //     // doc.autoTable({ tableReady });

    //     // doc.save('tableReady.pdf');
    // }

    const tableReadyM = () => {
        // if (cargando === true || taller === null) {
        //     return (
        //         <div className='dashboardHeader' style={{ marginLeft: '0.5vw', marginTop: '0.7vh', borderRadius: '3px', width: '18.5vw', backgroundImage: 'linear-gradient(to right, #8A2BE2, #DA70D6)', opacity: '0.9', textAlign: 'center' }}>
        //             <p>No hay aún datos para mostrar</p>
        //         </div>
        //     );
        // }
        return (
            <ReactBootstrap.Table striped bordered hover style={{ backgroundColor: 'white', marginBottom: '0vh' }}
            // SubComponent={row => {
            //     return (
            //         <div style={{ 'border': '1px solid rgba(0, 0, 0, 0.05)' }}>
            //             {row.row.summary}
            //         </div>
            //     )
            // }}
            // onExpandedChange={newExpanded => onExpandedChange(newExpanded)}


            >
                <thead style={{ fontWeight: '500px', font: 'Roboto', color: 'white', backgroundImage: 'linear-gradient(to right, #BA55D3, #DDA0DD)' }}>
                    {headerReadyM()}
                    <tr>
                        <th>Nombre</th>
                        <th>Aula</th>
                        <th>Cupo</th>
                        <th>Día</th>
                        <th>Inicio</th>
                        <th>Término</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    {console.log('dataM que se va a mapear', dataM.buscarClubMasculino)}
                    {dataM.buscarClubMasculino.map(renderPlayerM)}
                </tbody>
            </ReactBootstrap.Table>
        );
    }
    //Tabla Club Femenino
    const renderPlayerF = (player, index) => {
        return (
            <tr key={player.id} >
                <td> {player.nombre} </td>
                <td> {player.aula} </td>
                <td> {player.existencia} </td>
                <td> {player.dia} </td>
                <td> {player.fechaInicio} </td>
                <td> {player.fechaTermino} </td>
                <td  >
                    {/* <a href="#" class="btn btn-success btn-lg">
            <span class="glyphicon glyphicon-remove"></span> Borrar
        </a> */}

                    <a style={{ color: '#6A5ACD', marginRight: '0.5vw' }} onClick={() => getStudentX({ variables: { id: player.estudiante.id } })}>
                        <span className="fas fa-eye"></span>
                        <ModalStudent
                            modal={modal}
                            setModal={setModal}
                            studentX={studentX}
                        />
                    </a>


                    <a style={{ color: '#9400D3', marginRight: '0.5vw' }} href="#">
                        <span className="fas fa-edit"></span>
                    </a>
                    <a style={{ color: 'red', marginRight: '0vw' }} onClick={() =>
                        Swal.fire({
                            title: '¿Deseas eliminar del taller este estudiante?',
                            text: "Esta acción no podrá deshacerse.",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: 'red',
                            cancelButtonColor: '#9400D3',
                            confirmButtonText: 'Sí, Elimínalo',
                            cancelButtonText: 'No, Cancelar'
                        }).then(async (result) => {
                            if (result) {
                                try {
                                    console.log("justo antes de eliminar" + player.estudiante.id);
                                    p3d1d0 = player.id;
                                    console.log("3st3 3s p3d1d0", p3d1d0);
                                    const { data } = await eliminarTaller({
                                        variables: {
                                            id: player.id
                                        },
                                        refetchQueries: [
                                            {
                                                query: OBTENER_PEDIDOS_TALLER,
                                                variables: { id: player.id }
                                            }
                                        ]

                                    })
                                    renderPlayerF();
                                } catch (error) {
                                    p3d1d0 = player.id;
                                    console.log("3st3 3s p3d1d0", p3d1d0);
                                    console.log("justo antes de eliminar" + player.id);
                                    console.log(error);
                                }
                                if (result.value) {
                                    history.push('/dashboard');
                                    Swal.fire(
                                        'Eliminado',
                                        "El estudiante se ha eliminado exitosamente",
                                        'success'
                                    )
                                }
                            }

                        })

                    }>
                        <span className="fas fa-trash-alt"></span>
                    </a>



                    {/* <button type="button" class="buttonDelete">
        </button> */}

                </td>
            </tr>
        )
    }

    const headerReadyF = () => {
        return (
            <tr style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>
                <th colSpan='7' style={{ fontSize: '22px', paddingBottom: '3vh' }}>Tabla de Clubes Femeniles</th>
                {/* <th colSpan='1' style={{ paddingTop: '0', paddingBottom: '0' }}> */}
                {/* <tr>
            Maestro:

        </tr>
        <tr style={{ fontSize: '12px' }}>
            {dataT.obtenerTaller.maestro.nombre} {dataT.obtenerTaller.maestro.paterno} {dataT.obtenerTaller.maestro.materno}
        </tr> */}
                {/* <tr style={{ fontSize: '12px' }}>
                {dataR.buscarTaller.aula}
            </tr> */}

                {/* </th> */}
                {/* <th colSpan='1' style={{ paddingTop: '0', paddingBottom: '0' }}>
                    {/* <tr style={{marginBottom: '0.5vw'}}>
            Opciones
        </tr> 
                    <tr>
                        <a style={{ color: '#9400D3', marginRight: '0.5vw' }} href="#">
                            <span className="fas fa-eye"></span>
                        </a>
                        <a style={{ color: '#9400D3', marginRight: '0.5vw' }} href="#">
                            <span className="fas fa-edit"></span>
                        </a>
                        <a style={{ color: 'red', marginRight: '0vw' }} onClick={() =>

                            Swal.fire({
                                title: '¿Deseas eliminar del taller este estudiante?',
                                text: "Esta acción no podrá deshacerse.",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: 'red',
                                cancelButtonColor: '#9400D3',
                                confirmButtonText: 'Sí, Elimínalo',
                                cancelButtonText: 'No, Cancelar'
                            }).then(async (result) => {
                                if (result) {
                                    try {
                                        //console.log("justo antes de eliminar" + dataT.obtenerTaller.id);
                                        p3d1d0 = dataF.buscarClubFemenino.id;
                                        console.log("3st3 3s p3d1d0", p3d1d0);
                                        // const { data } = await eliminarTallerO({
                                        //     variables: {
                                        //         id: dataT.obtenerTaller.id
                                        //     },
                                        //     refetchQueries: [
                                        //         {
                                        //             query: OBTENER_TALLER,
                                        //             variables: { id: dataT.obtenerTaller.id }
                                        //         }
                                        //     ]

                                        // })
                                        renderPlayerF();
                                    } catch (error) {
                                        p3d1d0 = dataF.buscarClubFemenino.id;
                                        console.log("3st3 3s p3d1d0", p3d1d0);
                                        console.log("justo antes de eliminar" + dataF.buscarClubFemenino.id);
                                        console.log(error);
                                    }
                                    if (result.value) {
                                        history.push('/dashboard');
                                        Swal.fire(
                                            'Eliminado',
                                            "El estudiante se ha eliminado exitosamente",
                                            'success'
                                        )
                                    }
                                }

                            })

                        }>
                            <span className="fas fa-trash-alt"></span>
                        </a>
                        {/* <a style={{ color: 'red', marginRight: '0vw' }} href="#"

            // onClick={() =>

            //     Swal.fire({
            //         title: '¿Deseas eliminar este Taller?',
            //         text: "Esta acción no podrá deshacerse.",
            //         icon: 'warning',
            //         showCancelButton: true,
            //         confirmButtonColor: 'red',
            //         cancelButtonColor: '#9400D3',
            //         confirmButtonText: 'Sí, Elimínalo',
            //         cancelButtonText: 'No, Cancelar'
            //     }).then(async (result) => {
            //         if (result) {
            //             try {
            //                 console.log("justo antes de eliminar" + taller);
            //                 console.log("3st3 3s t4ll3r", taller);
            //                 const { data } = await eliminarTaller({
            //                     variables: {
            //                         id: taller
            //                     },
            //                     refetchQueries: [
            //                         {
            //                             query: OBTENER_PEDIDOS_TALLER,
            //                             variables: { taller }
            //                         }
            //                     ]

            //                 })
            //                 renderPlayer();
            //             } catch (error) {
            //                 console.log("3st3 3s t4ll3r", taller);
            //                 console.log(error);
            //             }
            //             if (result.value) {
            //                 Swal.fire(
            //                     'Eliminado',
            //                     "El Taller se ha eliminado exitosamente",
            //                     'success'
            //                 )
            //             }
            //         }

            //     })


            // }
            >
                <span className="fas fa-trash-alt"></span>
            </a> 

                    </tr>
                </th> */}

            </tr>
        );
        // if (taller === '5ebfe0272b0a583094431c36') {
        //     return (
        //         <tr>
        //             <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE DESARROLLO DOCENTE</th>
        //         </tr>
        //     );
        // }
        // if (taller === '5ebfb65bc4e0b63dc0f52f37') {
        //     return (
        //         <tr>
        //             <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE MÚSICA</th>
        //         </tr>
        //     );
        // }
        // if (taller === '5ebfb615c4e0b63dc0f52f36') {
        //     return (
        //         <tr>
        //             <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE DANZA</th>
        //         </tr>
        //     );
        // }
        // if (taller === '5ebfb5dfc4e0b63dc0f52f35') {
        //     return (
        //         <tr>
        //             <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE APRECIACIÓN DE LAS ARTES</th>
        //         </tr>
        //     );
        // }

    }
    // const expandRow = {
    //     renderer: row => (
    //         <div>
    //             <p>{`This Expand row is belong to rowKey ${row.id}`}</p>
    //             <p>You can render anything here, also you can add additional data on every row object</p>
    //             <p>expandRow.renderer callback will pass the origin row object to you</p>
    //         </div>
    //     )
    // };
    // const tablePDF = () => {
    //     const doc = new jsPDF();

    //     doc.autoTable({ tableReady });

    //     doc.save('tableReady.pdf');
    // }

    // var resultXY = Object.values(dataR.buscarTaller);

    // var arrTallersXY = [];
    // for (var i = 0; i < resultXY.length; i++) {
    //     arrTallersXY.push(Object.values(resultXY[i]));
    // }

    var columns = [
        { title: "Tipo", dataKey: "Tipo" },
        { title: "Nombre", dataKey: "Nombre" },
        { title: "Clave", dataKey: "Clave" },
        { title: "Cupo", dataKey: "Cupo" },
        { title: "Día", dataKey: "Día" },
        { title: "Inicio", dataKey: "Inicio" },
        { title: "Término", dataKey: "Término" },
        // { title: "T.Sangre", dataKey: "Sangre" },
        // { title: "Alergias", dataKey: "Alergias" },
        // { title: "Padecimientos", dataKey: "Padecimientos" },
    ];

    // const tablePDFT = () => {
    //     var doc = new jsPDF('p', 'pt');
    //     doc.autoTable(columns, arrTallersXY);
    //     doc.save('tableReady.pdf');
    //     // const doc = new jsPDF();

    //     // doc.autoTable({ tableReady });

    //     // doc.save('tableReady.pdf');
    // }

    const tableReadyF = () => {
        // if (cargando === true || taller === null) {
        //     return (
        //         <div className='dashboardHeader' style={{ marginLeft: '0.5vw', marginTop: '0.7vh', borderRadius: '3px', width: '18.5vw', backgroundImage: 'linear-gradient(to right, #8A2BE2, #DA70D6)', opacity: '0.9', textAlign: 'center' }}>
        //             <p>No hay aún datos para mostrar</p>
        //         </div>
        //     );
        // }
        return (
            <ReactBootstrap.Table striped bordered hover style={{ backgroundColor: 'white', marginBottom: '0vh' }}
            // SubComponent={row => {
            //     return (
            //         <div style={{ 'border': '1px solid rgba(0, 0, 0, 0.05)' }}>
            //             {row.row.summary}
            //         </div>
            //     )
            // }}
            // onExpandedChange={newExpanded => onExpandedChange(newExpanded)}


            >
                <thead style={{ fontWeight: '500px', font: 'Roboto', color: 'white', backgroundImage: 'linear-gradient(to right, #BA55D3, #DDA0DD)' }}>
                    {headerReadyF()}
                    <tr>
                        <th>Nombre</th>
                        <th>Aula</th>
                        <th>Cupo</th>
                        <th>Día</th>
                        <th>Inicio</th>
                        <th>Término</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    {console.log('dataM que se va a mapear', dataF.buscarClubFemenino)}
                    {dataF.buscarClubFemenino.map(renderPlayerF)}
                </tbody>
            </ReactBootstrap.Table>
        );
    }
    //tabla maestro
    const renderPlayerZ = (player, index) => {
        return (
            <tr key={player.id} >
                <td> {player.nombre} </td>
                <td> {player.paterno} </td>
                <td> {player.materno} </td>
                <td> {player.curp} </td>
                <td> {player.email} </td>
                <td> {player.celular} </td>
                <td  >
                    {/* <a href="#" class="btn btn-success btn-lg">
                    <span class="glyphicon glyphicon-remove"></span> Borrar
                </a> */}

                    <a style={{ color: '#6A5ACD', marginRight: '0.5vw' }} onClick={() => getTeacherX({ variables: { id: player.id } })}>
                        <span className="fas fa-eye"></span>
                        <ModalTeacher
                            modal={modal}
                            setModal={setModal}
                            teacherX={teacherX}
                        />
                    </a>


                    <a style={{ color: '#9400D3', marginRight: '0.5vw' }} href="#">
                        <span className="fas fa-edit"></span>
                    </a>
                    <a style={{ color: 'red', marginRight: '0vw' }} onClick={() => getTeacher({ variables: { id: player.id } })


                    }>
                        <span className="fas fa-trash-alt"></span>
                    </a>



                    {/* <button type="button" class="buttonDelete">
                </button> */}

                </td>
            </tr>
        )
    }

    const headerReadyZ = () => {
        return (
            <tr>
                <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>
                    <span class="fas fa-graduation-cap" style={{ marginRight: '0.3vw' }}></span>
                TABLA DE MAESTROS
            </th>
            </tr>
        );


        // if (taller === '5ebfe0272b0a583094431c36') {
        //     return (
        //         <tr>
        //             <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE DESARROLLO DOCENTE</th>
        //         </tr>
        //     );
        // }
        // if (taller === '5ebfb65bc4e0b63dc0f52f37') {
        //     return (
        //         <tr>
        //             <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE MÚSICA</th>
        //         </tr>
        //     );
        // }
        // if (taller === '5ebfb615c4e0b63dc0f52f36') {
        //     return (
        //         <tr>
        //             <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE DANZA</th>
        //         </tr>
        //     );
        // }
        // if (taller === '5ebfb5dfc4e0b63dc0f52f35') {
        //     return (
        //         <tr>
        //             <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE APRECIACIÓN DE LAS ARTES</th>
        //         </tr>
        //     );
        // }

    }
    // const expandRow = {
    //     renderer: row => (
    //         <div>
    //             <p>{`This Expand row is belong to rowKey ${row.id}`}</p>
    //             <p>You can render anything here, also you can add additional data on every row object</p>
    //             <p>expandRow.renderer callback will pass the origin row object to you</p>
    //         </div>
    //     )
    // };

    var columns = [
        { title: "Tipo", dataKey: "Tipo" },
        { title: "Nombre", dataKey: "Nombre" },
        { title: "Paterno", dataKey: "Paterno" },
        { title: "Materno", dataKey: "Materno" },
        { title: "Email", dataKey: "Email" },
        { title: "Celular", dataKey: "Celular" },
        { title: "CURP", dataKey: "CURP" },
        // { title: "T.Sangre", dataKey: "Sangre" },
        // { title: "Alergias", dataKey: "Alergias" },
        // { title: "Padecimientos", dataKey: "Padecimientos" },
    ];

    // var res = [];

    // for (var i in resultX)
    //     res.push(resultX[i].nombre);


    // for (var i in resultX)
    //     res.slice(resultX[i].paterno);

    // var rows = res;
    // console.log('resultx', resultX)
    // console.log('rows', rows)
    // const tablePDF = () => {
    //     var doc = new jsPDF('p', 'pt');
    //     doc.autoTable(columns, arrTeachers7);
    //     doc.save('tableReady.pdf');
    //     // const doc = new jsPDF();

    //     // doc.autoTable({ tableReady });

    //     // doc.save('tableReady.pdf');
    // }


    const tableReadyZ = () => {

        return (
            <ReactBootstrap.Table striped bordered hover style={{ backgroundColor: 'white' }}
            // SubComponent={row => {
            //     return (
            //         <div style={{ 'border': '1px solid rgba(0, 0, 0, 0.05)' }}>
            //             {row.row.summary}
            //         </div>
            //     )
            // }}
            // onExpandedChange={newExpanded => onExpandedChange(newExpanded)}


            >
                <thead style={{ fontWeight: '500px', font: 'Roboto', color: 'white', backgroundImage: 'linear-gradient(to right, #BA55D3, #DDA0DD)' }}>
                    {headerReady()}
                    <tr>
                        <th>Nombre</th>
                        <th>Paterno</th>
                        <th>Materno</th>
                        <th>CURP</th>
                        <th>Email</th>
                        <th>Celular</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    {dataZ.buscarMaestro.map(renderPlayerZ)}
                </tbody>
            </ReactBootstrap.Table>
        );
    }
    return (
        <Formik
            validationSchema={schemaValidacion}
            enableReinitialize
            //initialvalues={ obtenerMaestros}
            initialValues={initialValues}
            onSubmit={async (valores, funciones) => {
                console.log('enviando');
                console.log('Entró la validación de formik!!!!!!!!');
                //console.log(valores);
                const { searchType, searchText } = valores
                try {
                    console.log('Que vale searchType: ', searchType)
                    //Crear nuevo estudiante
                    if (searchType == 'Alumno') {
                        getStudent({
                            variables: {
                                texto: searchText,
                            }
                        })
                    }
                    if (searchType == 'Maestro') {
                        getTeacher({
                            variables: {
                                texto: searchText,
                            }
                        })
                    }
                    if (searchType == 'Club') {
                        getClub({
                            variables: {
                                texto: searchText,
                            }
                        })
                    } if (searchType == 'Club Femenino') {
                        getClubF({
                            variables: {
                                texto: searchText,
                            }
                        })
                    } if (searchType == 'Club Masculino') {
                        getClubM({
                            variables: {
                                texto: searchText,
                            }
                        })
                    } if (searchType == 'Taller') {
                        getTaller({
                            variables: {
                                texto: searchText,
                            }
                        })
                    }

                    // if (data) {

                    //     return (
                    //         tableReady()
                    //     );
                    // }

                    // return <p>No se ha encontrado alumno con dicho apellido paterno</p>

                } catch (error) {
                    console.log(error);
                }



            }}




        >
            {props => {
                { console.log(props) }
                return (
                    <div className='main'>
                        <Header />
                        <SideBar />
                        <div className='content'>
                            <form style={{ display: 'flex', flexDirection: 'column' }} onSubmit={props.handleSubmit} onKeyPress={e => { if (e.key === 'Enter') { e.preventDefault(); document.getElementById("submitBtn").click() } }}>
                                <div className='dashboardHeader' style={{ paddingLeft: '0.55vw', marginBottom: '2.5vh', marginTop: '-1.3vh', marginLeft: '30.9vw', width: '12vw' }}>  " BIENVENIDO "</div>
                                {props.touched.searchType && props.errors.searchType ? (
                                    <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginBottom: '-3vh', marginTop: '5vh' }}>
                                        <p>{props.errors.searchType}</p>
                                    </div>
                                ) : null}
                                <div className="input-group" style={{ marginLeft: '0.5vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '19vw', display: 'flex', flexDirection: 'row' }}>
                                    <span className="input-group-addon" style={{ color: 'purple', marginRight: '0vw', marginLeft: '0vw', position: 'relative', marginTop: '0.2vh' }}><i className="fas fa-question-circle"></i></span>
                                    <SearchType
                                        className="form-control"
                                        value={props.values.searchType}
                                        handleChange={props.handleChange}
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        errors={props.errors}
                                        touched={props.touched}
                                        useField={useField}
                                        useFormikContext={useFormikContext}
                                        props={props}
                                        selectedSearchType={selectedSearchType}
                                        setSelectedSearchType={setSelectedSearchType}
                                    />
                                </div>
                                {props.touched.searchText && props.errors.searchText ? (
                                    <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginBottom: '-3vh', marginTop: '15vh' }}>
                                        <p>{props.errors.searchText}</p>
                                    </div>
                                ) : null}
                                <div className="input-group" style={{ marginLeft: '0.5vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw', borderRadius: '8px', display: 'flex', flexDirection: 'row' }}>
                                    <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-search"></i></span>
                                    <input
                                        id="searchText"
                                        type="text"
                                        className="form-control"
                                        name="searchText"
                                        value={props.values.searchText}
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        placeholder='Búsqueda'
                                        style={{ borderRadius: '3px', marginBottom: '3.35vh' }}
                                    />
                                    <button style={{ marginBottom: '3.35vh', color: '#9400D3', marginLeft: '0vw', backgroundColor: '#9400D3', width: '2.4vw !important', borderRadius: '3px' }} type='submit' id="submitBtn">
                                        <span className="fas fa-arrow-circle-right" style={{ color: 'white', marginLeft: '0.4vw', marginTop: '0.5vh' }}></span>
                                    </button>
                                    {/* <Button style={{ height: '6vh', marginTop: '-0vh', width: '0.1vw' }} /> */}
                                    {data && tableReady()}
                                    {dataC && tableReadyC()}
                                    {dataF && tableReadyF()}
                                    {dataM && tableReadyM()}
                                    {dataR && tableReadyT()}
                                    {dataZ && tableReadyZ()}

                                </div>

                            </form>

                        </div>
                        <div className='footer'>
                            <div>
                                <span class="fas fa-university" style={{ marginRight: '0.3vw' }}></span>
                    Copyright © 2020, B3st0 Team
                </div>
                        </div>
                    </div>
                );
            }}
        </Formik>
    );
}

export default Dashboard;