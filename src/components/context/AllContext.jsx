import React, { useState, createContext } from 'react';

export const AllContext = createContext();

export const AllProvider = (props) => {

    const [selectedDate, setSelectedDate] = useState(null);
    const [student, setStudent] = useState([{ name: 'sunny', bloodType: 'A+', email: 'sunny071282@hotmail.com' }]);
    const [teacher, setTeacher] = useState([{}]);
    const [club, setClub] = useState([{}]);
    const [support, setSupport] = useState([{}]);
    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState('');
    const [name, setName] = useState('');
    const [lastName, setLastName] = useState('');

    return (
        <AllContext.Provider
            // selectedDate={selectedDate}
            // setSelectedDate={setSelectedDate}
            // student={student}
            // setStudent={setStudent}
            // teacher={teacher}
            // setTeacher={setTeacher}
            // club={club}
            // setClub={setClub}
            // support={support}
            // setSupport={setSupport}
            value={{ selectedDate, setSelectedDate, student, setStudent, teacher, setTeacher, club, setClub, support, setSupport, email, setEmail, password, setPassword, name, setName, lastName, setLastName }}
        >
            {props.children}
        </AllContext.Provider>
    );
}