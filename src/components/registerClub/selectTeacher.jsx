import React from 'react';
import Select from 'react-select';

const SelectTeacher = () => {

    const options = [
        { value: 'Johnny Deep', label: 'Johnny Deep' },
        { value: 'Michael Jordan', label: 'Michael Jordan' },
        { value: 'Chris Garner', label: 'Chris Garner' },
        { value: 'Eminem', label: 'Eminem' },
        { value: 'Sandra Bullock', label: 'Sandra Bullock' },
        { value: 'Shigeru Miyamoto', label: 'Shigeru Miyamoto' },
        { value: 'Quentin Tarantino', label: 'Quentin Tarantino' },
        { value: 'Shakira', label: 'Shakira' }
    ];

    return (
        <>
            <Select
                options={options}
                placeholder={'Elija el Docente'}
                className='selectStyleClassroom'
                menuPlacement="bottom"
                menuPosition="fixed"
            />
        </>
    );
}

export default SelectTeacher;