import React from 'react';
import { Button } from 'reactstrap';
import Header from '../headerBar';
import SideBar from '../sideBar';
import Footer from '../footer';
import SelectTeacher from './selectTeacher';
import CalendarStart from '../calendar/calendarStart';
import CalendarEnd from '../calendar/calendarEnd';
import ScheduleStart from '../calendar/scheduleStart';
import ScheduleEnd from '../calendar/scheduleEnd';
import Classroom from './classroom';
import { Link } from 'react-router-dom';

const Form = () => {

    return (
        <form className="main2">
            <Header />
            <SideBar />
            <div className='content'>
                <div className='headersGrid' >Ingrese los siguientes datos</div>
                <div className='container'>
                    <div className="input-group" style={{ marginRight: '0.5vw', paddingRight: '0.6vw', width: '17.7vw', marginTop: '6.5vh' }}>
                        <span className="input-group-addon"  > <i className="glyphicon glyphicon-user"></i></span>
                        <input id="club" type="text" className="form-control" name="club" placeholder="Nombre del Taller/Club" />
                    </div>
                    <CalendarStart />
                    <ScheduleStart />
                    <SelectTeacher />
                    <Button style={{ width: '10vw', height: '3.5vw', marginLeft: '7vw' }}>Guardar</Button>
                </div>
                <div className='container2'>
                    <div className="input-group" style={{ marginLeft: '0.45vw', marginRight: '0.6vw', paddingRight: '0.6vw', width: '17.7vw', marginTop: '6.5vh' }}>
                        <span className="input-group-addon"  > <i className="glyphicon glyphicon-user"></i></span>
                        <input id="capacity" type="text" className="form-control" name="capacity" placeholder="Cantidad de alumnos" />
                    </div>
                    <CalendarEnd />
                    <ScheduleEnd />
                    <Classroom />
                    <Link to='/dashboard'>
                        <Button style={{ marginLeft: '0.5vw', width: '10vw', height: '3.5vw', marginRight: '5vw' }}>Regresar</Button>
                    </Link>
                </div>


            </div>
            <Footer />




        </form >
    );

};

export default Form;