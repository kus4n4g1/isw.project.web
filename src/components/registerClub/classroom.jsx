import React from 'react';
import Select from 'react-select';

const Classroom = () => {

    const options = [
        { value: '01', label: '01' },
        { value: '02', label: '02' },
        { value: '03', label: '03' },
        { value: '04', label: '04' },
        { value: '05', label: '05' },
        { value: '06', label: '06' },
        { value: '07', label: '07' },
        { value: '08', label: '08' },
    ];

    return (
        <Select
            placeholder='Registre el aula'
            options={options}
            className='selectStyleClassroom2'
            menuPlacement="bottom"
            menuPosition="fixed"
        />
    );

}

export default Classroom;