import React from 'react';
import Header from '../headerBar';
import Menu from '../sideBar';
import Footer from '../footer';
import ClubStart from './clubStart';
import ClubEnd from './clubEnd';
import SelectCourse from './selectCourse';

const Form = () => {

    return (
        <form className='main'>
            <Header />
            <Menu />
            <div className='content'>
                <div className='headersGrid2'>Calendario de Inicio y Término de Cursos</div>
                <div className='container5'>
                    <div className='calendarMainContainer'>
                        <SelectCourse />
                        <div className='calendarContainer'>
                            <ClubStart />
                            <ClubEnd />
                        </div>
                        <div className='calendarScheduleContainer'>
                            <div className="input-group" style={{ marginLeft: '0vw', marginRight: '0vw', paddingRight: '0.6vw', marginTop: '0.5vh', width: '26.3vw' }}>
                                <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                                <input id="studentName" type="text" className="form-control" name="startDate" placeholder="Hora de Inicio" />
                            </div>
                            <div className="input-group" style={{ marginRight: '0vw', paddingRight: '0.6vw', marginTop: '0.5vh', width: '26.3vw' }}>
                                <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                                <input id="studentName" type="text" className="form-control" name="startDate" placeholder="Hora de Término" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </form>
    );
}

export default Form;