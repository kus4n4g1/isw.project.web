import React from 'react';
import Select from 'react-select';

const SelectCourse = () => {

    const options = [
        { value: 'chess', label: 'Ajedrez' },
        { value: 'painting', label: 'Pintura' },
        { value: 'reading', label: 'Lectura' },
        { value: 'write', label: 'Redacción' }
    ];

    return (
        <Select
            options={options}
            className='selectStyle3'
            // style={{ width: '20vw !important', zIndex: '999' }}
            placeholder='Elija el curso'
            menuPlacement="bottom"
            menuPosition="fixed"
        />
    );

}

export default SelectCourse;