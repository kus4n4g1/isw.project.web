import React, { useState } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';

const ClubEnd = () => {

    const [date, setDate] = useState(new Date());

    return (
        <Calendar
            onChange={data => setDate(data)}
            value={date}
        />
    );
}

export default ClubEnd;