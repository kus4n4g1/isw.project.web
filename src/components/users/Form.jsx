import React, { useState, useContext } from 'react';
import { Button } from 'reactstrap';
import Header from '../headerBar';
import Footer from '../footer';
import { AllContext } from '../context/AllContext';
import { gql, useQuery, useMutation } from '@apollo/client';
import moment from 'moment';
import { Route, Redirect, useHistory } from "react-router-dom";
import Swal from 'sweetalert2';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';


const NUEVO_USUARIO = gql`
    mutation nuevoUsuario($input: UsuarioInput) {
        nuevoUsuario(input: $input){
        id
        nombre
        apellido
        email
        creado
        }
    }  
`


const Form = () => {
    let history = useHistory();
    const value = useContext(AllContext);
    //console.log(value.student);
    //console.log(value);
    // const [email, setEmail] = useState('');
    // const [password, setPassword] = useState('');

    //const [mensaje, setMensaje] = useState(null);
    const [cargando, setCargando] = useState(true);

    const [loadOpt, loadOptions] = useState(null);

    const [mensaje, guardarMensaje] = useState(null);

    //Mutation para autenticar nuevos usuarios en apollo
    const [nuevoUsuario] = useMutation(NUEVO_USUARIO);
    //validación de formulario
    const formik = useFormik({
        initialValues: {
            name: '',
            lastName: '',
            email: '',
            password: '',
            passwordConfirmation: '',

        },
        validationSchema: Yup.object({
            name: Yup.string()
                .required('Nombre es obligatorio'),
            lastName: Yup.string()
                .required('Paterno es obligatorio'),
            email: Yup.string()
                .email('Email no es válido')
                .required('El email es obligatorio'),
            password: Yup.string()
                .required('La contraseña es obligatoria'),
            passwordConfirmation: Yup.string()
                .required('Confirme la contraseña')
                .oneOf([Yup.ref('password'), null], 'Las contraseñas deben coincidir')
        }),
        onSubmit: async valores => {
            // console.log('enviando');
            // console.log(valores);            
            const { name, lastName, email, password } = valores;
            value.setName(name);
            value.setLastName(lastName);
            value.setEmail(email);
            try {
                const { data } = await nuevoUsuario({
                    variables: {
                        input: {
                            nombre: name,
                            apellido: lastName,
                            email,
                            password
                        }
                    }
                });

                //console.log(data);

                //Redireccionar hacia Estudiantes
                Swal.fire({
                    title: 'Correcto',
                    text: "El usuario se registró con éxito, por favor registre los datos del alumno",
                    icon: 'success',
                    //showCancelButton: true,
                    confirmButtonColor: '#9400D3',
                    //cancelButtonColor: '#3085d6',
                    confirmButtonText: 'Continuar',
                    //cancelButtonText: 'Seguir registrando'
                    //history.push('/dashboard/admin/student');

                }).then((result) => {
                    history.push('/dashboard/admin/student');
                });

            } catch (error) {
                console.log(error);
            }


        }
    });




    //const [nuevoPedido] = useMutation(NUEVO_PEDIDO);

    // console.log(data);
    // console.log(loading);
    // console.log(error);
    // console.log(dataR);
    // console.log(loadingR);
    // console.log(errorR);



    const mostrarMensaje = () => {
        return (
            <div className='headersGrid' >
                <p>{mensaje}</p>
            </div>
        );

    }
    // const submitHandler = (e) => {
    //     e.preventDefault();
    // }
    //console.log(formik);
    return (
        <form className="main2" onSubmit={formik.handleSubmit}>
            <Header />
            <div className='content' >
                <div className='headersGridS' >
                    <span class="fas fa-user" style={{ marginLeft: '-0.5vw', marginRight: '0.3vw' }}></span>
                    Crear Usuario
                </div>
                {mensaje && mostrarMensaje()}
                <div className='containerS'>
                    {formik.touched.name && formik.errors.name ? (
                        <div style={{ fontSize: '16px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '0.7vh' }}>
                            <p>{formik.errors.name}</p>
                        </div>
                    ) : null}
                    <div className="input-group" style={{ marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.5vh', width: '17.7vw', marginBottom: '1vw' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.5vw', border: 'black' }}><i className="fas fa-user-graduate"></i></span>
                        <input
                            id="name"
                            type="text"
                            className="form-control"
                            //name="name"
                            value={formik.values.name}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            onKeyPress={e => { if (e.key === 'Enter') e.preventDefault() }}
                            placeholder='Nombre' />
                    </div>
                    {formik.touched.lastName && formik.errors.lastName ? (
                        <div style={{ fontSize: '16px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '-2.65vh' }}>
                            <p>{formik.errors.lastName}</p>
                        </div>
                    ) : null}
                    {/* // border: '2px solid red', borderRadius: '4px'  */}
                    <div className="input-group" style={{ marginRight: '0.5vw', paddingRight: '0.6vw', width: '17.7vw', marginTop: '1.3vh', marginBottom: '1vw' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.5vw', }}> <i className="fas fa-pencil-alt"></i></span>
                        <input
                            id="lastName"
                            type="text"
                            className="form-control"
                            name="lastName"
                            value={formik.values.lastName}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            onKeyPress={e => { if (e.key === 'Enter') e.preventDefault() }}
                            placeholder={'Apellido Paterno'} />
                    </div >
                    {formik.touched.email && formik.errors.email ? (
                        <div style={{ fontSize: '16px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '-2.65vh' }}>
                            <p>{formik.errors.email}</p>
                        </div>
                    ) : null}
                    {/* color: '9400D3'  #8A2BE2 */}
                    <div className="input-group" style={{ marginRight: '0.5vw', paddingRight: '0.6vw', width: '17.7vw', marginTop: '1.3vh', marginBottom: '1vw' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.5vw', }}> <i className="fas fa-envelope"></i></span>
                        <input
                            id="email"
                            type="text"
                            className="form-control"
                            name="email"
                            value={formik.values.email}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            onKeyPress={e => { if (e.key === 'Enter') e.preventDefault() }}
                            placeholder={'email'} />
                    </div >
                    {formik.touched.password && formik.errors.password ? (
                        <div style={{ fontSize: '16px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '-2.65vh' }}>
                            <p>{formik.errors.password}</p>
                        </div>
                    ) : null}
                    <div className="input-group" style={{ marginBottom: '1vw', marginRight: '0.5vw', zIndex: '-1', paddingRight: '0.6vw', width: '17.7vw', marginTop: '1.2vh', transform: 'tranlateZ(-1)!important' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.5vw', border: 'black' }}><i className="fas fa-unlock"></i></span>
                        <input
                            id="password"
                            type="password"
                            className="form-control"
                            name="password"
                            value={formik.values.password}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            onKeyPress={e => { if (e.key === 'Enter') e.preventDefault() }}
                            placeholder={'Escriba la contraseña'} />
                    </div>
                    {formik.touched.passwordConfirmation && formik.errors.passwordConfirmation ? (
                        <div style={{ fontSize: '16px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '-2.65vh' }}>
                            <p>{formik.errors.passwordConfirmation}</p>
                        </div>
                    ) : null}
                    <div className="input-group" style={{ marginRight: '0.5vw', paddingRight: '0.6vw', width: '17.7vw', marginTop: '2.5vh', marginBottom: '0.7vw' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.5vw', border: 'black' }}><i className="fas fa-lock"></i></span>
                        <input
                            id="passwordConfirmation"
                            type="password"
                            className="form-control"
                            name="passwordConfirmation"
                            value={formik.values.passwordConfirmation}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            onKeyPress={e => { if (e.key === 'Enter') { e.preventDefault(); document.getElementById("submitBtn").click() } }}
                            placeholder={'Confirme la contraseña'} />
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <Button
                            style={{ width: '8vw', height: '3.5vw', marginLeft: '0.9vw', paddingLeft: '1vw', paddingRight: '0.9vw' }}
                            type='submit'
                            id='submitBtn'
                        // disabled={formik.errors.passwordConfirmation
                        //     || formik.errors.email
                        //     || formik.errors.lastName
                        //     || formik.errors.password
                        //     || formik.errors.name
                        // }                      
                        //className={`${validarPedido()}`}
                        >
                            Guardar
                        </Button>
                        <Link to='/dashboard'>
                            <Button
                                style={{ width: '8vw', height: '3.5vw', marginRight: '0vw', paddingLeft: '1vw', paddingRight: '0.9vw' }}
                            >
                                Regresar
                            </Button>
                        </Link>

                    </div>

                </div>


            </div>
            <Footer />
        </form >
    );

};

export default Form;
