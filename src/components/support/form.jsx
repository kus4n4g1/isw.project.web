import React from 'react';
import Header from '../headerBar';
import SideBar from '../sideBar';
import Footer from '../footer';
import Issue from './issue';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
const Form = () => {

    return (

        <div className='main'>
            <Header />
            <SideBar />
            <div className='content'>
                <div className='container4Main'>
                    <div className='container4'>
                        <div className='headersGrid'>
                            Ingrese los siguientes datos
                        </div>
                        <Issue />
                        <textarea className='textIssue' placeholder='Describa brevemente el problema' />
                    </div>
                    <div className='containerFlexRow' style={{ marginLeft: '1.1vw' }}>
                        <Button>Enviar</Button>
                        <Link to='/dashboard/'>
                            <Button>Regresar</Button>
                        </Link>

                    </div>


                </div>


            </div>
            <Footer />
        </div >
    );

}
export default Form; 