import React from 'react';
import Select from 'react-select';

const Issue = () => {
    const IssueType = [
        { value: 'registerIssue', label: 'No me deja ingresar datos' },
        { value: 'clubIssue', label: 'No aparecen talleres disponibles' },
        { value: 'displayIssue', label: 'No muestra la información' },
        { value: 'webPageIssue', label: 'No guarda la información' },
        { value: 'suggestIssue', label: 'Sugerencia / Otro problema' }
    ]
    return (
        <Select className='selectIssue'
            options={IssueType} placeholder='¿Qué problema sucede? '

        />
    );
}
export default Issue;