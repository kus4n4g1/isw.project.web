import React, { useState, useContext } from 'react';
import { Button } from 'reactstrap';
import Header from '../headerBar';
import SideBar from '../sideBar';
import Footer from '../footer';
import Taller from '../registerStudent/taller';
import { Link } from 'react-router-dom';
import { AllContext } from '../context/AllContext';
import { gql, useQuery, useMutation } from '@apollo/client';
import { Route, Redirect, useHistory } from "react-router-dom";
import Swal from 'sweetalert2';
import { useFormik } from 'formik';
import * as Yup from 'yup';

const NUEVO_MAESTRO = gql`
mutation nuevoMaestro($input:MaestroInput){
    nuevoMaestro(input:$input){
      id
      nombre
      paterno
      materno
      email
      telefono
      celular
      curp
      sangre
      padecimientos
      creado
      grupo
    }
  }
`

const Form = () => {
    let history = useHistory();
    const value = useContext(AllContext);

    const [name, setName] = useState('');
    const [lastName, setLastName] = useState('');
    const [lastName2, setLastName2] = useState('');
    const [curp, setCurp] = useState('');
    const [phone, setPhone] = useState('');
    const [allergies, setAllergies] = useState('');
    const [bloodType, setBloodType] = useState('');
    const [email, setEmail] = useState('');
    const [diseases, setDiseases] = useState('');
    const [cellPhone, setCellPhone] = useState('');
    const [club, setClub] = useState(null);
    const [mensaje, guardarMensaje] = useState(null);


    const [nuevoMaestro] = useMutation(NUEVO_MAESTRO);

    // const token = localStorage.getItem('token', token);
    // console.log(token);
    //Mutation para autenticar nuevos usuarios en apollo
    //validación de formulario
    const formik = useFormik({
        initialValues: {
            name: '',
            lastName: '',
            lastName2: '',
            email: '',
            phone: '',
            cellPhone: '',
            curp: '',
            bloodType: '',
            allergies: '',
            diseases: '',

        },
        validationSchema: Yup.object({
            name: Yup.string()
                .required('Nombre es obligatorio*'),
            lastName: Yup.string()
                .required('Apellido paterno es obligatorio*'),
            lastName2: Yup.string()
                .required('Apellido materno es obligatorio*'),
            email: Yup.string()
                .email('El email no es válido')
                .required('Email es obligatorio*'),
            // phone: Yup.string(),
            // //.test('len', 'Teléfono de 10 dígitos', val => val.length === 10),
            phone: Yup.string()
                .matches(/^(\d{10})$/, {
                    message: 'Teléfono en 10 dígitos',
                    excludeEmptyString: true
                }),
            cellPhone: Yup.string()
                .required('Celular es obligatorio*')
                .matches(/^(\d{10})$/, {
                    message: 'Celular en 10 dígitos',
                    excludeEmptyString: true
                }),
            //.test('len', 'Teléfono de 10 dígitos', val => val.length === 10),
            curp: Yup.string()
                .required('Curp es obligatorio*')
                .matches(/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/,
                    {
                        message: 'Verifique el CURP',
                        excludeEmptyString: true
                    }),
            //.test('len', 'Curp inválido', val => val.length === 18),
            bloodType: Yup.string()
                .required('Tipo de sangre es obligatorio*'),
            allergies: Yup.string()
                .required('Alergias es obligatorio*'),
            diseases: Yup.string()
                .required('Padecimientos es obligatorio*'),
        }),
        onSubmit: async valores => {
            console.log('enviando');
            console.log(valores);
            const { name, lastName, lastName2, email, phone, cellPhone, curp, bloodType, allergies, diseases } = valores
            try {
                const { data } = await nuevoMaestro({
                    variables: {
                        input: {
                            nombre: name,
                            paterno: lastName,
                            materno: lastName2,
                            email: email,
                            telefono: phone,
                            celular: cellPhone,
                            curp: curp,
                            sangre: bloodType,
                            alergias: allergies,
                            padecimientos: diseases
                        }
                    }
                })
                // //Redireccionar
                Swal.fire({
                    title: 'Correcto',
                    text: "El maestro se registró con éxito!",
                    icon: 'success',
                    showCancelButton: true,
                    confirmButtonColor: '#9400D3',
                    cancelButtonColor: '#3085d6',
                    confirmButtonText: 'Volver a Inicio',
                    cancelButtonText: 'Seguir registrando'
                }).then((result) => {
                    if (result.value) {
                        history.push('/dashboard');
                        Swal.fire(
                            'Correcto',
                            'El maestro se registró correctamente',
                            'success'
                        )
                    } else {
                        history.push('/dashboard/admin/teacher')
                    }
                })
                // history.push('/dashboard');
                // //Mensaje de éxito                                
                // Swal.fire(
                //     'Correcto',
                //     'El maestro se registró correctamente',
                //     'success'
                // )
            } catch (error) {
                console.log(error);
            }


        }
    });



    return (
        <form onSubmit={formik.handleSubmit} className="main2">
            <Header></Header>
            <SideBar />
            <div className='content'>
                <div className='headersGrid' >
                    <span class="fas fa-graduation-cap" style={{ marginLeft: '-0.5vw', marginRight: '0.3vw' }}></span>
                    Captura de Maestro</div>
                <div className='container1XX'>
                    {formik.touched.name && formik.errors.name ? (
                        <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginBottom: '-3vh' }}>
                            <p>{formik.errors.name}</p>
                        </div>
                    ) : null}
                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '18vw' }}>
                        <span className="input-group-addon" style={{ color: '#9400D3', marginRight: '0.2vw', marginLeft: '-0.2vw', border: 'black' }}><i className="fas fa-graduation-cap"></i></span>
                        <input
                            id="name"
                            type="text"
                            className="form-control"
                            name="name"
                            placeholder="Nombre"
                            value={formik.values.name}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                        />
                    </div>
                    {formik.touched.lastName && formik.errors.lastName ? (
                        <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '9.1vh' }}>
                            <p>{formik.errors.lastName}</p>
                        </div>
                    ) : null}
                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '18vw' }}>
                        <span className="input-group-addon" style={{ color: '#9400D3', marginRight: '0.5vw', marginLeft: '0vw', border: 'black' }}><i className="fas fa-parking"></i></span>
                        <input
                            id="lastName"
                            type="text"
                            className="form-control"
                            name="lastName"
                            placeholder="Apellido Paterno"
                            value={formik.valueslastName}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                        />
                    </div>
                    {formik.touched.lastName2 && formik.errors.lastName2 ? (
                        <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '18.3vh' }}>
                            <p>{formik.errors.lastName2}</p>
                        </div>
                    ) : null}
                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '18vw' }}>
                        <span className="input-group-addon" style={{ color: '#9400D3', marginRight: '0vw', marginLeft: '-0.2vw', border: 'black' }}><i className="fas fa-signature"></i></span>
                        <input
                            id="lastName2"
                            type="text"
                            className="form-control"
                            name="lastName2"
                            placeholder="Apellido Materno"
                            value={formik.values.lastName2}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                        />
                    </div>
                    {formik.touched.phone && formik.errors.phone ? (
                        <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '27.5vh' }}>
                            <p>{formik.errors.phone}</p>
                        </div>
                    ) : null}
                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '18vw' }}>
                        <span className="input-group-addon" style={{ color: '#9400D3', marginRight: '0.3vw', marginLeft: '0vw', border: 'black' }}><i className="fas fa-phone-alt"></i></span>
                        <input
                            id="phone"
                            type="text"
                            className="form-control"
                            name="phone"
                            placeholder="Teléfono de Casa"
                            value={formik.values.phone}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                        />
                    </div>
                    {formik.touched.cellPhone && formik.errors.cellPhone ? (
                        <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '36.7vh' }}>
                            <p>{formik.errors.cellPhone}</p>
                        </div>
                    ) : null}
                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '18vw' }}>
                        <span className="input-group-addon" style={{ color: '#9400D3', marginRight: '0.5vw', marginLeft: '0.3vw', border: 'black' }}><i className="fas fa-mobile-alt"></i></span>
                        <input
                            id="cellPhone"
                            type="text"
                            className="form-control"
                            name="cellPhone"
                            placeholder="Celular"
                            value={formik.values.cellPhone}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                        />
                    </div>


                    <Button
                        style={{ width: '10vw', height: '3.5vw', marginLeft: '7.9vw', marginTop: '3.35vh' }}
                        type='submit'


                    //className={`${validarPedido()}`}
                    >
                        Guardar
                    </Button>
                </div>
                <div className='container2XX'>
                    {formik.touched.email && formik.errors.email ? (
                        <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginBottom: '-3vh' }}>
                            <p>{formik.errors.email}</p>
                        </div>
                    ) : null}
                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '18vw' }}>
                        <span className="input-group-addon" style={{ color: '#9400D3', marginRight: '0.5vw', marginLeft: '0vw', border: 'black' }}><i className="fas fa-envelope"></i></span>
                        <input
                            id="email"
                            type="text"
                            className="form-control"
                            name="email"
                            placeholder="Email"
                            value={formik.values.email}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                        />
                    </div>
                    {formik.touched.curp && formik.errors.curp ? (
                        <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '9.1vh' }}>
                            <p>{formik.errors.curp}</p>
                        </div>
                    ) : null}
                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '18vw' }}>
                        <span className="input-group-addon" style={{ color: '#9400D3', marginRight: '0.5vw', marginLeft: '0vw', border: 'black' }}><i className="fas fa-atlas"></i></span>
                        <input
                            id="curp"
                            type="text"
                            className="form-control"
                            name="curp"
                            placeholder="CURP"
                            value={formik.values.curp}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                        />
                    </div>


                    {/* <div className="input-group" style={{ paddingLeft: '0.3vw', paddingRight: '1vw' }}>
                        <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                        <input id="semester" type="text" className="form-control" name="semester" placeholder="Semestre" />
                    </div> */}


                    {formik.touched.bloodType && formik.errors.bloodType ? (
                        <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '18.3vh' }}>
                            <p>{formik.errors.bloodType}</p>
                        </div>
                    ) : null}
                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '18vw' }}>
                        <span className="input-group-addon" style={{ color: '#9400D3', marginRight: '0.5vw', marginLeft: '0vw', border: 'black' }}><i className="fas fa-syringe"></i></span>
                        <input
                            id="bloodType"
                            type="text"
                            className="form-control"
                            name="bloodType"
                            value={formik.values.bloodType}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            placeholder={'Tipo de Sangre'} />
                    </div>
                    {formik.touched.allergies && formik.errors.allergies ? (
                        <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '27.5vh' }}>
                            <p>{formik.errors.allergies}</p>
                        </div>
                    ) : null}
                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '18vw' }}>
                        <span className="input-group-addon" style={{ color: '#9400D3', marginRight: '0.5vw', marginLeft: '0vw', border: 'black' }}><i className="fas fa-allergies"></i></span>
                        <input
                            id="allergies"
                            type="text"
                            className="form-control"
                            name="allergies"
                            value={formik.values.allergies}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            placeholder={'Alergias'} />
                    </div>
                    {formik.touched.diseases && formik.errors.diseases ? (
                        <div style={{ fontSize: '15px', fontWeight: '300px', color: 'red', position: 'fixed', marginLeft: '2.7vw', marginTop: '36.7vh' }}>
                            <p>{formik.errors.diseases}</p>
                        </div>
                    ) : null}
                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '18vw' }}>
                        <span className="input-group-addon" style={{ color: '#9400D3', marginRight: '0.5vw', marginLeft: '0vw', border: 'black' }}><i className="fas fa-disease"></i></span>
                        <input
                            id="diseases"
                            type="text"
                            className="form-control"
                            name="diseases"
                            value={formik.values.diseases}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            placeholder={'Padecimientos'} />
                    </div>

                    <Link to='/dashboard'>
                        <Button style={{ marginLeft: '0.7vw', paddingLeft: '0vw', paddingRight: '1vw', marginTop: '3.35vh', width: '10vw', marginRight: '1vw', marginBottom: '0vw' }}>Regresar</Button>
                    </Link>
                </div>


            </div>

            <Footer />


        </form >
    );

};

export default Form;