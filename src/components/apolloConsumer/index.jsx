import React from 'react';
import { ApolloConsumer } from 'react-apollo';
import { useHistory } from "react-router-dom";



export default () => {
    let history = useHistory();
    <ApolloConsumer>
        {client => {
            client.resetStore();
            localStorage.setItem("token", "");
            return history.push('/');
        }}
    </ApolloConsumer>
}