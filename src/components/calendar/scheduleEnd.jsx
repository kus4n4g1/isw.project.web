import React, { useState } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css'
import propTypes from 'prop-types';

const ScheduleEnd = (propsS) => {
    const {
        value,
        handleChange,
        onChange,
        onBlur,
        errors,
        touched,
        props
    } = propsS;

    const [selectedTime, setSelectedTime] = useState(null);

    const handleTimeChange = (selectedTime, value) => {
        // console.log('Esto es selectedTime' + selectedTime);
        props.setFieldValue('scheduleEnd', selectedTime);
        setSelectedTime(selectedTime);
    };
    console.log(props);

    return (

        <DatePicker
            id="scheduleEnd"
            name="scheduleEnd"
            className='timeStyle'
            selected={props.values.scheduleEnd}
            errors={errors}
            touched={touched}
            onBlur={onBlur}
            onChange={selectedOption => {
                handleTimeChange(selectedOption);
                console.log("values", value);
            }}
            showTimeSelect
            showTimeSelectOnly
            timeIntervals={15}
            timeCaption="Time"
            dateFormat="h:mm aa"
            placeholderText="Hora de Término"
        />

    );

};
ScheduleEnd.propTypes = {
    value: propTypes.object,
    handleChange: propTypes.func,
    onChange: propTypes.func,
    onBlur: propTypes.func,
    errors: propTypes.object,
    touched: propTypes.object,
    useField: propTypes.func,
    useFormikContext: propTypes.func,
    props: propTypes.object,
};

export default ScheduleEnd;