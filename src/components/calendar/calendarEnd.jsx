import React, { useState } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css'
import propTypes from 'prop-types';

const CalendarEnd = (propsS) => {
    const {
        value,
        handleChange,
        onChange,
        onBlur,
        errors,
        touched,
        props
    } = propsS;

    const [selectedTime, setSelectedTime] = useState(null);

    const handleTimeChange = (selectedTime, value) => {
        // console.log('Esto es selectedTime' + selectedTime);
        props.setFieldValue('calendarEnd', selectedTime);
        setSelectedTime(selectedTime);
    };
    console.log(props);

    return (

        <DatePicker
            id="calendarEnd"
            name="calendarEnd"
            className='timeStyle'
            style={{ fontSize: '16px', marginRight: '3vw' }}
            selected={props.values.calendarEnd}
            errors={errors}
            touched={touched}
            onBlur={onBlur}
            onChange={selectedOption => {
                handleTimeChange(selectedOption);
                console.log("values", value);
            }}
            dateFormat='dd/MM/yyyy'
            minDate={new Date()}
            placeholderText='Fecha de Término'
            showYearDropdown
            scrollableYearDropdown
            dropdownMode="scroll"
        />

    );

}
CalendarEnd.propTypes = {
    value: propTypes.object,
    handleChange: propTypes.func,
    onChange: propTypes.func,
    onBlur: propTypes.func,
    errors: propTypes.object,
    touched: propTypes.object,
    useField: propTypes.func,
    useFormikContext: propTypes.func,
    props: propTypes.object,
};

export default CalendarEnd;