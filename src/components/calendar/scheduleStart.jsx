import React, { useState } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css'
import propTypes from 'prop-types';
import { Typography, Container, Button, Box } from "@material-ui/core";
import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker,
    KeyboardDatePicker
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";

const ScheduleStart = (propsS) => {
    const {
        value,
        handleChange,
        onChange,
        onBlur,
        errors,
        touched,
        useField,
        useFormikContext,
        props
    } = propsS;
    //const { setFieldValue } = useFormikContext();
    const [field] = useField(props);

    const [selectedTime, setSelectedTime] = useState(null);
    console.log('Este es props' + props);
    // console.log(value);
    // console.log(value.scheduleStart);
    // console.log(props);
    // console.log(props.values);
    // console.log('Esto es lo que valdrá scheduleStart' + props.values.scheduleStart);
    // const handleChange = (selectedOption) => {
    //console.log('It will never happen');
    // console.log(onChange);
    //console.log(onBlur);
    // console.log(formik);
    // console.log(formik.values.scheduleStart);
    // };
    const handleTimeChange = (selectedTime, value) => {
        //value = selectedTime.value;
        // console.log('Esto es selectedTime' + selectedTime);
        props.setFieldValue('scheduleStart', selectedTime);
        setSelectedTime(selectedTime);
    };
    //console.log(field.scheduleStart);
    return (

        <DatePicker
            id="scheduleStart"
            name="scheduleStart"
            className='timeStyle'
            selected={props.values.scheduleStart}
            errors={errors}
            touched={touched}
            onBlur={onBlur}
            onChange={selectedOption => {
                handleTimeChange(selectedOption);
                console.log("values", value);
                // handleChange("scheduleStart")(selectedOption);;
            }}
            showTimeSelect
            showTimeSelectOnly
            timeIntervals={15}
            timeCaption="Time"
            dateFormat="h:mm aa"
            placeholderText="Hora de Inicio"
        //onChange={date => setFieldValue('scheduleStart', date)}
        //selected={selectedTime}

        //onChange={date => setSelectedTime(date)}
        // selected={formik.values.scheduleStart}
        // onChange={date => formik.setFieldValue}
        //onBlur={formik.handleBlur}    
        />
        // <MuiPickersUtilsProvider utils={DateFnsUtils} style={{ width: '10vw' }}>
        //     <KeyboardDatePicker
        //         className='timeStyle'
        //         id="date-picker-dialog"
        //         label="Hora de inicio"
        //         inputVariant="outlined"
        //         format="dd/MM/yyyy"

        //         value={value.scheduleStart}
        //         onChange={value => props.setFieldValue("scheduleStart", value)}
        //         KeyboardButtonProps={{
        //             "aria-label": "change date"
        //         }}
        //     />
        // </MuiPickersUtilsProvider>
    );

}
ScheduleStart.propTypes = {
    value: propTypes.object,
    handleChange: propTypes.func,
    onChange: propTypes.func,
    onBlur: propTypes.func,
    errors: propTypes.object,
    touched: propTypes.object,
    useField: propTypes.func,
    useFormikContext: propTypes.func,
    props: propTypes.object,
};

export default ScheduleStart;