import React, { useState } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css'
import propTypes from 'prop-types';

const CalendarStart = (propsS) => {
    const {
        value,
        handleChange,
        onChange,
        onBlur,
        errors,
        touched,
        props
    } = propsS;
    const [selectedTime, setSelectedTime] = useState(null);

    const handleTimeChange = (selectedTime, value) => {
        // console.log('Esto es selectedTime' + selectedTime);
        props.setFieldValue('calendarStart', selectedTime);
        setSelectedTime(selectedTime);
    };
    console.log(props);

    // const handleChange = (selectedOption) => {
    //     console.log('It will never happen');
    // };


    return (

        <DatePicker
            id="calendarStart"
            name="calendarStart"
            className='timeStyle'
            style={{ fontSize: '16px', marginRight: '3vw' }}

            dateFormat='dd/MM/yyyy'
            minDate={new Date()}
            //onChange={date => setSelectedDate(date)}
            selected={props.values.calendarStart}
            errors={errors}
            touched={touched}
            onBlur={onBlur}
            onChange={selectedOption => {
                handleTimeChange(selectedOption);
                console.log("values", value);
            }}
            placeholderText='Fecha de Inicio'
            showYearDropdown
            scrollableYearDropdown
        />

    );

}
CalendarStart.propTypes = {
    value: propTypes.object,
    handleChange: propTypes.func,
    onChange: propTypes.func,
    onBlur: propTypes.func,
    errors: propTypes.object,
    touched: propTypes.object,
    useField: propTypes.func,
    useFormikContext: propTypes.func,
    props: propTypes.object,
};

export default CalendarStart;