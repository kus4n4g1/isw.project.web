import React from 'react';
import Select from 'react-select';
import propTypes from 'prop-types';

const Gender = (props) => {

    const {
        setGender
    } = props;

    const options = [
        { value: 'male', label: 'Masculino' },
        { value: 'female', label: 'Femenino' },
    ];

    return (
        <Select
            placeholder={'Seleccione el género '}
            className='selectStyle'
            options={options}
            style={{ fontSize: '16px' }}
            menuPlacement="auto"
            menuPosition="fixed"
            onChange={e => setGender(e.value)}

        // style={{ width: '18.2vw' }}
        />
    );
}

Gender.propTypes = {
    setGender: propTypes.func
};

export default Gender;