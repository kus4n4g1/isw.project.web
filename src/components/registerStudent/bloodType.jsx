import React from 'react';
import Select from 'react-select';
import propTypes from 'prop-types';

const BloodType = (props) => {
    const {
        setBloodType
    } = props;

    const options = [
        { value: 'o+', label: 'O+' },
        { value: 'o-', label: 'O-' },
        { value: 'a+', label: 'A+' },
        { value: 'a-', label: 'A-' },
        { value: 'b+', label: 'B+' },
        { value: 'b-', label: 'B-' },
        { value: 'ab+', label: 'AB+' },
        { value: 'ab-', label: 'AB-' }
    ];
    return (
        <Select
            options={options}
            className='selectStyle2'
            placeholder='Tipo de Sangre'
            onChange={e => setBloodType(e.value)}
            menuPlacement="bottom"
            menuPosition="fixed"
        />
    )
}

BloodType.propTypes = {
    setBloodType: propTypes.func
}

export default BloodType;