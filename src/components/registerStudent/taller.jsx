import React from 'react';
import Select from 'react-select';
import propTypes from 'prop-types';
import { gql, useQuery } from '@apollo/client';
import AsyncSelect from 'react-select';
//import { colors } from 'react-select/src/theme';


const OBTENER_TALLERES = gql`
    query obtenerTalleres {
        obtenerTalleres {
        id
        nombre
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino
        maestro
        aula
        creado    
        }
    }

`;

const Taller = (props) => {
    const {
        setClub,
        data,
        dataR,
        dataC,
        dataF,
        dataM,
        setCargando,
        setTaller,
        loadHeader,
        loadHeader2,
        loadHeader3,
        loadOptions,
        genero,
        genero2,
        esTaller,
        esClub,
        placeToHold
    } = props;

    //console.log(dataR);
    // console.log(setClub);

    const handleChange = (e) => {
        setClub(e.value);
        console.log("This is it man" + e.value);
        setTaller(e.value);
        loadHeader({ variables: { id: e.value } });
        loadHeader2({ variables: { id: e.value } });
        loadHeader3({ variables: { id: e.value } });
        loadOptions({ variables: { id: e.value } });
        setCargando(false);
    }

    const options = [
        { value: 'chess', label: 'Ajedrez' },
        { value: 'painting', label: 'Pintura' },
        { value: 'reading', label: 'Lectura' },
        { value: 'write', label: 'Redacción' }
    ];

    //if (loading) return 'Cargando...';

    // const content = dataR.obtenerTalleres.map((taller) =>
    //     <div key={taller.id}>
    //         <h3 value={taller.nombre}>{taller.nombre}</h3>
    //     </div>
    // );

    // {
    //     DataR.keys(obtenerTalleres).map((nombre, i) => (
    //         <li className="travelcompany-input" key={i}>
    //             <span className="input-label">key: {i} Name: {obtenerTalleres[nombre]}</span>
    //         </li>
    //     ))
    // }
    // console.log(content);
    // console.log(Object.keys(dataR.obtenerTalleres));

    // Object.keys(dataR.obtenerTalleres).map(function (nombre, id) {
    //     // use keyName to get current key's name
    //     // and a[keyName] to get its value
    // })
    // let optionsF = Object.keys(dataR).forEach(item => {
    //     console.log(item);



    // })
    // var rows = [];
    // for (let value of Object.values(dataR.obtenerTalleres)) {
    //     console.log(value); // John, then 30
    //     // rows.push(value);
    //     rows.push(value);

    // }

    var rows2 = [];
    if (esTaller) {
        obj = {};
        for (let value of Object.values(dataR.obtenerTalleres)) {
            //console.log(value); // John, then 30
            // rows.push(value);
            var obj = {};

            obj['value'] = value.id;
            obj['label'] = value.nombre + ' - \nDisponible: ' + value.existencia;
            rows2.push(obj);

        }
    }

    if (esClub) {
        for (let value of Object.values(dataC.obtenerClubes)) {
            //console.log(value); // John, then 30
            // rows.push(value);
            var obj = {};

            obj['value'] = value.id;
            obj['label'] = value.nombre + ' - \nDisponible: ' + value.existencia;
            rows2.push(obj);

        }
    }

    console.log("Genero: " + genero);
    if (genero === "Femenino") {
        console.log("Entró genero femenino");
        obj = {};
        for (let value of Object.values(dataF.obtenerClubesFemeninos)) {
            //console.log(value); // John, then 30
            // rows.push(value);
            var obj = {};

            obj['value'] = value.id;
            obj['label'] = value.nombre + ' - \nDisponible: ' + value.existencia;
            rows2.push(obj);

        }
    }
    if (genero2 === "Masculino") {
        obj = {};
        for (let value of Object.values(dataM.obtenerClubesMasculinos)) {
            //console.log(value); // John, then 30
            // rows.push(value);
            var obj = {};

            obj['value'] = value.id;
            obj['label'] = value.nombre + ' - \nDisponible: ' + value.existencia;
            rows2.push(obj);

        }
    }
    // var rowsC = [];
    // objC = {};
    // for (let value of Object.values(dataC.obtenerClubes)) {
    //     //console.log(value); // John, then 30
    //     // rows.push(value);
    //     var objC = {};

    //     objC['value'] = value.id;
    //     objC['label'] = value.nombre + ' - \nDisponible: ' + value.existencia;
    //     rowsC.push(objC);
    // }
    // var rowsF = [];
    // objF = {};
    // for (let value of Object.values(dataF.obtenerClubesFemeninos)) {
    //     //console.log(value); // John, then 30
    //     // rows.push(value);
    //     var objF = {};

    //     objF['value'] = value.id;
    //     objF['label'] = value.nombre + ' - \nDisponible: ' + value.existencia;
    //     rowsF.push(objF);
    // }
    // var rowsM = [];
    // objM = {};
    // for (let value of Object.values(dataM.obtenerClubesMasculinos)) {
    //     //console.log(value); // John, then 30
    //     // rows.push(value);
    //     var objM = {};

    //     objM['value'] = value.id;
    //     objM['label'] = value.nombre + ' - \nDisponible: ' + value.existencia;
    //     rowsM.push(objM);
    // }


    // console.log("Talleres: " + rows2);
    // console.log("Clubes generales: " + rowsC);
    // console.log("Clubes femeninos: " + rowsF);
    // console.log("Clubes masculinos: " + rowsM);




    // options={dataR.obtenerTalleres.map(taller => (
    //     <li className='selectStyle3' key={taller.id} value={taller.nombre}>
    //         {taller.nombre}
    //         {console.log(taller.nombre)}
    //     </li>
    // ))}

    return (
        <Select
            options={rows2}
            placeholder={placeToHold}
            className='selectStyle3XX'
            menuPlacement="bottom"
            menuPosition="fixed"
            onChange={handleChange}
            // getOptionValue={opciones => opciones.id}
            // getOptionLabel={opciones => opciones.nombre}
            // noOptionsMessage={() => "No hay resultados"}
            setClub={setClub}

        >

        </Select>
    )
}
Taller.propTypes = {
    setClub: propTypes.func,
    data: propTypes.object,
    dataR: propTypes.object,
    dataC: propTypes.object,
    dataF: propTypes.object,
    dataM: propTypes.object,
    setCargando: propTypes.func,
    setTaller: propTypes.any,
    loadHeader: propTypes.func,
    loadHeader2: propTypes.func,
    loadHeader3: propTypes.func,
    loadOptions: propTypes.func,
    genero: propTypes.any,
    genero2: propTypes.any,
    esTaller: propTypes.bool,
    esClub: propTypes.bool,
    placeToHold: propTypes.any
};

export default Taller;