import React, { useState, useContext } from 'react';
import { Button } from 'reactstrap';
import Header from '../headerBar';
import Footer from '../footer';
import Semester from './semester';
import Calendar from './calendar';
import Gender from './gender';
import Taller from './taller';
import { Link } from 'react-router-dom';
import BloodType from './bloodType';
import { AllContext } from '../context/AllContext';
import { gql, useQuery, useMutation } from '@apollo/client';
import moment from 'moment';
import { Route, Redirect, useHistory } from "react-router-dom";
import Swal from 'sweetalert2';

const OBTENER_ESTUDIANTES_USUARIO = gql`
  query obtenerEstudiantesUsuario{
       obtenerEstudiantesUsuario{
            id
            nombre
            paterno
            materno
            email
            nacimiento
            semestre
            genero
            curp    
            sangre
            alergias
            padecimientos
        }
    }    
`;
const OBTENER_TALLERES = gql`
query obtenerTalleres {
    obtenerTalleres {
    id
    nombre
    existencia
    dia
    fechaInicio
    fechaTermino
    horaInicio
    horaTermino
    maestro{
      id
      nombre
      paterno
      materno
      email
      telefono
      celular
      curp
      sangre
      alergias
      padecimientos
      creado
      grupo
    }
    aula
    creado    
    }
}
  
`;
const NUEVO_PEDIDO = gql`
    mutation nuevoPedido($input: PedidoInput) {
        nuevoPedido(input: $input) {
        id        
        }
    }
`;

const OBTENER_PEDIDOS = gql`
    query obtenerPedidos {
        obtenerPedidos{
        id
        estudiante{
            id
            nombre
            email
            semestre
        }
        estado
        }
    }  
`;
const OBTENER_CLUBES = gql`
    query obtenerClubes {
        obtenerClubes {
        id
        nombre
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino
        maestro{
        id
        nombre
        paterno
        materno
        email
        telefono
        celular
        curp
        sangre
        alergias
        padecimientos
        creado
        grupo
        }
        aula
        creado    
        }
    }

`;

const OBTENER_CLUBES_FEMENINOS = gql`
    query obtenerClubesFemeninos {
        obtenerClubesFemeninos {
        id
        nombre
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino
        maestro{
        id
        nombre
        paterno
        materno
        email
        telefono
        celular
        curp
        sangre
        alergias
        padecimientos
        creado
        grupo
        }
        aula
        creado    
        }
    }

`;

const OBTENER_CLUBES_MASCULINOS = gql`
    query obtenerClubesMasculinos {
        obtenerClubesMasculinos {
        id
        nombre
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino
        maestro{
        id
        nombre
        paterno
        materno
        email
        telefono
        celular
        curp
        sangre
        alergias
        padecimientos
        creado
        grupo
        }
        aula
        creado    
        }
    }

`;

const Form = () => {
    let history = useHistory();
    const value = useContext(AllContext);
    //console.log(value.student);
    const [name, setName] = useState('');
    const [lastName, setLastName] = useState('');
    const [lastName2, setLastName2] = useState('');
    const [curp, setCurp] = useState('');
    const [allergies, setAllergies] = useState('');
    const [email, setEmail] = useState('');
    const [semester, setSemester] = useState('');
    const [birthday, setBirthday] = useState(null);
    const [gender, setGender] = useState('');
    const [club, setClub] = useState([]);
    const [bloodType, setBloodType] = useState('');
    const [diseases, setDiseases] = useState('');
    const [mensaje, setMensaje] = useState(null);
    const [cargando, setCargando] = useState(true);
    const [taller, setTaller] = useState(null);
    const [loadOpt, loadOptions] = useState(null);
    const [loadHead, loadHeader] = useState(null);
    const [loadHead2, loadHeader2] = useState(null);
    const [loadHead3, loadHeader3] = useState(null);




    var cantidad = 1;
    var total = 1;

    const updateName = (e) => {
        setName(e.target.value);
    }

    const updateCurp = (e) => {
        setCurp(e.target.value);
    }

    const updateLastName = (e) => {
        setLastName(e.target.value);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        value.setStudent(prevStudent => [...prevStudent, {
            name: name,
            lastName: lastName,
            lastName2: lastName2,
            email: email,
            birthday: birthday,
            semester: semester,
            gender: gender,
            club: club,
            curp: curp,
            bloodType: bloodType,
            allergies: allergies,
            diseases: diseases
        }]);
        //console.log(value.student)
    }
    const validarPedido = () => {
        console.log(club);
        return (club => club === 0) ? " opacity-50 cursor-not-allowed " : "";
    }

    const { data, loading, error } = useQuery(OBTENER_ESTUDIANTES_USUARIO);

    const { data: dataP, error: errorP, loading: loadingP } = useQuery(OBTENER_PEDIDOS);

    const { data: dataR, error: errorR, loading: loadingR } = useQuery(OBTENER_TALLERES);

    const { data: dataC, loading: loadingC, error: errorC } = useQuery(OBTENER_CLUBES);

    const { data: dataF, loading: loadingF, error: errorF } = useQuery(OBTENER_CLUBES_FEMENINOS);

    const { data: dataM, loading: loadingM, error: errorM } = useQuery(OBTENER_CLUBES_MASCULINOS);

    const [nuevoPedido] = useMutation(NUEVO_PEDIDO);

    // console.log(data);
    // console.log(loading);
    // console.log(error);
    // console.log(dataR);
    // console.log(loadingR);
    // console.log(errorR);


    if (loading || loadingP || loadingF || loadingR || loadingM || loadingC) return 'Cargando...';
    console.log("Esto es obtener estudiantes al inicio: " + data.obtenerEstudiantesUsuario.email);
    console.log("Esto es obtener pedidos al inicio: " + dataP);
    console.log("Esto es talleres al inicio: " + dataR);
    console.log("Esto es clubes generales al inicio: " + dataC);
    console.log("Esto es clubes femeninos al inicio: " + dataF);
    console.log("Esto es clubes masculinos al inicio: " + dataM);


    var result1 = data.hasOwnProperty(email);
    console.log(result1);
    var resultX = Object.values(data.obtenerEstudiantesUsuario);

    var result2 = Object.values(dataP.obtenerPedidos);
    var result3 = Object.entries(dataP.obtenerPedidos);
    var result4 = Object.values(dataC.obtenerClubes);
    console.log("Esto es result2", result2);
    console.log(result3);
    console.log(result4);

    var marvelHeroes = result2.filter(function (hero) {
        console.log('Esto es hero estudiante email' + hero.estudiante.email)
        return hero.estudiante.email === resultX[0].email;
    });
    console.log("MArvel heroes", marvelHeroes);
    console.log("MArvel heroes length", marvelHeroes.length);

    // // Validación de taller en curso
    // if (marvelHeroes.length > 0) {
    //     //Redireccionar
    //     history.push('/');
    //     //Mensaje de éxito                                
    //     Swal.fire({
    //         icon: 'error',
    //         title: 'Oops...',
    //         text: 'Ya tienes un taller registrado!',
    //         //footer: '<a href>Why do I have this issue?</a>'
    //     })
    // }

    // function filterItems(query) {
    //     return result2.filter(function (el) {
    //         return el.query;
    //     })
    // }

    // console.log(filterItems('estudiante')); // ['apple', 'grapes']
    // console.log(filterItems('estado')); // ['banana', 'mango', 'orange']


    // const dateB = Date(data.obtenerEstudiantesUsuario.map(estudiante => estudiante.nacimiento));
    // const dateString = Date(data.obtenerEstudiantesUsuario.map(estudiante => estudiante.nacimiento)).toString();

    const dateB = Date(data.obtenerEstudiantesUsuario.map(estudiante => estudiante.nacimiento)).toString();
    var formattedDate = moment(data.obtenerEstudiantesUsuario.map(estudiante => estudiante.nacimiento)).format("LL");
    console.log(dateB);
    moment.locale('es');
    var formattedDate = moment(data.obtenerEstudiantesUsuario.map(estudiante => estudiante.nacimiento)).locale('es').format("LLLL");

    var genero = "";
    console.log("String vacío" + typeof genero);
    genero = (data.obtenerEstudiantesUsuario.map(estudiante => estudiante.genero));
    //moment.locale(false);
    genero = genero.toString();
    var genero2 = genero;
    var esTaller = true;
    var esClub = true;
    var placeToHold = "Elija Taller o Club...";

    const mostrarMensaje = () => {
        return (
            <div className='headersGrid' >
                <p>{mensaje}</p>
            </div>
        );

    }


    return (
        <form className="main2" onSubmit={handleSubmit} onKeyPress={e => { if (e.key === 'Enter') { e.preventDefault(); document.getElementById("submitBtn").click() } }}>
            <Header />
            <div className='content' >
                <div className='headersGrid' >
                    <span class="fas fa-user-graduate" style={{ marginLeft: '-0.5vw', marginRight: '0.3vw' }}></span>
                    Elige el Club o Taller deseado
                </div>
                {mensaje && mostrarMensaje()}
                <div className='container1X' style={{ marginRight: '9.3vw' }}>
                    <div className="input-group" style={{ marginLeft: '0.5vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.5vw' }}><i className="fas fa-user-graduate"></i></span>
                        <input
                            id="studentName"
                            type="text"
                            className="form-control"
                            name="name"
                            onChange={updateName}
                            value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.nombre)} />
                    </div>
                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', width: '16.9vw', marginTop: '3.35vh' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.3vw' }}> <i className="fas fa-parking"></i></span>
                        <input
                            id="lastName"
                            type="text"
                            className="form-control"
                            name="lastName"
                            onChange={updateLastName}
                            value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.paterno)} />
                    </div>
                    <div className="input-group" style={{ marginLeft: '0.5vw', paddingLeft: '0vw', paddingRight: '1vw', marginTop: '3.35vh', width: '17.3vw', marginRight: '1vw' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.7vw', marginLeft: '0.3vw' }}><i className="fas fa-file-medical-alt"></i></span>
                        <input
                            id="lastName2"
                            type="text"
                            className="form-control"
                            name="lastName2"
                            onChange={e => setLastName2(e.target.value)}
                            value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.materno)} />
                    </div>
                    <div className="input-group" style={{ marginLeft: '0.5vw', paddingLeft: '0vw', paddingRight: '1vw', marginTop: '3.35vh', width: '17.3vw', marginRight: '1vw' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.2vw' }}><i className="fas fa-venus-mars"></i></span>
                        <input
                            id="gender"
                            type="text"
                            className="form-control"
                            name="gender"
                            value={genero}
                        //onChange={data => setGender(data.target.value)}

                        />
                    </div>
                    <div className="input-group" style={{ marginLeft: '0.7vw', paddingLeft: '0', paddingRight: '1vw', marginRight: '0vw', width: '17.3vw', marginTop: '3.35vh' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.3vw' }}><i className="fas fa-birthday-cake"></i></span>
                        <input
                            id="birthday"
                            type="text"
                            className="form-control"
                            name="birthday"
                            value={formattedDate}
                            onChange={data => setBirthday(data.target.value)}
                        //placeholder={formattedDate} 
                        />
                    </div>
                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0vw', paddingRight: '1vw', marginTop: '3.35vh', width: '17.3vw' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0.3vw' }}><i className="fas fa-allergies"></i></span>
                        <input
                            id="allergies"
                            type="text"
                            className="form-control"
                            name="allergies"
                            onChange={e => setAllergies(e.target.value)}
                            value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.alergias)} />
                    </div>

                    <Button
                        style={{ width: '10vw', height: '3.5vw', marginLeft: '7.6vw', marginTop: '3.5vh' }}
                        type='submit'
                        disabled={club.length === 0}
                        onClick={async () => {
                            try {
                                await nuevoPedido({ variables: { input: { pedido: [{ id: club, cantidad: cantidad }], total: total, estudiante: data.obtenerEstudiantesUsuario[0].id, estado: "COMPLETADO" } } });
                                console.log(error);
                                console.log(mensaje);

                                //Redireccionar
                                history.push('/');
                                //Mensaje de éxito                                
                                Swal.fire(
                                    'Correcto',
                                    'El taller se registró correctamente',
                                    'success'
                                )

                            } catch (error) {
                                setMensaje(error.message.replace('GraphQL error: ', ''));
                                console.log(mensaje);

                                setTimeout(() => {
                                    setMensaje(null);
                                }, 3000);
                            }
                        }
                        }
                    //className={`${validarPedido()}`}
                    >
                        Guardar
                    </Button>
                    {console.log(data.obtenerEstudiantesUsuario[0].id)}
                    {console.log(data.obtenerEstudiantesUsuario[0])}

                </div>
                <div className='container2X'>

                    {/* <div className="input-group" style={{ paddingLeft: '0.3vw', paddingRight: '1vw' }}>
                        <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                        <input id="semester" type="text" className="form-control" name="semester" placeholder="Semestre" />
                    </div> */}
                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-envelope"></i></span>
                        <input
                            id="email"
                            type="text"
                            className="form-control"
                            name="email"
                            onChange={data => setEmail(data.target.value)}
                            value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.email)} />
                    </div>
                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-atlas"></i></span>
                        <input
                            id="curp"
                            type="text"
                            className="form-control"
                            name="curp"
                            onChange={updateCurp}
                            value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.curp)} />
                    </div>
                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-university"></i></span>
                        <input
                            id="semester"
                            type="text"
                            className="form-control"
                            name="semester"
                            onChange={data => setSemester(data.target.value)}
                            value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.semestre)} />
                    </div>


                    <div className="input-group " style={{ marginLeft: '0.7vw', paddingLeft: '0', paddingRight: '1vw', marginRight: '1vw', marginTop: '2.8vh' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.2vw', marginLeft: '0vw' }}><i className="fas fa-guitar"></i></span>
                        <Taller
                            club={club}
                            setClub={setClub}
                            data={data}
                            dataR={dataR}
                            loadingR={loadingR}
                            errorR={errorR}
                            dataC={dataC}
                            loadingC={loadingC}
                            errorC={errorC}
                            dataF={dataF}
                            loadingF={loadingF}
                            errorF={errorF}
                            dataM={dataM}
                            loadingM={loadingM}
                            errorM={errorM}
                            setTaller={setTaller}
                            setCargando={setCargando}
                            loadOptions={loadOptions}
                            loadHeader={loadHeader}
                            loadHeader2={loadHeader2}
                            loadHeader3={loadHeader3}
                            genero={genero}
                            genero2={genero2}
                            esTaller={esTaller}
                            esClub={esClub}
                            placeToHold={placeToHold}
                        />
                    </div>
                    {/* <div className="input-group" style={{ marginTop: '0vh', marginLeft: '0.5vw', paddingLeft: '0.3vw', paddingRight: '1vw', marginRight: '1vw', width: '18.5vw' }}>
                        <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                        <input id="bloodType" type="text" className="form-control" name="bloodType" placeholder="Tipo de Sangre" />
                    </div> */}
                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '1vw', paddingRight: '0.6vw', marginTop: '3.35vh', width: '17vw' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '-0.1vw' }}><i className="fas fa-syringe"></i></span>
                        <input
                            id="bloodType"
                            type="text"
                            className="form-control"
                            name="bloodType"
                            onChange={data => setBloodType(data.target.value)}
                            value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.sangre)} />
                    </div>
                    <div className="input-group" style={{ marginLeft: '0.7vw', marginRight: '0.5vw', paddingRight: '0.6vw', marginTop: '3.23vh', width: '16.8vw' }}>
                        <span className="input-group-addon" style={{ color: 'purple', marginRight: '0.5vw', marginLeft: '0vw' }}><i className="fas fa-disease"></i></span>
                        <input
                            id="diseases"
                            type="text"
                            className="form-control"
                            name="diseases"
                            onChange={e => setDiseases(e.target.value)}
                            value={data.obtenerEstudiantesUsuario.map(estudiante => estudiante.padecimientos)} />
                    </div>
                    <Link to='/dashboard'>
                        <Button style={{ marginLeft: '1vw', width: '10vw', height: '3.5vw', marginRight: '5vw', marginTop: '3.5vh' }}>Regresar</Button>
                    </Link>
                </div>
            </div>
            <Footer />
        </form >
    );

};

export default Form;
