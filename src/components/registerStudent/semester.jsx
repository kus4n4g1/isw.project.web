import React from 'react';
import Select from 'react-select';
import propTypes from 'prop-types';

const Semester = (props) => {

    const {
        setSemester
    } = props;

    const options = [{ value: 'I', label: 'I' }, { value: 'II', label: 'II' }, { value: 'III', label: 'III' }, { value: 'IV', label: 'IV' }, { value: 'V', label: 'V' }, { value: 'VI', label: 'VI' }]

    return (
        <Select
            className='selectStyle2'
            options={options}
            placeholder='Semestre'
            style={{ fontSize: '16px' }}
            menuPlacement="bottom"
            menuPosition="fixed"
            onChange={e => setSemester(e.value)}
        />
    );
};

Semester.propTypes = {
    setSemester: propTypes.func
};

export default Semester;