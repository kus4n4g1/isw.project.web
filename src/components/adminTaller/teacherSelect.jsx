import React, { useState } from 'react';
import Select from 'react-select';
import propTypes from 'prop-types';
import { gql, useQuery } from '@apollo/client';
import AsyncSelect from 'react-select';
//import { colors } from 'react-select/src/theme';




const TeacherSelect = (props) => {
    const {
        value,
        handleChange,
        onChange,
        onBlur,
        errors,
        touched,
        dataR,
    } = props;
    console.log(value);
    console.log(errors);
    console.log(touched);

    const [selectedTeacher, setSelectedTeacher] = useState("");
    //console.log(dataR);
    // console.log(setClub);

    // const handleChange = (e) => {
    //     setClub(e.value);
    //     console.log("This is it man" + e.value);
    //     setTaller(e.value);
    //     loadOptions({ variables: { id: e.value } });
    //     setCargando(false);
    // }
    const handleTeacherChange = (selectedTeacher, value) => {
        value = selectedTeacher.value;
        console.log(selectedTeacher);
        setSelectedTeacher(selectedTeacher);
    };


    var rows2 = [];

    obj = {};
    for (let value of Object.values(dataR.obtenerMaestros)) {
        //console.log(value); // John, then 30
        // rows.push(value);
        var obj = {};

        obj['value'] = value.id;
        obj['label'] = value.paterno + ' ' + value.materno + ' ' + value.nombre;
        rows2.push(obj);

    }


    return (
        <Select
            id="teacher"
            name="teacher"
            options={rows2}
            placeholder='Elija el Maestro'
            className='selectStyle3X'
            menuPlacement="bottom"
            menuPosition="fixed"
            //handleChange={onChange}
            //onChange={handleChange}
            onBlur={onBlur}
            // getOptionValue={opciones => opciones.id}
            // getOptionLabel={opciones => opciones.nombre}
            // noOptionsMessage={() => "No hay resultados"}
            onChange={selectedOption => {
                handleTeacherChange(selectedOption);
                // handleYearChange(selectedOption, values);
                // values.year = selectedOption.value;
                console.log("values", value);
                handleChange("teacher")(selectedOption.value);;
            }}
        >

        </Select>
    )
}
TeacherSelect.propTypes = {
    value: propTypes.object,
    dataR: propTypes.object,
    handleChange: propTypes.func,
    onChange: propTypes.func,
    onBlur: propTypes.func,
    errors: propTypes.object,
    touched: propTypes.object
};

export default TeacherSelect;