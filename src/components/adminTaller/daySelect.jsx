import React, { useState } from 'react';
import Select from 'react-select';
import propTypes from 'prop-types';

const DaySelect = (props) => {
    const {
        value,
        handleChange,
        onChange,
        onBlur,
        errors,
        touched,

    } = props;
    const [selectedDay, setSelectedDay] = useState("");

    const handleDayChange = (selectedDay, value) => {
        value = selectedDay.value;
        console.log(selectedDay);
        setSelectedDay(selectedDay);
    };
    const options = [
        { value: 'Lunes', label: 'Lunes' },
        { value: 'Martes', label: 'Martes' },
        { value: 'Miércoles', label: 'Miércoles' },
        { value: 'Jueves', label: 'Jueves' },
        { value: 'Viernes', label: 'Viernes' },
        { value: 'Sabado', label: 'Sabado' },
        { value: 'Domingo', label: 'Domingo' },
    ];

    return (
        <Select
            id="day"
            name="day"
            options={options}
            className='selectStyle3X'
            // style={{ width: '20vw !important', zIndex: '999' }}
            placeholder='Día que se impartirá'
            menuPlacement="bottom"
            menuPosition="fixed"
            //handleChange={onChange}
            errors={errors}
            touched={touched}
            onBlur={onBlur}
            onChange={selectedOption => {
                handleDayChange(selectedOption);
                console.log("values", value);
                handleChange("day")(selectedOption.value);;
            }}
        //onChange={handleChange}
        />
    );

}
DaySelect.propTypes = {
    value: propTypes.object,
    handleChange: propTypes.func,
    onChange: propTypes.func,
    onBlur: propTypes.func,
    errors: propTypes.object,
    touched: propTypes.object,

};

export default DaySelect;