import React, { useState } from 'react';
import SideBar from '../sideBar';
import Header from '../headerBar';
import { gql, useQuery, useLazyQuery, useMutation } from '@apollo/client';
import { Button } from 'reactstrap';
import ReactTable from 'react-table';
import * as ReactBootstrap from 'react-bootstrap';
import Taller from '../registerStudent/taller';
// import { Glyphicon } from 'react-bootstrap';
// import "react-table/react-table.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import ModalTaller from '../modal/modalTaller';
import ModalTeacher from '../modal/modalTeacher';
import ModalTeacherMod from '../modal/modalTeacherMod';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import Swal from 'sweetalert2';
import { useHistory } from "react-router-dom";
import { Link } from 'react-router-dom';
import t from 'typy';


//import AdminTeacher from '../adminTeachers';

const OBTENER_MAESTROS = gql`
    query obtenerMaestros{
        obtenerMaestros{
        id
        nombre
        paterno
        materno
        email
        telefono
        celular
        curp
        sangre
        alergias
        padecimientos
        grupo    
        }
    }  
`;
const OBTENER_MAESTROSP = gql`
    query obtenerMaestros{
        obtenerMaestros{
        nombre
        paterno
        materno
        email
        celular
        curp
        sangre       
        }
    }  
`;
const OBTENER_MAESTRO = gql`
    query obtenerMaestro($id:ID!){
        obtenerMaestro(id:$id){
        id
        nombre
        paterno
        materno
        email
        telefono
        celular
        curp
        sangre
        alergias
        padecimientos   
        }
    }
`;
const OBTENER_TALLERES = gql`
    query obtenerTalleres {
        obtenerTalleres {
        id
        nombre
        clave
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino
        maestro{
        id
        }
        aula
        creado    
        }
    }
`;

const OBTENER_CLUBES = gql`
    query obtenerClubes {
        obtenerClubes {
        id
        nombre
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino
        maestro{
        id
        nombre
        paterno
        materno
        email
        telefono
        celular
        curp
        sangre
        alergias
        padecimientos
        creado
        grupo
        }
        aula
        creado    
        }
    }

`;

const OBTENER_CLUBES_FEMENINOS = gql`
    query obtenerClubesFemeninos {
        obtenerClubesFemeninos {
        id
        nombre
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino
        maestro{
        id
        nombre
        paterno
        materno
        email
        telefono
        celular
        curp
        sangre
        alergias
        padecimientos
        creado
        grupo
        }
        aula
        creado    
        }
    }

`;

const OBTENER_CLUBES_MASCULINOS = gql`
    query obtenerClubesMasculinos {
        obtenerClubesMasculinos {
        id
        nombre
        existencia
        dia
        fechaInicio
        fechaTermino
        horaInicio
        horaTermino
        maestro{
        id
        nombre
        paterno
        materno
        email
        telefono
        celular
        curp
        sangre
        alergias
        padecimientos
        creado
        grupo
        }
        aula
        creado    
        }
    }

`;
const OBTENER_PEDIDOS = gql`
    query obtenerPedidos {
        obtenerPedidos{
        id
        estudiante{
            id
            nombre
            email
            semestre
        }
        estado
        }
    }  
`;


const ELIMINAR_MAESTRO = gql`
    mutation eliminarMaestro($id:ID!){
        eliminarMaestro(id:$id)
    }  
  `;


const Dashboard = () => {
    let history = useHistory();
    const [club, setClub] = useState([]);
    const [cargando, setCargando] = useState(true);
    const [taller, setTaller] = useState(null);
    const [pedido, setPedido] = useState(null);
    const [teacherX, setTeacherX] = useState(null);

    // const [resP, setResP] = useState(null);
    // const [resT, setResT] = useState(null);
    // const [resC, setResC] = useState(null);
    // const [resF, setResF] = useState(null);
    // const [resM, setResM] = useState(null);

    var marvelHeroesP = null;
    var marvelHeroesT = null;
    var marvelHeroesC = null;
    var marvelHeroesM = null;
    var marvelHeroesF = null;

    var p3d1d0 = null;
    const [modal, setModal] = useState(null);
    const [modal2, setModal2] = useState(null);

    //const [expanded, setExpanded] = useState();
    //const [defaultExpandedRows, setDefaultExpandedRow] = useState();

    const [getTeacherX] = useLazyQuery(OBTENER_MAESTRO, {
        onCompleted: thisData => {

            p3d1d0 = thisData.obtenerMaestro;
            setTeacherX(p3d1d0);
            console.log('teacherx', teacherX)
            console.log('pedido', p3d1d0);
            setModal(true);
            setTeacherX(p3d1d0);
        }
    });

    const [getTeacherY] = useLazyQuery(OBTENER_MAESTRO, {
        onCompleted: thisData => {

            p3d1d0 = thisData.obtenerMaestro;
            setTeacherX(p3d1d0);
            console.log('teacherx', teacherX)
            console.log('pedido', p3d1d0);
            setModal2(true);
            setTeacherX(p3d1d0);
        }
    });

    const [getTeacher] = useLazyQuery(OBTENER_MAESTRO, {
        onCompleted: thisData => {
            console.log('data ', thisData);
            console.log('data obtener Maestro por id', thisData.obtenerMaestro.id);
            p3d1d0 = thisData.obtenerMaestro.id;
            console.log("Esto es obtener maestro por id: " + p3d1d0);



            for (var i = 0; i < arrTeachers.length; i++) {
                console.log(arrTeachers[i][1]);
                if (arrTeachers[i][1] == p3d1d0) {
                    arrTeacherFiltered.push(arrTeachers[i][1]);
                }
            }
            for (var i = 0; i < arrTeachers4.length; i++) {
                console.log(arrTeachers4[i][1]);
                if (arrTeachers4[i][1] == p3d1d0) {
                    arrTeacherFiltered4.push(arrTeachers4[i][1]);
                }
            }
            for (var i = 0; i < arrTeachers5.length; i++) {
                console.log(arrTeachers5[i][1]);
                if (arrTeachers5[i][1] == p3d1d0) {
                    arrTeacherFiltered5.push(arrTeachers5[i][1]);
                }
            }
            for (var i = 0; i < arrTeachers6.length; i++) {
                console.log(arrTeachers6[i][1]);
                if (arrTeachers6[i][1] == p3d1d0) {
                    arrTeacherFiltered6.push(arrTeachers6[i][1]);
                }
            }
            marvelHeroesT = arrTeacherFiltered;
            marvelHeroesC = arrTeacherFiltered4;
            marvelHeroesF = arrTeacherFiltered5;
            marvelHeroesM = arrTeacherFiltered6;

            console.log('Este es ahora arrTeacher Filtered: ' + arrTeacherFiltered);
            console.log('Este es ahora marvelHeroesT: ' + marvelHeroesT);
            console.log('Esto es ahora marvel heroes T.length: ' + marvelHeroesT.length);



            //console.log('Esto vale result2 cuando se va a marvelizar', result2);

            // marvelHeroesP = result2.filter(function (hero) {
            //     //console.log('Esto es hero estudiante email' + hero.id)
            //     return hero.id === p3d1d0;
            // });
            // marvelHeroesT = result3XX.maestro.filter(function (hero) {
            //     return hero === p3d1d0;
            // });
            // marvelHeroesC = result4.filter(function (hero) {
            //     return hero.id === p3d1d0;
            // });
            // marvelHeroesF = result5.filter(function (hero) {
            //     return hero.id === p3d1d0;
            // });
            // marvelHeroesM = result6.filter(function (hero) {
            //     return hero.id === p3d1d0;
            // });
            // setBanderaSetP(true);

            // setBanderaSetP(false);
            // setResP(null);

            // setBanderaSetT(true);

            // setBanderaSetT(false);
            // setResT(null);

            // setBanderaSetC(true);

            // setResC(null);

            // setBanderaSetF(true);

            // setBanderaSetF(false);
            // setResF(null);

            // setBanderaSetM(true);

            // setBanderaSetM(false);
            // setResM(null);

            if (marvelHeroesT.length > 0 ||
                marvelHeroesC.length > 0 ||
                marvelHeroesF.length > 0 ||
                marvelHeroesM.length > 0
            ) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Ya tienes un taller registrado!',
                    //footer: '<a href>Why do I have this issue?</a>'
                })
            } else {
                Swal.fire({
                    title: '¿Deseas eliminar este Maestro?',
                    text: "Esta acción no podrá deshacerse.",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: 'red',
                    cancelButtonColor: '#9400D3',
                    confirmButtonText: 'Sí, Elimínalo',
                    cancelButtonText: 'No, Cancelar'
                }).then(async (result) => {
                    if (result) {
                        try {
                            console.log("justo antes de eliminar" + p3d1d0);

                            console.log("3st3 3s p3d1d0", p3d1d0);
                            const { data } = await eliminarMaestro({
                                variables: {
                                    id: p3d1d0
                                },
                                refetchQueries: [
                                    {
                                        query: OBTENER_MAESTROS
                                    }
                                ]

                            })
                            renderPlayer();
                        } catch (error) {
                            console.log(error);
                        }
                        if (result.value) {
                            history.push('/dashboard/teacher');
                            Swal.fire(
                                'Eliminado',
                                "El maestro se ha eliminado exitosamente",
                                'success'
                            )
                        }
                    }

                })
            }


        }
    });


    const [eliminarMaestro] = useMutation(ELIMINAR_MAESTRO);
    //     update(cache) {
    //         // Obtener una copia del objeto de cache
    //         const {obtenerPedidosTaller} = cache.readQuery({ query: OBTENER_PEDIDOS_TALLER });

    //         // Reescribir el cache
    //         cache.writeQuery({
    //             query: OBTENER_PEDIDOS_TALLER,
    //             data: {
    //                 obtenerPedidosTaller: obtenerPedidosTaller.filter(estudianteActual => estudianteActual.id != p3d1d0)
    //             }
    //         })
    //     }
    // });

    const { data, error, loading } = useQuery(OBTENER_MAESTROS);
    const { data: dataP, error: errorP, loading: loadingP } = useQuery(OBTENER_MAESTROSP);
    //const { data: dataP, error: errorP, loading: loadingP } = useQuery(OBTENER_PEDIDOS);

    const { data: dataR, error: errorR, loading: loadingR } = useQuery(OBTENER_TALLERES);

    const { data: dataC, loading: loadingC, error: errorC } = useQuery(OBTENER_CLUBES);

    const { data: dataF, loading: loadingF, error: errorF } = useQuery(OBTENER_CLUBES_FEMENINOS);

    const { data: dataM, loading: loadingM, error: errorM } = useQuery(OBTENER_CLUBES_MASCULINOS);

    if (loading || loadingF || loadingR || loadingM || loadingC || loadingP) return 'Cargando...';

    var resultX = Object.values(data.obtenerMaestros);
    var resultXY = Object.values(dataP.obtenerMaestros);
    //var result2 = Object.values(dataP.obtenerPedidos);
    var result3 = Object.values(dataR.obtenerTalleres);
    var result4 = Object.values(dataC.obtenerClubes);
    var result5 = Object.values(dataF.obtenerClubesFemeninos);
    var result6 = Object.values(dataM.obtenerClubesMasculinos);
    var result3X = Object.values(result3);
    var result4X = Object.values(result4);
    var result5X = Object.values(result5);
    var result6X = Object.values(result6);


    // var result3XX = Object.values(result3X[0].maestro);
    // var result3XXX = result3XX[1];
    var arrTeachers = [];
    var arrTeacherFiltered = [];
    var arrTeachers4 = [];
    var arrTeacherFiltered4 = [];
    var arrTeachers5 = [];
    var arrTeacherFiltered5 = [];
    var arrTeachers6 = [];
    var arrTeachers7 = [];

    var arrTeacherFiltered6 = [];

    for (var i = 0; i < result3X.length; i++) {
        arrTeachers.push(Object.values(result3X[i].maestro));
    }
    for (var i = 0; i < result4X.length; i++) {
        arrTeachers4.push(Object.values(result4X[i].maestro));
    }
    for (var i = 0; i < result5X.length; i++) {
        arrTeachers5.push(Object.values(result5X[i].maestro));
    }
    for (var i = 0; i < result6X.length; i++) {
        arrTeachers6.push(Object.values(result6X[i].maestro));
    }
    for (var i = 0; i < resultXY.length; i++) {
        arrTeachers7.push(Object.values(resultXY[i]));
    }
    console.log("arrteachers" + arrTeachers);
    console.log("arrteachers club" + arrTeachers4);
    console.log("arrteachers club fem" + arrTeachers5);
    console.log("arrteachers club masc" + arrTeachers6);
    console.log("Return data: " + arrTeachers7);


    // marvelHeroesT = arrTeachers.filter(function (hero) {
    //     return hero[1];
    // });
    //console.log('Este es ahora : ' + marvelHeroesT);



    //var result4 = Object.values(dataC.obtenerClubes);

    // var marvelHeroes = result2.filter(function (hero) {
    //     console.log('Esto es hero estudiante email' + hero.estudiante.email)
    //     return hero.estudiante.email === resultX[0].email;
    // });
    // console.log("MArvel heroes", marvelHeroes);
    // console.log("MArvel heroes length", marvelHeroes.length);


    // const [loadOptions, { data: players, error: errorZ, loading: loadingZ }] = useLazyQuery(OBTENER_PEDIDOS_TALLER, {
    //     variables: { id: taller },
    // onCompleted: data => {
    //     console.log('Variable donde se almacenarán los pedidos por taller' + pedido);
    //     console.log("Que es data" + data.obtenerPedidosTaller[0]);
    //     console.log('Que es players' + players.obtenerPedidosTaller[0]);
    //     setPedido(players);
    //     console.log("Pedidos por taller después de setear: " + pedido);

    // }
    // skip: { id: null }
    // });



    console.log(data);

    console.log(loading);
    console.log(error);

    const renderPlayer = (player, index) => {
        return (
            <tr key={player.id} >
                <td> {player.nombre} </td>
                <td> {player.paterno} </td>
                <td> {player.materno} </td>
                <td> {player.curp} </td>
                <td> {player.email} </td>
                <td> {player.celular} </td>
                <td  >
                    {/* <a href="#" class="btn btn-success btn-lg">
                        <span class="glyphicon glyphicon-remove"></span> Borrar
                    </a> */}

                    <a style={{ color: '#6A5ACD', marginRight: '0.5vw' }} onClick={() => getTeacherX({ variables: { id: player.id } })}>
                        <span className="fas fa-eye"></span>
                        <ModalTeacher
                            modal={modal}
                            setModal={setModal}
                            teacherX={teacherX}
                        />
                    </a>


                    <a style={{ color: '#9400D3', marginRight: '0.5vw' }} onClick={() => getTeacherY({ variables: { id: player.id } })}>
                        <span className="fas fa-edit"></span>
                        <ModalTeacherMod
                            modal2={modal2}
                            setModal2={setModal2}
                            teacherX={teacherX}
                        />
                    </a>
                    <a style={{ color: 'red', marginRight: '0vw' }} onClick={() => getTeacher({ variables: { id: player.id } })


                    }>
                        <span className="fas fa-trash-alt"></span>
                    </a>



                    {/* <button type="button" class="buttonDelete">
                    </button> */}

                </td>
            </tr>
        )
    }

    const headerReady = () => {
        return (
            <tr>
                <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>
                    <span class="fas fa-graduation-cap" style={{ marginRight: '0.3vw' }}></span>
                    TABLA DE MAESTROS
                </th>
            </tr>
        );


        if (taller === '5ebfe0272b0a583094431c36') {
            return (
                <tr>
                    <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE DESARROLLO DOCENTE</th>
                </tr>
            );
        }
        if (taller === '5ebfb65bc4e0b63dc0f52f37') {
            return (
                <tr>
                    <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE MÚSICA</th>
                </tr>
            );
        }
        if (taller === '5ebfb615c4e0b63dc0f52f36') {
            return (
                <tr>
                    <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE DANZA</th>
                </tr>
            );
        }
        if (taller === '5ebfb5dfc4e0b63dc0f52f35') {
            return (
                <tr>
                    <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>TALLER DE APRECIACIÓN DE LAS ARTES</th>
                </tr>
            );
        }

    }
    // const expandRow = {
    //     renderer: row => (
    //         <div>
    //             <p>{`This Expand row is belong to rowKey ${row.id}`}</p>
    //             <p>You can render anything here, also you can add additional data on every row object</p>
    //             <p>expandRow.renderer callback will pass the origin row object to you</p>
    //         </div>
    //     )
    // };

    var columns = [
        { title: "Tipo", dataKey: "Tipo" },
        { title: "Nombre", dataKey: "Nombre" },
        { title: "Paterno", dataKey: "Paterno" },
        { title: "Materno", dataKey: "Materno" },
        { title: "Email", dataKey: "Email" },
        { title: "Celular", dataKey: "Celular" },
        { title: "CURP", dataKey: "CURP" },
        // { title: "T.Sangre", dataKey: "Sangre" },
        // { title: "Alergias", dataKey: "Alergias" },
        // { title: "Padecimientos", dataKey: "Padecimientos" },
    ];

    var res = [];

    for (var i in resultX)
        res.push(resultX[i].nombre);


    for (var i in resultX)
        res.slice(resultX[i].paterno);

    var rows = res;
    console.log('resultx', resultX)
    console.log('rows', rows)
    // const tablePDF = () => {
    //     var doc = new jsPDF('p', 'pt');
    //     doc.autoTable(columns, arrTeachers7);
    //     doc.save('tableReady.pdf');
    //     // const doc = new jsPDF();

    //     // doc.autoTable({ tableReady });

    //     // doc.save('tableReady.pdf');
    // }


    const tableReady = () => {

        return (
            <ReactBootstrap.Table striped bordered hover style={{ backgroundColor: 'white' }}
            // SubComponent={row => {
            //     return (
            //         <div style={{ 'border': '1px solid rgba(0, 0, 0, 0.05)' }}>
            //             {row.row.summary}
            //         </div>
            //     )
            // }}
            // onExpandedChange={newExpanded => onExpandedChange(newExpanded)}


            >
                <thead style={{ fontWeight: '500px', font: 'Roboto', color: 'white', backgroundImage: 'linear-gradient(to right, #BA55D3, #DDA0DD)' }}>
                    {headerReady()}
                    <tr>
                        <th>Nombre</th>
                        <th>Paterno</th>
                        <th>Materno</th>
                        <th>CURP</th>
                        <th>Email</th>
                        <th>Celular</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    {data.obtenerMaestros.map(renderPlayer)}
                </tbody>
            </ReactBootstrap.Table>
        );
    }

    return (
        <div className='main'>
            <Header />
            <SideBar />
            <div className='content' style={{ marginLeft: '8vw' }}>

                <div style={{ display: 'flex', flexDirection: 'column' }}>
                    {tableReady()}
                    <div style={{ display: 'flex' }}>
                        <Button style={{ opacity: '0.9', width: '10vw !important', paddingLeft: "0px !important", paddingRight: "0px !important", height: '8vh' }}
                            onClick={() => {
                                var doc = new jsPDF('p', 'pt');
                                doc.autoTable(columns, arrTeachers7);
                                doc.save('tableReady.pdf');
                            }}
                        >
                            <span class="fas fa-print"></span> Reporte
                        </Button>
                        <Link to='/dashboard'>
                            <Button style={{ opacity: '0.9', width: '10vw !important', paddingLeft: "0px !important", paddingRight: "0px !important", height: '8vh' }}
                            // onClick={tablePDF()}
                            >
                                <span class="fas fa-home"></span> Regresar
                            </Button>
                        </Link>
                    </div>
                </div>
            </div>
            <div className='footer'>
                <div>
                    <span class="fas fa-university" style={{ marginRight: '0.3vw' }}></span>
                    Copyright © 2020, B3st0 Team
                </div>
            </div>
        </div>
    );

}

export default Dashboard;