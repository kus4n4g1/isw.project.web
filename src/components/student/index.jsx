import React, { useState } from 'react';
import SideBar from '../sideBar';
import Header from '../headerBar';
import { gql, useQuery, useLazyQuery, useMutation } from '@apollo/client';
import { Button } from 'reactstrap';
import ReactTable from 'react-table';
import * as ReactBootstrap from 'react-bootstrap';
import Taller from '../registerStudent/taller';
// import { Glyphicon } from 'react-bootstrap';
// import "react-table/react-table.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import ModalTaller from '../modal/modalTaller';
import ModalStudent from '../modal/modalStudent';
import ModalStudentMod from '../modal/modalStudentMod';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import Swal from 'sweetalert2';
import { useHistory } from "react-router-dom";
import { Link } from 'react-router-dom';

//import AdminTeacher from '../adminTeachers';

const OBTENER_ALUMNOS = gql`
    query obtenerEstudiantes{
        obtenerEstudiantes{
        id
        nombre
        paterno
        materno
        email    
        nacimiento
        semestre
        genero
        curp
        grupo    
        }
    }
`;
const OBTENER_ALUMNOSP = gql`
    query obtenerEstudiantes{
        obtenerEstudiantes{        
        nombre
        paterno
        materno
        email    
        semestre
        genero
        curp
        }
    }
`;

const OBTENER_PEDIDOS = gql`
    query obtenerPedidos {
        obtenerPedidos{
        id
        estudiante{
            id
            nombre
            email
            semestre
        }
        estado
        }
    }  
`;
const OBTENER_ESTUDIANTE = gql`

query obtenerEstudiante($id:ID!){
    obtenerEstudiante(id:$id){
      id
      nombre
      paterno
      materno
      email
      nacimiento
      semestre
      genero
      curp
      sangre
      alergias
      padecimientos
      matricula
    }
  }
`;

const OBTENER_ESTUDIANTEX = gql`

query obtenerEstudiante($id:ID!){
    obtenerEstudiante(id:$id){
      id
      nombre
      paterno
      materno
      email
      nacimiento
      semestre
      genero
      curp
      sangre
      alergias
      padecimientos
      matricula
    }
  }
`;
// const OBTENER_PEDIDOS_TALLER = gql`
//     query obtenerPedidosTaller($id:ID!) {
//         obtenerPedidosTaller(id:$id){
//         id
//         pedido{
//             id
//             cantidad
//         }
//         estudiante{
//             id
//             nombre
//             paterno
//             materno
//             email
//             nacimiento
//             semestre
//             genero
//             curp
//             sangre
//             alergias
//             padecimientos     
//         }
//         total    
//         }
//     }
//   `;

const ELIMINAR_ESTUDIANTE = gql`
    mutation eliminarEstudiante($id:ID!){
        eliminarEstudiante(id:$id)
    }  
  `;


const Dashboard = () => {
    let history = useHistory();
    const [club, setClub] = useState([]);
    const [cargando, setCargando] = useState(true);
    const [taller, setTaller] = useState(null);
    const [pedido, setPedido] = useState(null);
    const [studentX, setStudentX] = useState(null);
    var p3d1d0 = null;
    const [modal, setModal] = useState(null);
    const [modal2, setModal2] = useState(null);

    //const [expanded, setExpanded] = useState();
    //const [defaultExpandedRows, setDefaultExpandedRow] = useState();

    var marvelHeroesP = null;
    const [getStudentX] = useLazyQuery(OBTENER_ESTUDIANTE, {
        onCompleted: thisData => {

            p3d1d0 = thisData.obtenerEstudiante;
            setStudentX(p3d1d0);
            console.log('studentx', studentX)
            console.log('pedido', p3d1d0);
            setModal(true);
            setStudentX(p3d1d0);
        }
    });
    const [getStudentY] = useLazyQuery(OBTENER_ESTUDIANTE, {
        onCompleted: thisData => {

            p3d1d0 = thisData.obtenerEstudiante;
            setStudentX(p3d1d0);
            console.log('studentx', studentX)
            console.log('pedido', p3d1d0);
            setModal2(true);
            setStudentX(p3d1d0);
        }
    });
    const [getStudent] = useLazyQuery(OBTENER_ESTUDIANTE, {
        onCompleted: thisData => {
            console.log('data ', thisData);
            console.log('data obtener estudiante por id', thisData.obtenerEstudiante.id);
            p3d1d0 = thisData.obtenerEstudiante.id;
            console.log("Esto es obtener estudiante por id: " + p3d1d0);



            for (var i = 0; i < arrStudents.length; i++) {
                console.log(arrStudents[i][1]);
                if (arrStudents[i][1] == p3d1d0) {
                    arrStudentFiltered.push(arrStudents[i][1]);
                }
            }
            // for (var i = 0; i < arrTeachers4.length; i++) {
            //     console.log(arrTeachers4[i][1]);
            //     if (arrTeachers4[i][1] == p3d1d0) {
            //         arrTeacherFiltered4.push(arrTeachers4[i][1]);
            //     }
            // }
            // for (var i = 0; i < arrTeachers5.length; i++) {
            //     console.log(arrTeachers5[i][1]);
            //     if (arrTeachers5[i][1] == p3d1d0) {
            //         arrTeacherFiltered5.push(arrTeachers5[i][1]);
            //     }
            // }
            // for (var i = 0; i < arrTeachers6.length; i++) {
            //     console.log(arrTeachers6[i][1]);
            //     if (arrTeachers6[i][1] == p3d1d0) {
            //         arrTeacherFiltered6.push(arrTeachers6[i][1]);
            //     }
            // }
            marvelHeroesP = arrStudentFiltered;
            // marvelHeroesC = arrTeacherFiltered4;
            // marvelHeroesF = arrTeacherFiltered5;
            // marvelHeroesM = arrTeacherFiltered6;

            console.log('Este es ahora arrStudent Filtered: ' + arrStudentFiltered);
            console.log('Este es ahora marvelHeroesP: ' + marvelHeroesP);
            console.log('Esto es ahora marvel heroes T.length: ' + marvelHeroesP.length);

            if (marvelHeroesP.length > 0

                // ||
                // marvelHeroesC.length > 0 ||
                // marvelHeroesF.length > 0 ||
                // marvelHeroesM.length > 0
            ) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Ya tiene un taller o club en curso. Por favor, elimínelo primero de allí!',
                    //footer: '<a href>Why do I have this issue?</a>'
                })
            } else {
                Swal.fire({
                    title: '¿Deseas eliminar este Alumno?',
                    text: "Esta acción no podrá deshacerse.",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: 'red',
                    cancelButtonColor: '#9400D3',
                    confirmButtonText: 'Sí, Elimínalo',
                    cancelButtonText: 'No, Cancelar'
                }).then(async (result) => {
                    if (result) {
                        try {
                            console.log("justo antes de eliminar" + p3d1d0);

                            console.log("3st3 3s p3d1d0", p3d1d0);
                            const { data } = await eliminarEstudiante({
                                variables: {
                                    id: p3d1d0
                                },
                                refetchQueries: [
                                    {
                                        query: OBTENER_ALUMNOS
                                    }
                                ]

                            })
                            renderPlayer();
                        } catch (error) {
                            console.log(error);
                        }
                        if (result.value) {
                            history.push('/dashboard/student');
                            Swal.fire(
                                'Eliminado',
                                "El alumno se ha eliminado exitosamente",
                                'success'
                            )
                        }
                    }

                })
            }


        }
    });

    const [eliminarEstudiante] = useMutation(ELIMINAR_ESTUDIANTE);
    //     update(cache) {
    //         // Obtener una copia del objeto de cache
    //         const {obtenerPedidosTaller} = cache.readQuery({ query: OBTENER_PEDIDOS_TALLER });

    //         // Reescribir el cache
    //         cache.writeQuery({
    //             query: OBTENER_PEDIDOS_TALLER,
    //             data: {
    //                 obtenerPedidosTaller: obtenerPedidosTaller.filter(estudianteActual => estudianteActual.id != p3d1d0)
    //             }
    //         })
    //     }
    // });

    const { data, error, loading } = useQuery(OBTENER_ALUMNOS);
    const { data: dataPY, error: errorPY, loading: loadingPY } = useQuery(OBTENER_ALUMNOSP);

    const { data: dataP, error: errorP, loading: loadingP } = useQuery(OBTENER_PEDIDOS);


    // const [loadOptions, { data: players, error: errorZ, loading: loadingZ }] = useLazyQuery(OBTENER_PEDIDOS_TALLER, {
    //     variables: { id: taller },
    // onCompleted: data => {
    //     console.log('Variable donde se almacenarán los pedidos por taller' + pedido);
    //     console.log("Que es data" + data.obtenerPedidosTaller[0]);
    //     console.log('Que es players' + players.obtenerPedidosTaller[0]);
    //     setPedido(players);
    //     console.log("Pedidos por taller después de setear: " + pedido);

    // }
    // skip: { id: null }
    // });
    if (loading || loadingP || loadingPY) return 'Cargando ...';

    var resultX = Object.values(data.obtenerEstudiantes);
    var resultXY = Object.values(dataPY.obtenerEstudiantes);

    var result2 = Object.values(dataP.obtenerPedidos);
    var result2X = Object.values(result2);

    var arrStudents = [];
    var arrStudentsXY = [];

    var arrStudentFiltered = [];
    console.log('Estudiantes py: ' + resultXY)
    for (var i = 0; i < result2X.length; i++) {
        arrStudents.push(Object.values(result2X[i].estudiante));
    }
    for (var i = 0; i < resultXY.length; i++) {
        arrStudentsXY.push(Object.values(resultXY[i]));
    }
    console.log("arrStudents" + arrStudentsXY);

    // console.log(data);

    // console.log(loading);
    // console.log(error);

    const functionModal = (e) => {
        e.preventDefault();
        setModal(true);
        setStudentX(e.target.id);
        console.log(e);
        console.log(e.target.id);
    }

    const renderPlayer = (player, index) => {
        return (
            <tr key={player.id}  >
                <td> {player.nombre} </td>
                <td> {player.paterno} </td>
                <td> {player.materno} </td>
                <td> {player.curp} </td>
                <td> {player.email} </td>
                <td> {player.semestre} </td>
                <td style={{ display: 'flex' }} >
                    {/* <a href="#" class="btn btn-success btn-lg">
                        <span class="glyphicon glyphicon-remove"></span> Borrar
                    </a> */}

                    <a style={{ color: '#6A5ACD', marginRight: '0.5vw' }} onClick={() => getStudentX({ variables: { id: player.id } })}>
                        <span className="fas fa-eye"></span>
                        <ModalStudent
                            modal={modal}
                            setModal={setModal}
                            studentX={studentX}
                        />
                    </a>


                    <a style={{ color: '#9400D3', marginRight: '0.5vw' }} onClick={() => getStudentY({ variables: { id: player.id } })}>
                        <span className="fas fa-edit"></span>
                        <ModalStudentMod
                            modal2={modal2}
                            setModal2={setModal2}
                            studentX={studentX}
                        />
                    </a>
                    <a style={{ color: 'red', marginRight: '0vw' }} onClick={() => getStudent({ variables: { id: player.id } })


                    }>
                        <span className="fas fa-trash-alt"></span>
                    </a>



                    {/* <button type="button" class="buttonDelete">
                    </button> */}

                </td>
            </tr>
        )
    }

    const headerReady = () => {

        return (
            <tr>
                <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>
                    <span class="fas fa-user-graduate" style={{ marginRight: '0.3vw' }}></span>
                    TABLA DE ESTUDIANTES</th>
            </tr>
        );

    }
    // const expandRow = {
    //     renderer: row => (
    //         <div>
    //             <p>{`This Expand row is belong to rowKey ${row.id}`}</p>
    //             <p>You can render anything here, also you can add additional data on every row object</p>
    //             <p>expandRow.renderer callback will pass the origin row object to you</p>
    //         </div>
    //     )
    // };
    // const tablePDF = () => {
    //     const doc = new jsPDF();

    //     doc.autoTable({ tableReady });

    //     doc.save('tableReady.pdf');
    // }

    var columns = [
        { title: "Tipo", dataKey: "Tipo" },
        { title: "Nombre", dataKey: "Nombre" },
        { title: "Paterno", dataKey: "Paterno" },
        { title: "Materno", dataKey: "Materno" },
        { title: "Email", dataKey: "Email" },
        { title: "Semestre", dataKey: "Semestre" },
        { title: "Género", dataKey: "Género" },
        // { title: "T.Sangre", dataKey: "Sangre" },
        // { title: "Alergias", dataKey: "Alergias" },
        // { title: "Padecimientos", dataKey: "Padecimientos" },
    ];

    // const tablePDF = () => {
    //     var doc = new jsPDF('p', 'pt');
    //     doc.autoTable(columns, arrStudentsXY);
    //     doc.save('tableReady.pdf');
    //     // const doc = new jsPDF();

    //     // doc.autoTable({ tableReady });

    //     // doc.save('tableReady.pdf');
    // }
    const tableReady = () => {

        return (
            <ReactBootstrap.Table striped bordered hover style={{ backgroundColor: 'white' }}
            // SubComponent={row => {
            //     return (
            //         <div style={{ 'border': '1px solid rgba(0, 0, 0, 0.05)' }}>
            //             {row.row.summary}
            //         </div>
            //     )
            // }}
            // onExpandedChange={newExpanded => onExpandedChange(newExpanded)}


            >
                <thead style={{ fontWeight: '500px', font: 'Roboto', color: 'white', backgroundImage: 'linear-gradient(to right, #BA55D3, #DDA0DD)' }}>
                    {headerReady()}
                    <tr>
                        <th>Nombre</th>
                        <th>Paterno</th>
                        <th>Materno</th>
                        <th>CURP</th>
                        <th>Email</th>
                        <th>Semestre</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    {data.obtenerEstudiantes.map(renderPlayer)}
                </tbody>
            </ReactBootstrap.Table>
        );
    }

    return (
        <div className='main'>
            <Header />
            <SideBar />
            <div className='content' style={{ marginLeft: '8vw' }}>

                <div style={{ display: 'flex', flexDirection: 'column' }}>
                    {tableReady()}
                    <div style={{ display: 'flex' }}>
                        <Button style={{ opacity: '0.9', width: '10vw !important', paddingLeft: "0px !important", paddingRight: "0px !important", height: '8vh' }}
                            onClick={() => {
                                var doc = new jsPDF('p', 'pt');
                                doc.autoTable(columns, arrStudentsXY);
                                doc.save('tableReady.pdf');
                            }}
                        >
                            <span class="fas fa-print"></span> Reporte
                        </Button>
                        <Link to='/dashboard'>
                            <Button style={{ opacity: '0.9', width: '10vw !important', paddingLeft: "0px !important", paddingRight: "0px !important", height: '8vh' }}
                            // onClick={tablePDF()}
                            >
                                <span class="fas fa-home"></span> Regresar
                            </Button>
                        </Link>
                    </div>
                </div>
            </div>
            <div className='footer'>
                <div>
                    <span class="fas fa-university" style={{ marginRight: '0.3vw' }}></span>
                    Copyright © 2020, B3st0 Team
                </div>
            </div>
        </div>
    );

}

export default Dashboard;