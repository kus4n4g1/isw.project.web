import React from 'react';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';

const Form = () => {

    return (
        <div className='menu'>
            {/* <Link to='/dashboard/agenda'>
                <Button className='buttonA'>Agenda</Button>
            </Link> */}
            <div style={{
                marginLeft: '4.2vw',
                boxShadow: '0px 0px 10px rgba(0, 0, 0, .25)',
                textDecoration: 'underline overline',

            }}>ALTA</div>
            <Link to='/dashboard/admin/user'>
                <Button className='buttonA' style={{ paddingLeft: '0vw', paddingRight: '0.5vw' }}>
                    <span class="fas fa-user-graduate" style={{ marginRight: '0.3vw' }}></span>
                    ALUMNOS
                </Button>
            </Link>
            <Link to='/dashboard/admin/teacher'>
                <Button className='buttonA' style={{ paddingLeft: '0vw', paddingRight: '0.5vw' }}>
                    <span class="fas fa-graduation-cap" style={{ marginRight: '0.3vw' }}></span>
                    MAESTROS
                </Button>
            </Link>
            <Link to='/dashboard/admin/club'>
                <Button className='buttonA' style={{ paddingLeft: '0vw', paddingRight: '0.5vw' }}>
                    <span class="fas fa-chess-king" style={{ marginRight: '0.3vw' }}></span>
                    CLUBES
                </Button>
            </Link>
            <Link to='/dashboard/admin/taller'>
                <Button className='buttonA' style={{ paddingLeft: '0vw', paddingRight: '0.5vw' }}>
                    <span class="fas fa-guitar" style={{ marginRight: '0.3vw' }}></span>
                    TALLERES
                </Button>
            </Link>
            <div style={{
                marginLeft: '3.1vw',
                boxShadow: '0px 0px 10px rgba(0, 0, 0, .25)',
                textDecoration: 'underline overline',
            }}>GESTIÓN</div>
            <Link to='/dashboard/student'>
                <Button className='buttonA' style={{ paddingLeft: '0vw', paddingRight: '0.5vw' }}>
                    <span class="fas fa-user-graduate" style={{ marginRight: '0.3vw' }}></span>
                    ALUMNOS
                </Button>
            </Link>
            <Link to='/dashboard/teacher'>
                <Button className='buttonA' style={{ paddingLeft: '0vw', paddingRight: '0.5vw' }}>
                    <span class="fas fa-graduation-cap" style={{ marginRight: '0.3vw' }}></span>
                    MAESTROS
                </Button>
            </Link>
            <Link to='/dashboard/club'>
                <Button className='buttonA' style={{ paddingLeft: '0vw', paddingRight: '0.5vw' }}>
                    <span class="fas fa-chess-queen" style={{ marginRight: '0.3vw' }}></span>
                    CLUBES
                </Button>
            </Link>
            <Link to='/dashboard/taller'>
                <Button className='buttonA' style={{ paddingLeft: '0vw', paddingRight: '0.5vw' }}>
                    <span class="fas fa-palette" style={{ marginRight: '0.3vw' }}></span>
                    TALLERES
                </Button>
            </Link>
            {/* <Link to='/dashboard/admin/student'>
                <Button className='buttonA'>Usuarios</Button>
            </Link> */}
        </div>
    );
}

export default Form;