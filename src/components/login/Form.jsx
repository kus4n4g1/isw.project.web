import React, { useState } from 'react';
import { Button, Input } from 'reactstrap';
import { Link } from 'react-router-dom';
import TopInfo from '../topInfo';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useMutation, useQuery, gql } from '@apollo/client';
import { Route, Redirect, useHistory } from "react-router-dom";



const AUTENTICAR_USUARIO = gql`
    mutation autenticarUsuario($input:AutenticarInput){
        autenticarUsuario(input:$input){
        token
        }
    }
`
const OBTENER_PEDIDOS = gql`
    query obtenerPedidos {
        obtenerPedidos{
        id
        pedido{
            id
        }
        estudiante{
            email      
        }
        estado
        }
    }
`

const Form = () => {

    let history = useHistory();

    const { data: dataR, error: errorR, loading: loadingR } = useQuery(OBTENER_PEDIDOS);
    //console.log(dataR);

    // routing

    //const router = useRouter();

    const [mensaje, guardarMensaje] = useState(null);

    //Mutation para autenticar nuevos usuarios en apollo
    const [autenticarUsuario] = useMutation(AUTENTICAR_USUARIO);

    //validación de formulario
    const formik = useFormik({
        initialValues: {
            email: '',
            password: ''
        },
        validationSchema: Yup.object({
            email: Yup.string()
                .email('El email no es válido*')
                .required('El email es obligatorio!'),
            password: Yup.string()
                .required('El password es obligatorio*')
        }),
        onSubmit: async valores => {
            // console.log('enviando');
            // console.log(valores);
            const { email, password } = valores;

            try {
                const { data } = await autenticarUsuario({
                    variables: {
                        input: {
                            email,
                            password
                        }
                    }
                });

                //console.log(data);
                guardarMensaje('Autenticando...');

                //Guardar el token en localstorage
                const { token } = data.autenticarUsuario;
                localStorage.removeItem('token');
                localStorage.setItem('token', token);
                //Redireccionar hacia Estudiantes

                setTimeout(() => {
                    guardarMensaje(null);

                    if (email === 'sunny071282@gmail.com') {
                        return history.push('/dashboard');
                    }
                    // , window.location.reload(true)
                    return (history.push('/dashboard/register'))
                    //return <Redirect from='/' to="/dashboard"></Redirect>
                    //Route.push('/dashboard/register');
                }, 2000);

            } catch (error) {
                guardarMensaje(error.message.replace('GraphQL error: ', ''))
                //console.log(error);

                setTimeout(() => {
                    guardarMensaje(null);
                }, 3000);
            }
        }
    });

    const mostrarMensaje = () => {
        return (
            <div className='msgError' style={{ marginTop: '14px' }}>
                <p>{mensaje}</p>
            </div>
        );
    }

    //if (loading) return 'Cargando...';

    return (
        <div className='main'>
            <div className='content'>
                <TopInfo style={{ marginBottom: '10px important!' }} />
                <form className='loginA' style={{ marginLeft: '0.5vw' }} onSubmit={formik.handleSubmit}>
                    {mensaje && mostrarMensaje()}
                    <div className='logoA' style={{ marginBottom: '10px', marginTop: '10px' }} />
                    <div style={{ display: 'flex', flexDirection: 'column', marginLeft: '0vw', width: '13.9vw' }}>
                        <div className="input-group" style={{ marginBottom: '1.2vh', marginTop: '1.2vh' }}>
                            <span className="input-group-addon" style={{ marginLeft: '-1.9vw', opacity: '0.9' }} >
                                <span class="fas fa-envelope" style={{ marginLeft: '-0.5vw', marginRight: '0.5vw', color: 'purple' }}></span>
                            </span>
                            <Input
                                placeholder='Email'
                                id='email'
                                type='text'
                                value={formik.values.email}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                style={{ marginBottom: '10px' }}

                            />
                        </div>

                        {formik.touched.email && formik.errors.email ? (
                            <div style={{ position: 'fixed', marginTop: '6.6vh', fontSize: '16px' }}>
                                <p>{formik.errors.email}</p>
                            </div>
                        ) : null}

                        <div className="input-group" >
                            <span className="input-group-addon" style={{ marginLeft: '-1.9vw', opacity: '0.9' }}>
                                <span class="fas fa-key" style={{ marginLeft: '-0.5vw', marginRight: '0.5vw', color: 'purple' }}></span>
                            </span>
                            <Input
                                placeholder='Contraseña'
                                id='password'
                                type='password'
                                value={formik.values.password}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                style={{ marginBottom: '10px' }}
                            />
                        </div>

                        {formik.touched.password && formik.errors.password ? (
                            <div style={{ position: 'fixed', marginTop: '14.7vh', fontSize: '15px' }}>
                                <p>{formik.errors.password}</p>
                            </div>
                        ) : null}

                    </div>

                    {/* <Link to='/dashboard'> */}
                    <Button className='buttonA' type='submit'>Ingresar</Button>
                    {/* </Link> */}
                </form>
            </div>
        </div>
    );
}

export default Form;