import React from 'react';
import ReactDOM from 'react-dom';
import { useHistory } from 'react-router-dom';
import App from './components/App';
import './styles.css';
//import './bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
// import { ApolloClient, createHttpLink } from '@apollo/client';
// import { InMemoryCache } from 'apollo-cache';
// import { setContext } from 'apollo-link-context';
import { ApolloProvider } from '@apollo/react-hooks';
import client from './config/apollo';
import { AllProvider } from './components/context/AllContext';

// const httpLink = createHttpLink({
//     uri: 'http://localhost:4000/'
// });

// const authLink = setContext((_, { headers }) => {
//     return {
//         headers: {

//         }
//     }
// })

// const client = new ApolloClient({
//     link: authLink.concat(httpLink),
//     cache: new InMemoryCache()
// })
// const MyApp = ({ Component, pageProps}) => {

//     console.log('Desde _app.js')
//     return(
//     <Component {...pageProps} />
//     );
// }

const ApolloApp = () => (
  <ApolloProvider client={client} useHistory={useHistory}>
    <App />
  </ApolloProvider>
);

ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById('root')
);
